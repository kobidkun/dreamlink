<link rel="shortcut icon" type="image/x-icon" href="images/fab.png"/>
<!doctype html>
<html>




<meta http-equiv="content-type" content="text/html;charset=UTF-8"/><!-- /Added by HTTrack -->
<head>
    <meta charset="utf-8">
    <title>@yield('title') - Dreamlink</title>


    <link href="css/style.css" rel="stylesheet">

    <link rel="stylesheet" href="css/animate.css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
          integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">




    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
    @yield('header-css')
</head>
<body>
<header>



    <link href="css/style.css" rel="stylesheet"/>

    <link rel="icon" href="images/fav.html" type="image/gif" sizes="24x24">


    @include('components.header');


</header>


@yield('content')

<div id="full-width"
     style="background-image:url(images/telephone.jpg); min-width:1200px; background-position:center; background-size:cover; background-repeat:no-repeat; height:500px;">
    <center>
        <table width="1140" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
                <td width="1200" align="center" valign="top">
                    <table width="600" border="0" align="center" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="1200" height="80">&nbsp;</td>
                        </tr>
                        <style>
                            .refund a {
                                color: #39cba5;
                                text-align: center;
                                font-weight: 900;
                                font-size: 18px;

                            }
                        </style>
                        <tr>
                            <td align="center" valign="top">If you want the best connection, you want Dreamlink.
                                Dreamlink High Speed Optical Fiber Internet offers a high-quality connection. <br>
                                Call today and we'll help you pick the services and packages that's right for you.
                                <br>
                                <br>
                                Talk to our representative now <br>
                                <br>
                                <p class="refund"><a href="/terms-and-condition">Terms & Condition and Refund Policy</a> | <a href="/privacy">Privacy Policy</a></p>
                                <br>
                                <span class="subtitle"><i class="fa fa-phone-square" aria-hidden="true"></i> Call 0353 2501854 / 52</span><br/>
                                <span class="subtitle"><i class="fa fa-phone-square" aria-hidden="true"></i> Call 70470 93333/ 7047092222</span><br/>
                                <a href="https://www.facebook.com/Dreamlink-Technologies-174741843094314/"
                                   target="_blank">
                                    <img src="images/facebook-logo.png" width="70" height="40"
                                         title="Facebook"/></a>
                                <a href="#" target="_blank">
                                    <img src="images/twitter-logo.png" width="40" height="40" title="Twitter"/></a>
                                <a href="#" target="_blank"
                                   style="margin-left: 10px;">
                                    <img src="images/instagram-logo.png" width="40" height="40" title="Instagram"/></a>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </center>
</div>
<footer id="container">
    <center> © Copyright  2018 Dreamlink Technologies P. Ltd. All rights reserved.</center>


</footer>
<!-- Facebook Pixel Code -->
<script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window, document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '138099013528935');
    fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
               src="https://www.facebook.com/tr?id=138099013528935&ev=PageView&noscript=1"
    /></noscript>
<!-- End Facebook Pixel Code -->


<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-110170833-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-110170833-1');
</script>
<script type='application/ld+json'>
{
    "@context": "http://schema.org/",
    "@type": "LocalBusiness",
    "name": "dreamlink Technologies Private Limited",
    "url": "https://www.dreamlink.in",
    "image": "https://www.dreamlink.in/img/logo2.png",
    "description": "Dreamlink Technology Pvt Ltd was born with a vision to provide the best internet service for entertainment , education and business purposes in West Bengal On 2016. Based in Siliguri we offer broadband services on optic fiber and our Fiber-to-the-home (FTTH) Technology can carry huge amount of information at speed upto 1Gbps. We are the first FTTH(fiber-to-the-home) broadband service provider in this area",
    "address": {
        "@type": "PostalAddress",
        "streetAddress": "Spencer Plaza, 2nd Floor, Burdwan Road",
        "addressLocality": "Siliguri",
        "addressRegion": "WEST BENGAL",
        "postalCode": "734005",
        "addressCountry": "India"
    },
      "aggregateRating": {
    "@type": "AggregateRating",
    "ratingValue": "90",
    "bestRating": "100",
    "worstRating": "60",
    "ratingCount": "3"
  },
    "geo": {
    "@type": "GeoCoordinates",
    "latitude": 26.7092272,
    "longitude": 88.3467509
  },
    "telephone": "+913532501854"
}
</script>
@yield('footerscripts')
</body>
</html>
