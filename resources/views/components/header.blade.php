<style>
    .navbar {

        position: fixed; /* Set the navbar to fixed position */
        height: 60px;
        margin-bottom: 0px;
    !important;
    }

    .nav-link {
        font-size: 20px;

    }

    .nav-link:hover {
        background-color: #ecedee;

    }

    .navbar-nav > li {
        margin-left: 13px;
        margin-right: 13px;
    }

    .dropdown-menu {
        min-width: 200px;
    }

    .dropdown-menu.columns-3 {
        min-width: 680px;
    }

    .dropdown-menu li a {
        padding: 5px 15px;
        font-weight: 300;
    }

    .multi-column-dropdown {
        list-style: none;
        margin: 0px;
        padding: 0px;
    }

    .multi-column-dropdown li a {
        display: block;
        clear: both;
        line-height: 1.428571429;
        color: #333;
        white-space: normal;
    }

    .nav-header-link {
        padding-left: 10px;
        font-size: 16px;
        color: #00B58B;
        font-weight: 900;
    }

    .multi-column-dropdown li a:hover {
        text-decoration: none;
        color: #262626;
        background-color: #ecedee;
    }

    @media (max-width: 767px) {
        .dropdown-menu.multi-column {
            min-width: 240px !important;
            overflow-x: hidden;
        }
    }




</style>

<style>.navbar-collapse.collapse {
            display: block!important;
        }

        .navbar-nav>li, .navbar-nav {
            float: left !important;
        }

        .navbar-nav.navbar-right:last-child {
            margin-right: -15px !important;
        }

        .navbar-right {
            float: right!important;
        }</style>

<nav class="navbar navbar-expand-lg navbar-expand fixed-top navbar-light bg-light sticky-top">
    <a class="navbar-brand" style="padding-left: 5%" href="/"><img src="img/logo2.png" height="60px"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
            aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" style="padding-left: 10%" id="navbarNavDropdown">
        <ul class="navbar-nav">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink"
                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Service
                </a>
                <div class="dropdown-menu multi-column columns-3" aria-labelledby="navbarDropdownMenuLink">

                    <!-- <a class="dropdown-item" href="#">Action</a>
                     <a class="dropdown-item" href="#">Another action</a>
                     <a class="dropdown-item" href="#">Something else here</a><!---->

                    <div class="row">
                        <div class="col-sm-6">
                            <ul class="multi-column-dropdown">
                                <span class="nav-header-link hidden-xs">Residential Broadband</span>
                                <li><a href="/dreamlink-fiber">Dreamlink Fiber </a>
                                </li>

                                <li><a href="/dreamlink-rf">Dreamlink RF <span class="badge badge-secondary"
                                                                                     style="background-color: #00B58B"> New</span></a>
                                </li>
                                <li><a href="/plans-pricing">Plans and Pricing</a></li>
                                <div class="dropdown-divider"></div>
                                <span class="nav-header-link hidden-xs">Others</span>
                                <li><a href="https://play.google.com/store/apps/details?id=in.com.dreamlink.dreamlinkapp">Download App</a></li>

                            </ul>
                        </div>
                        <div class="col-sm-6">
                            <ul class="multi-column-dropdown">
                                <span class="nav-header-link">SME Broadband</span>
                                <li><a href="/sme-plan">Plans & Pricing</a></li>
                            </ul>
                        </div>

                    </div>


                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink"
                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Support
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="/ftth_troubleshoot">FAQs - DreamLink Fiber</a>
                    <a class="dropdown-item" href="/dreamlink-general-support">FAQs - DreamLink General Support</a><!--
                    <a class="dropdown-item" href="/dreamlink-settings">Configure Software/Router</a>-->
                    <a class="dropdown-item" href="/security">Security Articles</a>
                </div>
            </li>

            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink"
                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Bills & Payment
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="https://eservice.dreamlink.in/"  target="_blank">Customer login</a>
                    <a class="dropdown-item" href="https://www.dreamlink.in/home" >Online Payment</a>
                </div>
            </li>

            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink"
                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Company
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="/companyprofile">Company Profile</a>
                    <a class="dropdown-item" href="/missionvision">Vision & Mission</a>
                    <a class="dropdown-item" href="/quality">Quality Policy</a>

                    <a class="dropdown-item" href="#">Calendar</a>
                </div>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="contact">Contact</a>
            </li>


        </ul>
    </div>
</nav>
</nav>

<style>
    .flootingform {
        position: fixed;
        -moz-border-radius-topleft: 10px;
        -moz-border-radius-topright: 5px;
        height: 32px;
        width: 240px;
        background: #39cba5;
        z-index: 100;
        bottom: 0;
        right: 10px;
        border-radius: 25px 25px 0px 0px;

    }

    .flootingform p a{
        margin-left: 27px;
        margin-top: 5px;!important;
        text-align: center;
        align-content: center;
        align-items: center;
        color: #ffffff;!important;
        font-size: 18px;
        font-weight: 500;
        text-decoration: none;

    }

    .modalpopup {
        z-index: 1000;
        margin-top: 100px;

    }

    .flootingformright {
        position: fixed;
        -moz-border-radius-topleft: 10px;
        -moz-border-radius-topright: 5px;
        height: 40px;
        width: 200px;
        background: #39cba5;
        z-index: 100;
        bottom: 55%;
        left: -90px;
        -webkit-transform: rotate(90deg);
        -moz-transform: rotate(90deg);
        -o-transform: rotate(90deg);
        -ms-transform: rotate(90deg);
        transform: rotate(90deg);


    }

    .flootingformright p a{
        margin-left: 27px;
        margin-top: 5px;!important;
        text-align: center;
        align-content: center;
        align-items: center;
        color: #ffffff;!important;
        font-size: 18px;
        font-weight: 500;
        text-decoration: none;

    }

    .modal-backdrop {
        z-index: 30;
    }

</style>




<div class="flootingform">
    <div style="height: 3px"></div>
    <p><a data-toggle="modal" href="#" data-target="#myModal">
            <i class="fa fa-phone" aria-hidden="true"></i>Book Now</a></p>
</div>
<div id="myModal" class="modal modalpopup " role="dialog">
    <div class="modal-dialog">

        <div class="modal-content modalpopup">
            <div class="modal-header" style="background-color: #39cba5">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" style="color: #ffffff;">Quick Booking Now</h4>
            </div>
            <form role="form" method="POST" action="{{ route('landingpage.saveform') }}">

                <div class="modal-body">
                    {{ csrf_field() }}
                    <input type="text" required name="name" class="form-control required input-lg not-dark"
                           value="" placeholder="Your Name*">
                    <br>
                    <input type="text" required name="mobile" class="form-control required input-lg not-dark"
                           value="" placeholder="Mobile Number">
                    <br>
                    <input type="text" name="email" class="form-control required input-lg not-dark"
                           value="" placeholder="Email (optional)">

                </div>
                <div class="modal-footer">
                    <button type="submit" style="background-color: #00D7A5; border-color: #39cba5" class="btn btn-primary btn-block">
                        <i class="fa fa-envelope"></i> Book Now
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
