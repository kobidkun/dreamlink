<table width="1000" border="0" align="center" cellpadding="0"
       cellspacing="0" class="tr-color">
    <tr>
        <td colspan="5" align="center">
            <p>
                <span class="subtitle">ONE TIME SET-UP CHARGE<br>
                    <div
                            class="router-pricing-box">
                        <div
                                class="tab-title-col">
                            <div
                                    class="bold"
                                    style="margin-top: 0px;"></div>

                            <ul class="white">

                                <li class="bold"
                                    style="padding-top: 13px; vertical-align: middle;">Duration</li>

                                <li
                                        class="transparent">Fiber Router Rent
                                </li>

                                <li
                                        class="transparent">Dropwire Charges
                                </li>

                                <li class="transparent"> Refundable Deposit</li>
                            </ul>
                        </div>

                        <div class="tab-pricing-col" style="width: 25%;"><ul><li class="bold">1 Month<div
                                            style="margin-bottom: 8px;"></div></li>

                                <li class="transparent"><span>
                                        <i
                                                class="fa fa-inr" aria-hidden="true"></i>

                                        1000</span>&nbsp;<span></span>&nbsp;<span></span>&nbsp;</li>


                                <li
                                        class="transparent"><span><i class="fa fa-inr"
                                                                     aria-hidden="true"></i> 800</span>&nbsp;<span></span>&nbsp;<span></span>&nbsp;</li> <li
                                        class="transparent"><span><i class="fa fa-inr"
                                                                     aria-hidden="true"></i> 1500</span>&nbsp;<span></span>&nbsp;<span></span>&nbsp;</li>


                            </ul></div><div
                                class="tab-pricing-col" style="width: 25%;"><ul><li class="bold">6 Months<div
                                            style="margin-bottom: 8px;"></div></li><li
                                        class="transparent"><span><i class="fa fa-inr"
                                                                     aria-hidden="true"></i> 500</span>&nbsp;<span></span>&nbsp;<span></span>&nbsp;</li>

                                <li
                                        class="transparent"><span><i class="fa fa-inr"
                                                                     aria-hidden="true"></i> 800</span>&nbsp;<span></span>&nbsp;<span></span>&nbsp;</li><li
                                        class="transparent"><span><i class="fa fa-inr"
                                                                     aria-hidden="true"></i> 1500</span>&nbsp;<span></span>&nbsp;<span></span>&nbsp;</li>

                            </ul></div><div
                                class="tab-pricing-col" style="width: 25%;"><ul><li class="bold">12 Months<div
                                            style="margin-bottom: 8px;"></div></li><li
                                        class="transparent"><span>FREE</span>&nbsp;<span></span>&nbsp;<span></span>&nbsp;</li>

                                <li
                                        class="transparent"><span><i class="fa fa-inr"
                                                                     aria-hidden="true"></i> 800</span>&nbsp;<span></span>&nbsp;<span></span>&nbsp;</li>

                                <li
                                        class="transparent"><span><i class="fa fa-inr"
                                                                     aria-hidden="true"></i> 1500</span>&nbsp;<span></span>&nbsp;<span></span>&nbsp;</li>

                            </ul></div>                        <div
                                class="clear"></div>
                    </div> </span>
            </p>
        </td>
    </tr>
    <tr>
        <td width="52" height="60">&nbsp;</td>
        <td width="948" height="60" colspan="4"><p>&nbsp;</p>
            <div style="background-color: #FFF; padding: 15px; position: relative; left: -20px;">
                <p>
                <h4>GST is extra.</h4>
                Above mentioned charge of Fiber Router is rental only. The Router should be returned if
                the service is discontinued.<br/><br/>
                Refundable deposit will be returned only upon returning the Router.<br/>
                Plans pricing may vary from location to location and feasibility<br/>
                </p>
            </div>
        </td>
    </tr>
</table>