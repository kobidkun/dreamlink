<table width="1200" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td align="center" valign="top">&nbsp;</td>
    </tr>
    <tr>
        <td align="center" valign="top">
            <table width="800" border="0"
                   align="center" cellpadding="0" cellspacing="0">
                <tr>

                </tr>

                <tr>
                    <td align="center"></td>
                </tr>
                <tr>
                    <td align="center"><br>
                        <table width="1030" border="0" cellspacing="0"
                               cellpadding="0">
                            <tr>
                                <td width="1030">

                                    <center>
                                        <div class="subtitle">Dreamlink Fiber SME Plans and Pricing</div>
                                    </center>

                                    <div id="parentHorizontalTab">
                                        <div class="resp-tabs-container hor_1">

                                            <div>
                                                <p>
                                                    <!--vertical Tabs-->


                                                    <div id="ChildVerticalTab_1">


                                                        <ul class="resp-tabs-list ver_1">
                                                            <li>5 Mbps Vol</li>
                                                            <li>10 Mbps Vol</li>
                                                            <li>25 Mbps Vol </li>
                                                            <li>50 Mbps Vol</li>
                                                            <li>100 Mbps Vol</li>
                                                            <li>6 Mbps UL</li>
                                                            <li>8 Mbps UL</li>
                                                        </ul>
                                                        <div class="resp-tabs-container ver_1">
                                                            <div>
                                                <p>
                                                <div class="tab-header">
                                                                        <span class="subtitle"
                                                                              style="padding-top: 15px;">5 Mbps Volume Package</span>
                                                </div>
                                                <div class="tab-subheader">
                                                    <div class="tab-title-col"
                                                         style="width: 150px;">

                                                        <ul class="white">
                                                            <li class="bold"
                                                                style="padding: 17px;"></li>
                                                            <li class="transparent">Price/Renewal
                                                            </li>
                                                            <li class="transparent2">Installation
                                                                Charge
                                                            </li>

                                                            <li class="transparent2">Turbo Speed
                                                            </li>


                                                            <li class="transparent2">Unlimited Speed
                                                            </li>

                                                            <li class="transparent2">Total Package
                                                                Price
                                                            </li>
                                                        </ul>
                                                        <span style="font-size: smaller; font-weight: normal;color: white;
                                padding: 5px 10px; margin-top: 10px; margin-left: 5px;display: block;">Fair Usage Policy is applicable</span>
                                                    </div>
                                                    <div class="tab-pricing-col"
                                                         style="width: 27%;">
                                                        <ul>
                                                            <li class="bold">1 Month</li>
                                                            <li class="transparent"><strong><i class="fa fa-inr" aria-hidden="true"></i> 1000&nbsp;</strong>
                                                            </li>
                                                            <li class="transparent2">
                                                                <strong><strike><i class="fa fa-inr" aria-hidden="true"></i>
                                                                        2500</strike>&nbsp;FREE&nbsp;</strong>
                                                            </li>

                                                            <li class="transparent2">
                                                                <strong>75 Gb / Month
                                                                       </strong>
                                                            </li>

                                                            <li class="transparent2">
                                                                <strong>
                                                                       1 Mbps
                                                                </strong>
                                                            </li>


                                                            <li class="transparent2"><strong><i class="fa fa-inr" aria-hidden="true"></i>
                                                                    1000&nbsp;</strong></li>
                                                            <li><a href="#" target="_blank">
                                                                    <div
                                                                            class="order-button">
                                                                        Order Now »
                                                                    </div>
                                                                </a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="tab-pricing-col"
                                                         style="width: 28%;">
                                                        <ul>
                                                            <li class="bold">6 + 1 Months</li>
                                                            <li class="transparent"><strong><i class="fa fa-inr" aria-hidden="true"></i>
                                                                    6000&nbsp;</strong>
                                                            </li>
                                                            <li class="transparent2">
                                                                <strong>
                                                                    <strike>

                                                                        <i class="fa fa-inr" aria-hidden="true"></i>
                                                                        2500</strike>&nbsp;FREE&nbsp;

                                                                </strong>
                                                            </li>

                                                            <li class="transparent2">
                                                                <strong>
                                                                    525 Gb
                                                                </strong>
                                                            </li>

                                                            <li class="transparent2">
                                                                <strong>
                                                                    1 Mbps
                                                                </strong>
                                                            </li>

                                                            <li class="transparent2">
                                                                <strong>
                                                                    <i class="fa fa-inr" aria-hidden="true"></i>
                                                                    4800
                                                                </strong>
                                                            </li>
                                                            <li><a href="#" target="_blank">
                                                                    <div
                                                                            class="order-button">
                                                                        Order Now »
                                                                    </div>
                                                                </a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="tab-pricing-col"
                                                         style="width: 28%;">
                                                        <ul>
                                                            <li class="bold">12 + 2 Months</li>
                                                            <li class="transparent"><strong>
                                                                    <i class="fa fa-inr" aria-hidden="true"></i>
                                                                    12000&nbsp;</strong></li>

                                                            <li class="transparent2">
                                                                <strong><strike><i class="fa fa-inr" aria-hidden="true"></i>
                                                                        2500</strike>&nbsp;FREE&nbsp;</strong>
                                                            </li>


                                                            <li class="transparent2"><strong>
                                                                   1050 Gb
                                                                    &nbsp;</strong></li>

                                                            <li class="transparent2"><strong>
                                                                 1 Mbps
                                                                    &nbsp;</strong></li>

                                                            <li class="transparent2">
                                                                <strong><i class="fa fa-inr" aria-hidden="true"></i>
                                                                        12000</strong>
                                                            </li>




                                                            <li><a href="#" target="_blank">
                                                                    <div
                                                                            class="order-button">
                                                                        Order Now »
                                                                    </div>
                                                                </a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="clear"><p style="position: relative; color: white; font-weight: bold; display: block;
                                                 padding: 10px;">
                                                            Above mentioned plans are exclusively
                                                            for residential purpose only and shall
                                                            not be resold or used for business
                                                            purpose.
                                                        </p></div>
                                                </div>


                                                </p>
                                            </div>


                                            {{--next pricingf --}}


                                            <div><p>
                                                <div class="tab-header">
                                                                        <span class="subtitle"
                                                                              style="padding-top: 15px;">10 Mbps Volume Package</span>
                                                </div>
                                                <div class="tab-subheader">
                                                    <div class="tab-title-col"
                                                         style="width: 150px;">

                                                        <ul class="white">
                                                            <li class="bold"
                                                                style="padding: 17px;"></li>
                                                            <li class="transparent">Price/Renewal
                                                            </li>
                                                            <li class="transparent2">Installation
                                                                Charge
                                                            </li>

                                                            <li class="transparent2">Turbo Speed
                                                            </li>


                                                            <li class="transparent2">Unlimited Speed
                                                            </li>

                                                            <li class="transparent2">Total Package
                                                                Price
                                                            </li>
                                                        </ul>
                                                        <span style="font-size: smaller; font-weight: normal;color: white;
                                padding: 5px 10px; margin-top: 10px; margin-left: 5px;display: block;">Fair Usage Policy is applicable</span>
                                                    </div>
                                                    <div class="tab-pricing-col"
                                                         style="width: 27%;">
                                                        <ul>
                                                            <li class="bold">1 Month</li>
                                                            <li class="transparent"><strong><i class="fa fa-inr" aria-hidden="true"></i> 1500</strong>
                                                            </li>
                                                            <li class="transparent2">
                                                                <strong><strike><i class="fa fa-inr" aria-hidden="true"></i>
                                                                        2500</strike>&nbsp;FREE&nbsp;</strong>
                                                            </li>

                                                            <li class="transparent2">
                                                                <strong>125 Gb / Month
                                                                </strong>
                                                            </li>

                                                            <li class="transparent2">
                                                                <strong>
                                                                    1.5 Mbps
                                                                </strong>
                                                            </li>

                                                            <li class="transparent2"><strong><i class="fa fa-inr" aria-hidden="true"></i>
                                                                    1500&nbsp;</strong></li>
                                                            <li><a href="#" target="_blank">
                                                                    <div
                                                                            class="order-button">
                                                                        Order Now »
                                                                    </div>
                                                                </a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="tab-pricing-col"
                                                         style="width: 28%;">
                                                        <ul>
                                                            <li class="bold">6 + 1 Months</li>
                                                            <li class="transparent"><strong><i class="fa fa-inr" aria-hidden="true"></i>9000
                                                                    &nbsp;</strong>
                                                            </li>
                                                            <li class="transparent2">
                                                                <strong>
                                                                    <strike>

                                                                        <i class="fa fa-inr" aria-hidden="true"></i>
                                                                        2500</strike>&nbsp;FREE&nbsp;

                                                                </strong>
                                                            </li>

                                                            <li class="transparent2">
                                                                <strong>
                                                                    875 Gb
                                                                </strong>
                                                            </li>

                                                            <li class="transparent2">
                                                                <strong>
                                                                    1.5 Mbps
                                                                </strong>
                                                            </li>

                                                            <li class="transparent2">
                                                                <strong>
                                                                    <i class="fa fa-inr" aria-hidden="true"></i>
                                                                    9000
                                                                </strong>
                                                            </li>
                                                            <li><a href="#" target="_blank">
                                                                    <div
                                                                            class="order-button">
                                                                        Order Now »
                                                                    </div>
                                                                </a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="tab-pricing-col"
                                                         style="width: 28%;">
                                                        <ul>
                                                            <li class="bold">12 + 2 Months</li>
                                                            <li class="transparent"><strong>
                                                                    <i class="fa fa-inr" aria-hidden="true"></i>
                                                                    18000&nbsp;</strong></li>

                                                            <li class="transparent2">
                                                                <strong><strike><i class="fa fa-inr" aria-hidden="true"></i>
                                                                        2500</strike>&nbsp;FREE&nbsp;</strong>
                                                            </li>


                                                            <li class="transparent2"><strong>
                                                                    1750 Gb
                                                                    &nbsp;</strong></li>

                                                            <li class="transparent2"><strong>
                                                                    1.5 Mbps
                                                                    &nbsp;</strong></li>

                                                            <li class="transparent2">
                                                                <strong><i class="fa fa-inr" aria-hidden="true"></i>
                                                                    18000</strong>
                                                            </li>




                                                            <li><a href="#" target="_blank">
                                                                    <div
                                                                            class="order-button">
                                                                        Order Now »
                                                                    </div>
                                                                </a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="clear"><p style="position: relative; color: white; font-weight: bold; display: block;
                                                 padding: 10px;">
                                                            Above mentioned plans are exclusively
                                                            for residential purpose only and shall
                                                            not be resold or used for business
                                                            purpose.
                                                        </p></div>
                                                </div>


                                                </p>
                                            </div>


                                            {{--next pricing --}}
                                            <div><p>
                                                <div class="tab-header">
                                                                        <span class="subtitle"
                                                                              style="padding-top: 15px;">25 Mbps VOLUME</span>
                                                </div>
                                                <div class="tab-subheader">
                                                    <div class="tab-title-col"
                                                         style="width: 150px;">

                                                        <ul class="white">
                                                            <li class="bold"
                                                                style="padding: 17px;"></li>
                                                            <li class="transparent">Price/Renewal
                                                            </li>
                                                            <li class="transparent2">Installation
                                                                Charge
                                                            </li>

                                                            <li class="transparent2">Turbo Speed
                                                            </li>


                                                            <li class="transparent2">Unlimited Speed
                                                            </li>

                                                            <li class="transparent2">Total Package
                                                                Price
                                                            </li>
                                                        </ul>
                                                        <span style="font-size: smaller; font-weight: normal;color: white;
                                padding: 5px 10px; margin-top: 10px; margin-left: 5px;display: block;">Fair Usage Policy is applicable</span>
                                                    </div>
                                                    <div class="tab-pricing-col"
                                                         style="width: 27%;">
                                                        <ul>
                                                            <li class="bold">1 Month</li>
                                                            <li class="transparent"><strong><i class="fa fa-inr" aria-hidden="true"></i> 2500&nbsp;</strong>
                                                            </li>
                                                            <li class="transparent2">
                                                                <strong><strike><i class="fa fa-inr" aria-hidden="true"></i>
                                                                        2500</strike>&nbsp;FREE&nbsp;</strong>
                                                            </li>

                                                            <li class="transparent2">
                                                                <strong>200 Gb / Month
                                                                </strong>
                                                            </li>

                                                            <li class="transparent2">
                                                                <strong>
                                                                    2 Mbps
                                                                </strong>
                                                            </li>

                                                            <li class="transparent2"><strong><i class="fa fa-inr" aria-hidden="true"></i>
                                                                    2500&nbsp;</strong></li>
                                                            <li><a href="#" target="_blank">
                                                                    <div
                                                                            class="order-button">
                                                                        Order Now »
                                                                    </div>
                                                                </a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="tab-pricing-col"
                                                         style="width: 28%;">
                                                        <ul>
                                                            <li class="bold">6 + 1 Months</li>
                                                            <li class="transparent"><strong><i class="fa fa-inr" aria-hidden="true"></i>
                                                                    15000</strong>
                                                            </li>
                                                            <li class="transparent2">
                                                                <strong>
                                                                    <strike>

                                                                        <i class="fa fa-inr" aria-hidden="true"></i>
                                                                        2500</strike>&nbsp;FREE&nbsp;

                                                                </strong>
                                                            </li>

                                                            <li class="transparent2">
                                                                <strong>
                                                                    1400 Gb
                                                                </strong>
                                                            </li>

                                                            <li class="transparent2">
                                                                <strong>
                                                                    2 Mbps
                                                                </strong>
                                                            </li>

                                                            <li class="transparent2">
                                                                <strong>
                                                                    <i class="fa fa-inr" aria-hidden="true"></i>
                                                                    1500
                                                                </strong>
                                                            </li>
                                                            <li><a href="#" target="_blank">
                                                                    <div
                                                                            class="order-button">
                                                                        Order Now »
                                                                    </div>
                                                                </a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="tab-pricing-col"
                                                         style="width: 28%;">
                                                        <ul>
                                                            <li class="bold">12 + 2 Months</li>
                                                            <li class="transparent"><strong>
                                                                    <i class="fa fa-inr" aria-hidden="true"></i>
                                                                    30000&nbsp;</strong></li>

                                                            <li class="transparent2">
                                                                <strong><strike><i class="fa fa-inr" aria-hidden="true"></i>
                                                                        2500</strike>&nbsp;FREE&nbsp;</strong>
                                                            </li>


                                                            <li class="transparent2"><strong>
                                                                    2800 Gb
                                                                    &nbsp;</strong></li>

                                                            <li class="transparent2"><strong>
                                                                    2 Mbps
                                                                    &nbsp;</strong></li>

                                                            <li class="transparent2">
                                                                <strong><i class="fa fa-inr" aria-hidden="true"></i>
                                                                    30000</strong>
                                                            </li>




                                                            <li><a href="#" target="_blank">
                                                                    <div
                                                                            class="order-button">
                                                                        Order Now »
                                                                    </div>
                                                                </a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="clear"><p style="position: relative; color: white; font-weight: bold; display: block;
                                                 padding: 10px;">
                                                            Above mentioned plans are exclusively
                                                            for residential purpose only and shall
                                                            not be resold or used for business
                                                            purpose.
                                                        </p></div>
                                                </div>


                                                </p>
                                            </div>

                                            <div><p>
                                                <div class="tab-header">
                                                                        <span class="subtitle"
                                                                              style="padding-top: 15px;">50 Mbps VOLUME</span>
                                                </div>
                                                <div class="tab-subheader">
                                                    <div class="tab-title-col"
                                                         style="width: 150px;">

                                                        <ul class="white">
                                                            <li class="bold"
                                                                style="padding: 17px;"></li>
                                                            <li class="transparent">Price/Renewal
                                                            </li>
                                                            <li class="transparent2">Installation
                                                                Charge
                                                            </li>

                                                            <li class="transparent2">Turbo Speed
                                                            </li>


                                                            <li class="transparent2">Unlimited Speed
                                                            </li>

                                                            <li class="transparent2">Total Package
                                                                Price
                                                            </li>
                                                        </ul>
                                                        <span style="font-size: smaller; font-weight: normal;color: white;
                                padding: 5px 10px; margin-top: 10px; margin-left: 5px;display: block;">Fair Usage Policy is applicable</span>
                                                    </div>
                                                    <div class="tab-pricing-col"
                                                         style="width: 27%;">
                                                        <ul>
                                                            <li class="bold">1 Month</li>
                                                            <li class="transparent"><strong><i class="fa fa-inr" aria-hidden="true"></i> 3000&nbsp;</strong>
                                                            </li>
                                                            <li class="transparent2">
                                                                <strong><strike><i class="fa fa-inr" aria-hidden="true"></i>
                                                                        2500</strike>&nbsp;FREE&nbsp;</strong>
                                                            </li>

                                                            <li class="transparent2">
                                                                <strong>250 Gb / Month
                                                                </strong>
                                                            </li>

                                                            <li class="transparent2">
                                                                <strong>
                                                                    3 Mbps
                                                                </strong>
                                                            </li>

                                                            <li class="transparent2"><strong><i class="fa fa-inr" aria-hidden="true"></i>
                                                                    3000&nbsp;</strong></li>
                                                            <li><a href="#" target="_blank">
                                                                    <div
                                                                            class="order-button">
                                                                        Order Now »
                                                                    </div>
                                                                </a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="tab-pricing-col"
                                                         style="width: 28%;">
                                                        <ul>
                                                            <li class="bold">6 + 1 Months</li>
                                                            <li class="transparent"><strong><i class="fa fa-inr" aria-hidden="true"></i>
                                                                    18000&nbsp;</strong>
                                                            </li>
                                                            <li class="transparent2">
                                                                <strong>
                                                                    <strike>

                                                                        <i class="fa fa-inr" aria-hidden="true"></i>
                                                                        2500</strike>&nbsp;FREE&nbsp;

                                                                </strong>
                                                            </li>

                                                            <li class="transparent2">
                                                                <strong>
                                                                    1750 Gb
                                                                </strong>
                                                            </li>

                                                            <li class="transparent2">
                                                                <strong>
                                                                    3 Mbps
                                                                </strong>
                                                            </li>

                                                            <li class="transparent2">
                                                                <strong>
                                                                    <i class="fa fa-inr" aria-hidden="true"></i>
                                                                    18000
                                                                </strong>
                                                            </li>
                                                            <li><a href="#" target="_blank">
                                                                    <div
                                                                            class="order-button">
                                                                        Order Now »
                                                                    </div>
                                                                </a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="tab-pricing-col"
                                                         style="width: 28%;">
                                                        <ul>
                                                            <li class="bold">12 + 2 Months</li>
                                                            <li class="transparent"><strong>
                                                                    <i class="fa fa-inr" aria-hidden="true"></i>
                                                                    36000&nbsp;</strong></li>

                                                            <li class="transparent2">
                                                                <strong><strike><i class="fa fa-inr" aria-hidden="true"></i>
                                                                        2500</strike>&nbsp;FREE&nbsp;</strong>
                                                            </li>


                                                            <li class="transparent2"><strong>
                                                                    3500 Gb
                                                                    &nbsp;</strong></li>

                                                            <li class="transparent2"><strong>
                                                                    3 Mbps
                                                                    &nbsp;</strong></li>

                                                            <li class="transparent2">
                                                                <strong><i class="fa fa-inr" aria-hidden="true"></i>
                                                                    36000</strong>
                                                            </li>




                                                            <li><a href="#" target="_blank">
                                                                    <div
                                                                            class="order-button">
                                                                        Order Now »
                                                                    </div>
                                                                </a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="clear"><p style="position: relative; color: white; font-weight: bold; display: block;
                                                 padding: 10px;">
                                                            Above mentioned plans are exclusively
                                                            for residential purpose only and shall
                                                            not be resold or used for business
                                                            purpose.
                                                        </p></div>
                                                </div>


                                                </p>
                                            </div>
                                            <div><p>
                                                <div class="tab-header">
                                                                        <span class="subtitle"
                                                                              style="padding-top: 15px;">100 Mbps Volume Package</span>
                                                </div>
                                                <div class="tab-subheader">
                                                    <div class="tab-title-col"
                                                         style="width: 150px;">

                                                        <ul class="white">
                                                            <li class="bold"
                                                                style="padding: 17px;"></li>
                                                            <li class="transparent">Price/Renewal
                                                            </li>
                                                            <li class="transparent2">Installation
                                                                Charge
                                                            </li>

                                                            <li class="transparent2">Turbo Speed
                                                            </li>


                                                            <li class="transparent2">Unlimited Speed
                                                            </li>

                                                            <li class="transparent2">Total Package
                                                                Price
                                                            </li>
                                                        </ul>
                                                        <span style="font-size: smaller; font-weight: normal;color: white;
                                padding: 5px 10px; margin-top: 10px; margin-left: 5px;display: block;">Fair Usage Policy is applicable</span>
                                                    </div>
                                                    <div class="tab-pricing-col"
                                                         style="width: 27%;">
                                                        <ul>
                                                            <li class="bold">1 Month</li>
                                                            <li class="transparent"><strong><i class="fa fa-inr" aria-hidden="true"></i> 4000&nbsp;</strong>
                                                            </li>
                                                            <li class="transparent2">
                                                                <strong><strike><i class="fa fa-inr" aria-hidden="true"></i>
                                                                        2500</strike>&nbsp;FREE&nbsp;</strong>
                                                            </li>

                                                            <li class="transparent2">
                                                                <strong>300 Gb / Month
                                                                </strong>
                                                            </li>

                                                            <li class="transparent2">
                                                                <strong>
                                                                    4 Mbps
                                                                </strong>
                                                            </li>

                                                            <li class="transparent2"><strong><i class="fa fa-inr" aria-hidden="true"></i>
                                                                    4000&nbsp;</strong></li>
                                                            <li><a href="#" target="_blank">
                                                                    <div
                                                                            class="order-button">
                                                                        Order Now »
                                                                    </div>
                                                                </a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="tab-pricing-col"
                                                         style="width: 28%;">
                                                        <ul>
                                                            <li class="bold">6 + 1 Months</li>
                                                            <li class="transparent"><strong>

                                                                    <i class="fa fa-inr" aria-hidden="true"></i>
                                                                    24000&nbsp;</strong>
                                                            </li>
                                                            <li class="transparent2">
                                                                <strong>
                                                                    <strike>

                                                                        <i class="fa fa-inr" aria-hidden="true"></i>
                                                                        1500</strike>&nbsp;FREE&nbsp;

                                                                </strong>
                                                            </li>

                                                            <li class="transparent2">
                                                                <strong>
                                                                    2100 Gb
                                                                </strong>
                                                            </li>

                                                            <li class="transparent2">
                                                                <strong>
                                                                    4 Mbps
                                                                </strong>
                                                            </li>

                                                            <li class="transparent2">
                                                                <strong>
                                                                    <i class="fa fa-inr" aria-hidden="true"></i>
                                                                    24000
                                                                </strong>
                                                            </li>
                                                            <li><a href="#" target="_blank">
                                                                    <div
                                                                            class="order-button">
                                                                        Order Now »
                                                                    </div>
                                                                </a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="tab-pricing-col"
                                                         style="width: 28%;">
                                                        <ul>
                                                            <li class="bold">12 + 2 Months</li>
                                                            <li class="transparent"><strong>
                                                                    <i class="fa fa-inr" aria-hidden="true"></i>
                                                                    48000</strong></li>

                                                            <li class="transparent2">
                                                                <strong><strike><i class="fa fa-inr" aria-hidden="true"></i>
                                                                        2500</strike>&nbsp;FREE&nbsp;</strong>
                                                            </li>


                                                            <li class="transparent2"><strong>
                                                                    4200 Gb
                                                                    &nbsp;</strong></li>

                                                            <li class="transparent2"><strong>
                                                                    4 Mbps
                                                                    &nbsp;</strong></li>

                                                            <li class="transparent2">
                                                                <strong><i class="fa fa-inr" aria-hidden="true"></i>
                                                                    48000</strong>
                                                            </li>




                                                            <li><a href="#" target="_blank">
                                                                    <div
                                                                            class="order-button">
                                                                        Order Now »
                                                                    </div>
                                                                </a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="clear"><p style="position: relative; color: white; font-weight: bold; display: block;
                                                 padding: 10px;">
                                                            Above mentioned plans are exclusively
                                                            for residential purpose only and shall
                                                            not be resold or used for business
                                                            purpose.
                                                        </p></div>
                                                </div>


                                                </p>
                                            </div>
                                            <div><p>
                                                <div class="tab-header">
                                                                        <span class="subtitle"
                                                                              style="padding-top: 15px;">6 Mbps UNLIMITED</span>
                                                </div>
                                                <div class="tab-subheader">
                                                    <div class="tab-title-col"
                                                         style="width: 150px;">

                                                        <ul class="white">
                                                            <li class="bold"
                                                                style="padding: 17px;"></li>
                                                            <li class="transparent">Price/Renewal
                                                            </li>
                                                            <li class="transparent2">Installation
                                                                Charge
                                                            </li>

                                                            <li class="transparent2">Turbo Speed
                                                            </li>


                                                            <li class="transparent2">Unlimited Speed
                                                            </li>

                                                            <li class="transparent2">Total Package
                                                                Price
                                                            </li>
                                                        </ul>
                                                        <span style="font-size: smaller; font-weight: normal;color: white;
                                padding: 5px 10px; margin-top: 10px; margin-left: 5px;display: block;">Fair Usage Policy is applicable</span>
                                                    </div>
                                                    <div class="tab-pricing-col"
                                                         style="width: 27%;">
                                                        <ul>
                                                            <li class="bold">1 Month</li>
                                                            <li class="transparent"><strong><i class="fa fa-inr" aria-hidden="true"></i> 1000&nbsp;</strong>
                                                            </li>
                                                            <li class="transparent2">
                                                                <strong><strike><i class="fa fa-inr" aria-hidden="true"></i>
                                                                        2500</strike>&nbsp;FREE&nbsp;</strong>
                                                            </li>

                                                            <li class="transparent2">
                                                                <strong>Unlimited
                                                                </strong>
                                                            </li>

                                                            <li class="transparent2">
                                                                <strong>
                                                                    6 Mbps
                                                                </strong>
                                                            </li>

                                                            <li class="transparent2"><strong><i class="fa fa-inr" aria-hidden="true"></i>
                                                                    1000&nbsp;</strong></li>
                                                            <li><a href="#" target="_blank">
                                                                    <div
                                                                            class="order-button">
                                                                        Order Now »
                                                                    </div>
                                                                </a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="tab-pricing-col"
                                                         style="width: 28%;">
                                                        <ul>
                                                            <li class="bold">6 + 1 Months</li>
                                                            <li class="transparent"><strong><i class="fa fa-inr" aria-hidden="true"></i>
                                                                    6000&nbsp;</strong>
                                                            </li>
                                                            <li class="transparent2">
                                                                <strong>
                                                                    <strike>

                                                                        <i class="fa fa-inr" aria-hidden="true"></i>
                                                                        2500</strike>&nbsp;FREE&nbsp;

                                                                </strong>
                                                            </li>

                                                            <li class="transparent2">
                                                                <strong>
                                                                    Unlimited
                                                                </strong>
                                                            </li>

                                                            <li class="transparent2">
                                                                <strong>
                                                                    6 Mbps
                                                                </strong>
                                                            </li>

                                                            <li class="transparent2">
                                                                <strong>
                                                                    <i class="fa fa-inr" aria-hidden="true"></i>
                                                                    6000
                                                                </strong>
                                                            </li>
                                                            <li><a href="#" target="_blank">
                                                                    <div
                                                                            class="order-button">
                                                                        Order Now »
                                                                    </div>
                                                                </a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="tab-pricing-col"
                                                         style="width: 28%;">
                                                        <ul>
                                                            <li class="bold">12 + 2 Months</li>
                                                            <li class="transparent"><strong>
                                                                    <i class="fa fa-inr" aria-hidden="true"></i>
                                                                    12000&nbsp;</strong></li>

                                                            <li class="transparent2">
                                                                <strong><strike><i class="fa fa-inr" aria-hidden="true"></i>
                                                                        2500</strike>&nbsp;FREE&nbsp;</strong>
                                                            </li>


                                                            <li class="transparent2"><strong>
                                                                    Unlimited
                                                                    &nbsp;</strong></li>

                                                            <li class="transparent2"><strong>
                                                                    6 Mbps
                                                                    &nbsp;</strong></li>

                                                            <li class="transparent2">
                                                                <strong><i class="fa fa-inr" aria-hidden="true"></i>
                                                                    12000</strong>
                                                            </li>




                                                            <li><a href="#" target="_blank">
                                                                    <div
                                                                            class="order-button">
                                                                        Order Now »
                                                                    </div>
                                                                </a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="clear"><p style="position: relative; color: white; font-weight: bold; display: block;
                                                 padding: 10px;">
                                                            Above mentioned plans are exclusively
                                                            for residential purpose only and shall
                                                            not be resold or used for business
                                                            purpose.
                                                        </p></div>
                                                </div>


                                                </p>
                                            </div>
                                            <div><p>
                                                <div class="tab-header">
                                                                        <span class="subtitle"
                                                                              style="padding-top: 15px;">8 Mbps UNLIMITED</span>
                                                </div>
                                                <div class="tab-subheader">
                                                    <div class="tab-title-col"
                                                         style="width: 150px;">

                                                        <ul class="white">
                                                            <li class="bold"
                                                                style="padding: 17px;"></li>
                                                            <li class="transparent">Price/Renewal
                                                            </li>
                                                            <li class="transparent2">Installation
                                                                Charge
                                                            </li>

                                                            <li class="transparent2">Turbo Speed
                                                            </li>


                                                            <li class="transparent2">Unlimited Speed
                                                            </li>

                                                            <li class="transparent2">Total Package
                                                                Price
                                                            </li>
                                                        </ul>
                                                        <span style="font-size: smaller; font-weight: normal;color: white;
                                padding: 5px 10px; margin-top: 10px; margin-left: 5px;display: block;">Fair Usage Policy is applicable</span>
                                                    </div>
                                                    <div class="tab-pricing-col"
                                                         style="width: 27%;">
                                                        <ul>
                                                            <li class="bold">1 Month</li>
                                                            <li class="transparent"><strong><i class="fa fa-inr" aria-hidden="true"></i> 1500&nbsp;</strong>
                                                            </li>
                                                            <li class="transparent2">
                                                                <strong><strike><i class="fa fa-inr" aria-hidden="true"></i>
                                                                        2500</strike>&nbsp;FREE&nbsp;</strong>
                                                            </li>

                                                            <li class="transparent2">
                                                                <strong>Unlimited
                                                                </strong>
                                                            </li>

                                                            <li class="transparent2">
                                                                <strong>
                                                                    8 Mbps
                                                                </strong>
                                                            </li>

                                                            <li class="transparent2"><strong><i class="fa fa-inr" aria-hidden="true"></i>
                                                                    1500&nbsp;</strong></li>
                                                            <li><a href="#" target="_blank">
                                                                    <div
                                                                            class="order-button">
                                                                        Order Now »
                                                                    </div>
                                                                </a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="tab-pricing-col"
                                                         style="width: 28%;">
                                                        <ul>
                                                            <li class="bold">6 + 1 Months</li>
                                                            <li class="transparent"><strong><i class="fa fa-inr" aria-hidden="true"></i>
                                                                    9000&nbsp;</strong>
                                                            </li>
                                                            <li class="transparent2">
                                                                <strong>
                                                                    <strike>

                                                                        <i class="fa fa-inr" aria-hidden="true"></i>
                                                                        2500</strike>&nbsp;FREE&nbsp;

                                                                </strong>
                                                            </li>

                                                            <li class="transparent2">
                                                                <strong>
                                                                    Unlimited
                                                                </strong>
                                                            </li>

                                                            <li class="transparent2">
                                                                <strong>
                                                                    8 Mbps
                                                                </strong>
                                                            </li>

                                                            <li class="transparent2">
                                                                <strong>
                                                                    <i class="fa fa-inr" aria-hidden="true"></i>
                                                                    9000
                                                                </strong>
                                                            </li>
                                                            <li><a href="#" target="_blank">
                                                                    <div
                                                                            class="order-button">
                                                                        Order Now »
                                                                    </div>
                                                                </a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="tab-pricing-col"
                                                         style="width: 28%;">
                                                        <ul>
                                                            <li class="bold">12 + 2 Months</li>
                                                            <li class="transparent"><strong>
                                                                    <i class="fa fa-inr" aria-hidden="true"></i>
                                                                    18000&nbsp;</strong></li>

                                                            <li class="transparent2">
                                                                <strong><strike><i class="fa fa-inr" aria-hidden="true"></i>
                                                                        2500</strike>&nbsp;FREE&nbsp;</strong>
                                                            </li>


                                                            <li class="transparent2"><strong>
                                                                    Unlimited
                                                                    &nbsp;</strong></li>

                                                            <li class="transparent2"><strong>
                                                                    8 Mbps
                                                                    &nbsp;</strong></li>

                                                            <li class="transparent2">
                                                                <strong><i class="fa fa-inr" aria-hidden="true"></i>
                                                                    18000</strong>
                                                            </li>




                                                            <li><a href="#" target="_blank">
                                                                    <div
                                                                            class="order-button">
                                                                        Order Now »
                                                                    </div>
                                                                </a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="clear"><p style="position: relative; color: white; font-weight: bold; display: block;
                                                 padding: 10px;">
                                                            Above mentioned plans are exclusively
                                                            for residential purpose only and shall
                                                            not be resold or used for business
                                                            purpose.
                                                        </p></div>
                                                </div>


                                                </p>
                                            </div>


                                            <div><p>
                                                <div class="tab-header">
                                                                        <span class="subtitle"
                                                                              style="padding-top: 15px;">8 Mbps UNLIMITED</span>
                                                </div>
                                                <div class="tab-subheader">
                                                    <div class="tab-title-col"
                                                         style="width: 150px;">

                                                        <ul class="white">
                                                            <li class="bold"
                                                                style="padding: 17px;"></li>
                                                            <li class="transparent">Price/Renewal
                                                            </li>
                                                            <li class="transparent2">Installation
                                                                Charge
                                                            </li>

                                                            <li class="transparent2">Turbo Speed
                                                            </li>


                                                            <li class="transparent2">Unlimited Speed
                                                            </li>

                                                            <li class="transparent2">Total Package
                                                                Price
                                                            </li>
                                                        </ul>
                                                        <span style="font-size: smaller; font-weight: normal;color: white;
                                padding: 5px 10px; margin-top: 10px; margin-left: 5px;display: block;">Fair Usage Policy is applicable</span>
                                                    </div>
                                                    <div class="tab-pricing-col"
                                                         style="width: 27%;">
                                                        <ul>
                                                            <li class="bold">1 Month</li>
                                                            <li class="transparent"><strong><i class="fa fa-inr" aria-hidden="true"></i> 800&nbsp;</strong>
                                                            </li>
                                                            <li class="transparent2">
                                                                <strong><strike><i class="fa fa-inr" aria-hidden="true"></i>
                                                                        2500</strike>&nbsp;FREE&nbsp;</strong>
                                                            </li>

                                                            <li class="transparent2">
                                                                <strong>Unlimited
                                                                </strong>
                                                            </li>

                                                            <li class="transparent2">
                                                                <strong>
                                                                    8 Mbps
                                                                </strong>
                                                            </li>

                                                            <li class="transparent2"><strong><i class="fa fa-inr" aria-hidden="true"></i>
                                                                    1200&nbsp;</strong></li>
                                                            <li><a href="#" target="_blank">
                                                                    <div
                                                                            class="order-button">
                                                                        Order Now »
                                                                    </div>
                                                                </a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="tab-pricing-col"
                                                         style="width: 28%;">
                                                        <ul>
                                                            <li class="bold">6 + 1 Months</li>
                                                            <li class="transparent"><strong><i class="fa fa-inr" aria-hidden="true"></i>
                                                                    7200&nbsp;</strong>
                                                            </li>
                                                            <li class="transparent2">
                                                                <strong>
                                                                    <strike>

                                                                        <i class="fa fa-inr" aria-hidden="true"></i>
                                                                        2500</strike>&nbsp;FREE&nbsp;

                                                                </strong>
                                                            </li>

                                                            <li class="transparent2">
                                                                <strong>
                                                                    Unlimited
                                                                </strong>
                                                            </li>

                                                            <li class="transparent2">
                                                                <strong>
                                                                    8 Mbps
                                                                </strong>
                                                            </li>

                                                            <li class="transparent2">
                                                                <strong>
                                                                    <i class="fa fa-inr" aria-hidden="true"></i>
                                                                    7200
                                                                </strong>
                                                            </li>
                                                            <li><a href="#" target="_blank">
                                                                    <div
                                                                            class="order-button">
                                                                        Order Now »
                                                                    </div>
                                                                </a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="tab-pricing-col"
                                                         style="width: 28%;">
                                                        <ul>
                                                            <li class="bold">12 + 2 Months</li>
                                                            <li class="transparent"><strong>
                                                                    <i class="fa fa-inr" aria-hidden="true"></i>
                                                                    14400&nbsp;</strong></li>

                                                            <li class="transparent2">
                                                                <strong><strike><i class="fa fa-inr" aria-hidden="true"></i>
                                                                        2500</strike>&nbsp;FREE&nbsp;</strong>
                                                            </li>


                                                            <li class="transparent2"><strong>
                                                                    Unlimited
                                                                    &nbsp;</strong></li>

                                                            <li class="transparent2"><strong>
                                                                    8 Mbps
                                                                    &nbsp;</strong></li>

                                                            <li class="transparent2">
                                                                <strong><i class="fa fa-inr" aria-hidden="true"></i>
                                                                    14400</strong>
                                                            </li>




                                                            <li><a href="#" target="_blank">
                                                                    <div
                                                                            class="order-button">
                                                                        Order Now »
                                                                    </div>
                                                                </a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="clear"><p style="position: relative; color: white; font-weight: bold; display: block;
                                                 padding: 10px;">
                                                            Above mentioned plans are exclusively
                                                            for residential purpose only and shall
                                                            not be resold or used for business
                                                            purpose.
                                                        </p></div>
                                                </div>


                                                </p>
                                            </div>

                                        </div>


                                    </div>
                                    </p>
                                    </div>

                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

