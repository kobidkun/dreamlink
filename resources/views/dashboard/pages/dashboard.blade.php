
@extends('dashboard.structure');
@section('usertitle')
    Dashboard
@endsection
@section('content')
    <link href="{{ asset('dashboard/chart/morris.css')}}" rel="stylesheet">
    <link href="{{ asset('dashboard/chart/animate.css')}}" rel="stylesheet">
  {{--

    <link href="{{ asset('dashboard/chart/chartist-plugin-tooltip.css')}}" rel="stylesheet">

    <link href="{{ asset('dashboard/chart/cmGauge.css')}}" rel="stylesheet">

    --}}
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">Dashboard</h4> </div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">

                    <a href="#" target="_blank" class="btn btn-primary pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light">
                        Get Support</a>
                    <ol class="breadcrumb">
                        <li><a href="#">Dashboard</a></li>
                    </ol>
                </div>
                <!-- /.col-lg-12 -->
            </div>

            <div class="row">

                <div class="col-md-12 col-lg-4">
                    <div class="white-box">
                        <div class="row">
                            <div class="col-sm-6">
                                <h2 class="m-b-0 font-medium">{{$payments}}</h2>
                                <h5 class="text-muted m-t-0">Payments Recieved</h5></div>
                            <div class="row">
                                <div class="col-sm-4 col-xs-6"><span class="peity-bar"
                                                                     data-peity='{ "fill": ["#6164c1", "#13dafe"]}'
                                                                     data-width="100%"
                                                                     data-height="60">
                                        @foreach(  $payment as $paymentdetails  )
                                            {!!  $paymentdetails->id !!},
                                        @endforeach
                                        0

                                    </span></div>


                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-lg-4">
                    <div class="white-box">
                        <div class="row">
                            <div class="col-sm-6">
                                <h2 class="m-b-0 font-medium"><i class="fa fa-inr" aria-hidden="true"></i> {{$paymenttotal}}</h2>
                                <h5 class="text-muted m-t-0">Total Bill Amount</h5></div>
                            <div class="row">
                                <div class="col-sm-4 col-xs-6"><span class="peity-bar"
                                                                     data-peity='{ "fill": ["#f96262", "#f2f2f2"]}'
                                                                     data-width="100%" data-height="60">
                                        {{--30,2,8,4,-3,8,1,-3,6,-5,9,2,-8,1,4,8,9,8,2,1--}}
                                        @foreach(  $payment as $paymentdetails  )
                                            {!!  $paymentdetails->amount !!},
                                        @endforeach
                                        0

                                    </span></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-lg-4">
                    <div class="white-box">
                        <div class="row">
                            <div class="col-sm-6">
                                <h2 class="m-b-0 font-medium">{{ $failedpayment }}</h2>
                                <h5 class="text-muted m-t-0">Failed Payments</h5></div>
                            <div class="row">
                                <div class="col-sm-4 col-xs-6"><span class="peity-bar"
                                                                     data-peity='{ "fill": ["#4c5667", "#99d683"]}'
                                                                     data-width="100%"
                                                                     data-height="60">
                                         @foreach(  $failedpaymentlist as $paymentdetails  )
                                            {!!  $paymentdetails->amount !!},
                                        @endforeach
                                        0
                                    </span></div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- /.row -->

            <div class="row">
                <div class="col-md-12">
                    <div class="panel">
                        <div class="panel-heading">All Transaction</div>
                        <div class="table-responsive">
                            <table class="table table-hover manage-u-table">
                                <thead>
                                <tr>
                                    <th width="150" class="text-center">#TXT ID</th>
                                    <th>PAYMENT STATUS</th>
                                    <th>USERNAME</th>
                                    <th>AMOUNT</th>
                                    <th>DATE</th>
                                    <th width="150">MANAGE</th>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach(  $payment as $paymentdetails  )


                                    <tr>
                                        <td class="text-center">{{$paymentdetails->txnid}}</td>
                                        <td>
                                            @if ($paymentdetails->status === 'Failed')
                                                <span class="label label-danger">{{$paymentdetails->status}}</span>

                                            @else
                                                <span class="label label-primary">{{$paymentdetails->status}}</span>
                                            @endif


                                        </td>

                                        <td>{{Auth::user()->username}}
                                        </td>
                                        <td><i class="fa fa-inr" aria-hidden="true"></i> {{$paymentdetails->amount}}
                                        </td>

                                        <td>{{$paymentdetails->created_at}}
                                        </td>

                                        <td>
                                            <a href="{{ route('paymentdetails.show', $paymentdetails->id) }}">   <button type="button"
                                                                                                                         class="btn btn-primary btn-outline">View Details</button></a>

                                        </td>
                                    </tr>

                                @endforeach


                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>


        <!-- ============================================================== -->
        <!-- End Page Content -->
        <!-- ============================================================== -->
    </div>
    @endsection;

@section('footer');

<script src="{{ asset('dashboard/chart/jquery.peity.min.js')}}"></script>
<script src="{{ asset('dashboard/chart/jquery.peity.init.js')}}"></script>



@endsection


