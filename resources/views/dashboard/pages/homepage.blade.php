@extends('dashboard.structure');
@section('usertitle')
    Proceed to Payment
@endsection
@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">Dashboard</h4> </div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">

                    <a href="#" target="_blank" class="btn btn-primary pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light">
                        Get Support</a>
                    <ol class="breadcrumb">
                        <li><a href="#">Dashboard</a></li>
                    </ol>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <div class="row">
                <div class="col-md-8">
                    <div class="white-box">
                        <form
                                class="form-material form-horizontal"
                                role="form" method="get" action="{{ route('pay') }}"
                        >
                            {{ csrf_field() }}

                            <div class="form-group">
                                <label class="col-md-12">Your Full Name </label>
                                <div class="col-md-12">
                                    <input type="text"
                                           required
                                           name="name"
                                           class="form-control form-control-line"
                                           value="{{ Auth::user()->name }}">

                                    <input type="hidden"
                                           name="udf1"
                                           required
                                           class="form-control form-control-line"
                                           value="{{ Auth::user()->id }}">


                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-12">Your Email </label>
                                <div class="col-md-12">
                                    <input type="text" required  name="email"
                                           class="form-control form-control-line" value="{{ Auth::user()->email }}"> </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-12">Your Mobile </label>
                                <div class="col-md-12">
                                    <input type="text" required name="job_title"
                                           class="form-control form-control-line" value="{{ Auth::user()->mobile }}"></div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-12">Renew Date </label>
                                <div class="col-md-12">
                                    <input type="text"  required class="form-control form-control-line" value="{{ date('d-m-y') }}" ></div>
                            </div>


                            <div class="form-group">
                                <label class="col-md-12">Amount </label>
                                <div class="col-md-12">
                                    <input type="number" required name="amount" class="form-control form-control-line"  ></div>
                            </div>



                            <button class="btn btn-primary btn-block"><i class="fa fa-credit-card-alt"></i> Pay Now</button>
                        </form>

                    </div>
                </div>

                <div class="col-md-4">
                    <div class="white-box">
                        <h4 style="text-align: center">Available Payment Methods</h4>
                        <ul class="wallet-list">
                            <li><i style="color: #2D8EFC" class="icon-credit-card"></i><a href="javascript:void(0)">Debit Card</a></li>
                            <li><i style="color: #4FE140"  class="fa fa-credit-card-alt"></i><a href="javascript:void(0)">Credit Card</a></li>
                            <li><i style="color: #8C00F3"  class="icon-globe"></i><a href="javascript:void(0)">Internet Banking</a></li>
                            <li><i style="color: #FF004F"  class="ti-wallet"></i><a href="javascript:void(0)">Wallets</a></li>
                            <li><i style="color: #FFC22D"  class="fa fa-rupee"></i><a href="javascript:void(0)">UPI</a></li>

                        </ul>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
            <footer class="footer text-center">&copy; 2017 <img src="img/logo-tiny.png" alt="">Technologies Private Limited </footer>
        </div>
        <!-- ============================================================== -->
        <!-- End Page Content -->
        <!-- ============================================================== -->
    </div>
    @endsection;

@section('footer');



    @endsection