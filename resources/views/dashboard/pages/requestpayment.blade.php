@extends('dashboard.structure');
@section('usertitle')
    Payment Request of Rs {{ $data->amount  }}
@endsection
@section('content')
    <link href="dashboard/steps.css" rel="stylesheet">
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title"> Payment Request </h4> </div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">

                    <a href="#" target="_blank" class="btn btn-primary pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light">
                        Get Support</a>
                    <ol class="breadcrumb">
                        <li><a href="#">Payment Request</a></li>
                    </ol>
                </div>
                <!-- /.col-lg-12 -->
            </div>


            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-info" >
                        <div class="panel-heading" style="background-color: rgba(57,203,165,1)">
                         <span style="text-transform: uppercase; font-weight: 600;"> {{ $data->package  }}</span>   Renal Request of <i class="fa fa-inr" aria-hidden="true"></i> {{ $data->amount  }}
                        for Username {{ $data->username  }}
                        </div>
                        <div class="panel-wrapper collapse in" aria-expanded="true">
                            <div class="panel-body">
                                <form class="form-horizontal" role="form" method="get"
                                      action="{{ route('paynow') }}">
                                    <div class="form-body">
                                        <h3 class="box-title">Payment Details</h3>
                                        <hr class="m-t-0 m-b-40">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Name:</label>
                                                    <div class="col-md-9">
                                                        <p class="form-control-static"> {{ $data->name }} </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Mobile:</label>
                                                    <div class="col-md-9">
                                                        <p class="form-control-static"> {{ $data->mobile  }} </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <!--/row-->
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Email:</label>
                                                    <div class="col-md-9">
                                                        <p class="form-control-static"> {{ $data->email  }} </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Username:</label>
                                                    <div class="col-md-9">
                                                        <p class="form-control-static"> {{ $data->username  }}</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <!--/row-->
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Package:</label>
                                                    <div class="col-md-9">
                                                        <p class="form-control-static"> {{$data->package}}  </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Amount:</label>
                                                    <div class="col-md-9">
                                                        <p class="form-control-static"><i class="fa fa-inr" aria-hidden="true"></i> {{$data->amount}}  </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>


                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="col-md-12">
                                                    @if( $data->status === 'Failed')
                                                        <button type="submit" class="btn btn-block btn-success">
                                                            <i class="fa fa-tick"></i> Retry</button>
                                                        @elseif($data->status === 'Success')
                                                        <div class="row" style="margin-top: -200px">
                                                            <div class="col-md-4"></div>
                                                            <div class="col-md-4 " >
                                                                <img src="../../../images/paid.png"  height="250px" alt="">
                                                            </div>
                                                            <div class="col-md-4"></div>
                                                        </div>
                                                        @else
                                                        <button type="submit" class="btn btn-block btn-success">
                                                            <i class="fa fa-tick"></i> Pay Now</button>
                                                        @endif

                                                </div>
                                            </div>

                                            <!--/span-->
                                        </div>
                                        <!--/row-->





                                        </div>
                                        <!--/row-->


                                        <div class="row">
                                            <div class="col-md-12">
                                                {{ csrf_field() }}

                                                <input type="hidden" name="name" value="{{$data->name}} "  placeholder="Name" />
                                                <input type="hidden" name="reqid" value="{{$data->id}} "  placeholder="Name" />
                                                <input type="hidden" name="email" value="{{$data->email}}"  placeholder="Email" />
                                                <input type="hidden" name="package" value="{{$data->package}}"  placeholder="Name" />
                                                <input type="hidden" name="mobile" value="{{ $data->mobile }}"  placeholder="Name" />
                                                <input type="hidden" name="amount" value="{{ $data->amount }}" placeholder="Locality" />
                                                <input type="hidden" name="id"  value="{{ $data->user_id }}" />

                                            </div>


                                        </div>





                                    </div>

                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

            </div>
            <!-- /.container-fluid -->
            <footer class="footer text-center">&copy; 2017 <img src="img/logo-tiny.png" alt="">Technologies Private Limited </footer>
        </div>
        <!-- ============================================================== -->
        <!-- End Page Content -->
        <!-- ============================================================== -->
    </div>

@endsection;

@section('footer');




@endsection






