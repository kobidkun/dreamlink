@extends('dashboard.structure');
@section('usertitle')
    {{ Auth::user()->name }}'s  Profile
@endsection
@section('content')

    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">  {{ Auth::user()->name }}'s  Profile</h4> </div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">



                    <ol class="breadcrumb">
                        <li><a href="#">Dashboard</a></li>
                        <li class="active">  {{ Auth::user()->name }}'s  Profile</li>
                    </ol>
                </div>
            </div>
            <!-- /.row -->
            <!-- .row -->
            <div class="row">
                <div class="col-md-4 col-xs-12">
                    <div class="white-box">
                        <div class="user-bg"> <img width="100%" alt="user"
                                                   src="https://api.adorable.io/avatars/120/{{ Auth::user()->username }}.png">
                            <div class="overlay-box">
                                <div class="user-content">
                                    <a href="javascript:void(0)"><img src="https://api.adorable.io/avatars/120/{{ Auth::user()->username }}.png" class="thumb-lg img-circle" alt="img"></a>
                                    <h4 class="text-white">{{ Auth::user()->name }}</h4>
                                    <h5 class="text-white">{{ Auth::user()->username }}</h5>
                                    <h5 class="text-white">{{ Auth::user()->email }}</h5>

                                </div>
                            </div>
                        </div>
                        <div class="user-btm-box">
                            <div class="row">
                                <div class="col-md-12 col-xs-6 b-r"> <strong>Full Name</strong>
                                    <br>
                                    <p class="text-muted">{{ Auth::user()->email }}</p>
                                </div>
                                <div class="col-md-12 col-xs-12 b-r"> <strong>Mobile</strong>
                                    <br>
                                    <p class="text-muted">{{ Auth::user()->mobile }}</p>
                                </div>
                                <div class="col-md-12 col-xs-12 b-r"> <strong>Email</strong>
                                    <br>
                                    <p class="text-muted">{{ Auth::user()->email }}</p>
                                </div>
                                <div class="col-md-12 col-xs-12"> <strong>Location</strong>
                                    <br>
                                    <p class="text-muted">{{ Auth::user()->city }}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-8 col-xs-12">
                    <div class="white-box">


                                <form
                                        role="form" method="post"

                                        action="{{ route('profile.update', Auth::user()->id) }}"
                                        class="form-horizontal form-material">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="_method" value="PUT">
                                    <div class="form-group">
                                        <label class="col-md-12">Password
                                            <span style="color: rgba(255,73,97,1); font-weight: 200">(Required)</span></label>
                                        <div class="col-md-12">

                                            <input type="password"
                                                   required
                                                   name="password"
                                                   class="form-control form-control-line"> </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-12">Phone No</label>
                                        <div class="col-md-12">
                                            <input type="text" placeholder="{{ Auth::user()->mobile }}"
                                                   value="{{ Auth::user()->mobile }}"
                                                   name="mobile"
                                                   class="form-control form-control-line"> </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-12">Address</label>
                                        <div class="col-md-12">
                                            <input type="text"
                                                   name="address"
                                                   value="{{ Auth::user()->address }}"
                                                   placeholder="{{ Auth::user()->address }}" class="form-control form-control-line"> </div>
                                    </div>


                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <button class="btn btn-success">Update Profile</button>
                                        </div>
                                    </div>
                                </form>

                        </div>
                    </div>
                </div>
            </div>



            <!-- ============================================================== -->
    <!-- End Page Content -->
    <!-- ============================================================== -->
    </div>
    </div>
@endsection;

@section('footer');



@endsection