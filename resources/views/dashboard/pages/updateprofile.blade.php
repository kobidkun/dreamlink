@extends('dashboard.structure');
@section('usertitle')
Update Profile
@endsection
@section('content')
    <link href="dashboard/steps.css" rel="stylesheet">
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">Update Details</h4> </div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">

                    <a href="#" target="_blank" class="btn btn-primary pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light">
                        Get Support</a>
                    <ol class="breadcrumb">
                        <li><a href="#">Update Details</a></li>
                    </ol>
                </div>
                <!-- /.col-lg-12 -->
            </div>


            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-info" >
                        <div class="panel-heading" style="background-color: rgba(57,203,165,1)"> Verification</div>
                        <div class="panel-wrapper collapse in" aria-expanded="true">
                            <div class="panel-body">
                                <form class="form-horizontal" role="form" method="post"
                                      action="{{ route('update.profile.store.verify') }}">
                                    <div class="form-body">
                                        <h3 class="box-title">Person Info</h3>
                                        <hr class="m-t-0 m-b-40">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">First Name:</label>
                                                    <div class="col-md-9">
                                                        <p class="form-control-static"> {{ Auth::user()->name }} </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Mobile:</label>
                                                    <div class="col-md-9">
                                                        <p class="form-control-static"> {{ Auth::user()->mobile }} </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <!--/row-->
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Email:</label>
                                                    <div class="col-md-9">
                                                        <p class="form-control-static"> {{ Auth::user()->email }} </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Username:</label>
                                                    <div class="col-md-9">
                                                        <p class="form-control-static"> {{ Auth::user()->username }}</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <!--/row-->
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Account Type:</label>
                                                    <div class="col-md-9">
                                                        <p class="form-control-static"> {{ Auth::user()->acctype }} </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Phone:</label>
                                                    <div class="col-md-9">
                                                        <p class="form-control-static"> {{ Auth::user()->phone }}  </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <!--/row-->
                                        <h3 class="box-title">Address</h3>
                                        <hr class="m-t-0 m-b-40">

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Address:</label>
                                                    <div class="col-md-9">
                                                        <p class="form-control-static">
                                                            {{ Auth::user()->address }} </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">City:</label>
                                                    <div class="col-md-9">
                                                        <p class="form-control-static"> Siliguri </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">State:</label>
                                                    <div class="col-md-9">
                                                        <p class="form-control-static"> West Bengal </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->


                                        </div>
                                        <!--/row-->
                                        <h3 class="box-title">Password Change</h3>
                                        <hr class="m-t-0 m-b-40">

                                        <div class="row">
                                            <div class="col-md-9">
                                                {{ csrf_field() }}

                                                <input type="hidden" name="name" value="{{ Auth::user()->name }}"  placeholder="Name" />
                                                <input type="hidden" name="email" value="{{ Auth::user()->email }}"  placeholder="Email" />
                                                <input type="hidden" name="username" value="{{ Auth::user()->username }}"  placeholder="Name" />
                                                <input type="hidden" name="mobile" value="{{ Auth::user()->mobile }}"  placeholder="Name" />
                                                <input type="hidden" name="address" value="{{ Auth::user()->address }}" placeholder="Locality" />
                                                <input type="hidden" name="profile_saved"  value="1" />
                                                <input type="hidden" name="id"  value="{{ Auth::user()->id }}" />
                                                <input type="hidden" name="city" value="Siliguri" placeholder="City" />
                                                <input type="hidden" name="country" placeholder="India" value="India" />
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Enter New Password:</label>
                                                    <div class="col-md-9">
                                                        <input type="password" required name="password">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                            </div>

                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">

                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6">

                                            </div>
                                            <!--/span-->


                                        </div>

                                        {{--next--}}

                                    </div>
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <button type="submit" class="btn btn-info"> <i class="fa fa-tick"></i> Update and back to Dashboard</button>

                                            </div>
                                            <div class="col-md-6"> </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

            </div>
            <!-- /.container-fluid -->
            <footer class="footer text-center">&copy; 2017 <img src="img/logo-tiny.png" alt="">Technologies Private Limited </footer>
        </div>
        <!-- ============================================================== -->
        <!-- End Page Content -->
        <!-- ============================================================== -->
    </div>

@endsection;

@section('footer');




@endsection






