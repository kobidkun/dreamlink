@extends('dashboard.structure');
@section('usertitle')
    All Tranactions
@endsection
@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">Dashboard</h4></div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">

                    <a href="#" target="_blank"
                       class="btn btn-primary pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light">
                        Get Support</a>
                    <ol class="breadcrumb">
                        <li><a href="#">Dashboard</a></li>
                    </ol>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel">
                        <div class="panel-heading">All Transaction</div>
                        <div class="table-responsive">
                            <table class="table table-hover manage-u-table">
                                <thead>
                                <tr>
                                    <th width="150" class="text-center">#TXT ID</th>
                                    <th>PAYMENT STATUS</th>
                                    <th>USERNAME</th>
                                    <th>AMOUNT</th>
                                    <th>DATE</th>
                                    <th width="150">MANAGE</th>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach(  $payment as $paymentdetails  )


                                    <tr>
                                        <td class="text-center">{{$paymentdetails->txnid}}</td>
                                        <td>
                                            @if ($paymentdetails->status === 'Failed')
                                                <span class="label label-danger">{{$paymentdetails->status}}</span>

                                            @else
                                                <span class="label label-primary">{{$paymentdetails->status}}</span>
                                            @endif


                                        </td>

                                        <td>{{Auth::user()->username}}
                                        </td>
                                        <td><i class="fa fa-inr" aria-hidden="true"></i> {{$paymentdetails->amount}}
                                        </td>

                                        <td>{{$paymentdetails->created_at}}
                                        </td>

                                        <td>
                                            <a href="{{ route('paymentdetails.show', $paymentdetails->id) }}">   <button type="button"
                                                    class="btn btn-primary btn-outline">View Details</button></a>

                                        </td>
                                    </tr>

                                @endforeach


                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <!-- /.row -->


            <!-- /.container-fluid -->
            <footer class="footer text-center">&copy; 2017 <img src="img/logo-tiny.png" alt="">Technologies Private
                Limited
            </footer>
        </div>
        <!-- ============================================================== -->
        <!-- End Page Content -->
        <!-- ============================================================== -->
    </div>
@endsection;

@section('footer');



@endsection