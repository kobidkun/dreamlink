<!-- Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->
<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav slimscrollsidebar">
        <div class="sidebar-head">
            <h3><span class="fa-fw open-close"><i class="ti-close ti-menu"></i></span> <span class="hide-menu">Navigation</span></h3> </div>
        <div class="user-profile">
            <div class="dropdown user-pro-body">
                <div><img src="https://api.adorable.io/avatars/120/{{ Auth::user()->username }}.png" alt="user-img" width="30px" class="img-circle"></div>
                <a href="#" class="dropdown-toggle u-dropdown"
                   data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                    {{ Auth::user()->name }} </a>

            </div>
        </div>
        <ul class="nav" id="side-menu">

            <li> <a href="/manage" class="waves-effect "><i data-icon="&#xe026;" class="linea-icon linea-basic fa-fw"></i>
                    <span class="hide-menu"> Dashboard  </span></a>

            </li>

            <li> <a href="/home" class="waves-effect "><i  class="ti-credit-card"></i>
                    <span class="hide-menu"> Pay Now  </span></a>

            </li>

            <li> <a href="/payments" class="waves-effect "><i  class=" ti-list"></i>
                    <span class="hide-menu"> All Transactions  </span></a>

            </li>

            <li> <a href="/requested-payments" class="waves-effect "><i class="ti-check-box"></i>
                    <span class="hide-menu"> Requested Payments </span></a>

            </li>

            <li> <a href="/profile" class="waves-effect "><i  class="ti-user"></i>
                    <span class="hide-menu"> Profile </span></a>

            </li>



            <li> <a href="{{ route('logout') }}"
                    onclick="event.preventDefault();document.getElementById('logout-form').submit();"

                    class="waves-effect "><i class="fa fa-power-off"></i>
                    <span class="hide-menu"> Logout </span></a>

            </li>





        </ul>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Left Sidebar -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/pace.min.js"></script>
<style>
    .pace{-webkit-pointer-events:none;pointer-events:none;-webkit-user-select:none;-moz-user-select:none;user-select:none}.pace-inactive{display:none}.pace .pace-progress{background: #39cba5;position:fixed;z-index:2000;top:0;right:100%;width:100%;height:3px}
</style>