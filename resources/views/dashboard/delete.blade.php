
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="../plugins/images/favicon.png">
    <title>Manage Services Dreamlink</title>
    <!-- Bootstrap Core CSS -->
    <link href="dashboard/compiled.min.css" rel="stylesheet">


    <link href="css/colors/megna.css" id="theme" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body class="fix-header">
<!-- ============================================================== -->
<!-- Preloader -->
<!-- ============================================================== -->
<div class="preloader">
    <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/>
    </svg>
</div>
<!-- ============================================================== -->
<!-- Wrapper -->
<!-- ============================================================== -->
<div id="wrapper">
    <!-- ============================================================== -->
    <!-- Topbar header - style you can find in pages.scss -->logo-word.png
    <!-- ============================================================== -->
    <nav class="navbar navbar-default navbar-static-top m-b-0">
        <div class="navbar-header">
            <div class="top-left-part">
                <!-- Logo -->
                <a class="logo" href="/">
                    <!-- Logo icon image, you can use font-icon also --><b>
                        <!--This is dark logo icon-->
                        <img src="img/logo.png" alt="home" class="dark-logo" />
                        <!--This is light logo icon-->
                        <img src="img/logo-small.png" alt="home" class="light-logo" />
                    </b>
                    <!-- Logo text image you can use text also --><span class="hidden-xs">
                        <!--This is dark logo text-->
                        <img src="img/logo-word.png" alt="home" class="dark-logo" />
                        <!--This is light logo text-->
                        <img src="img/logo-word.png" alt="home" class="light-logo" />
                     </span> </a>
            </div>
            <!-- /Logo -->
            <!-- Search input and Toggle icon -->
            <ul class="nav navbar-top-links navbar-left">
                <li><a href="javascript:void(0)" class="open-close waves-effect waves-light"><i class="ti-menu"></i></a></li>
                <li class="dropdown">


                    <!-- /.dropdown-messages -->
                </li>


            </ul>
            <ul class="nav navbar-top-links navbar-right pull-right">
                <li>
                    <form role="search" class="app-search hidden-sm hidden-xs m-r-10">
                        <input type="text" placeholder="Search..." class="form-control"> <a href=""><i class="fa fa-search"></i></a> </form>
                </li>


                <!-- /.dropdown -->
            </ul>
        </div>
        <!-- /.navbar-header -->
        <!-- /.navbar-top-links -->
        <!-- /.navbar-static-side -->
    </nav>
    <!-- End Top Navigation -->
    <!-- ============================================================== -->
  @include('dashboard.components.left-nav')
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Page Content -->
    <!-- ============================================================== -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">Dashboard</h4> </div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">

                    <a href="#" target="_blank" class="btn btn-primary pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light">
                        Get Support</a>
                    <ol class="breadcrumb">
                        <li><a href="#">Dashboard</a></li>
                    </ol>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <div class="row">
                <div class="col-md-8">
                    <div class="white-box">
                        <form
                                class="form-material form-horizontal"
                              role="form" method="get" action="{{ route('pay') }}"
                        >
                            {{ csrf_field() }}


                            <div class="form-group">
                                <label class="col-md-12">Your Full Name </label>
                                <div class="col-md-12">
                                    <input type="text"
                                           name="name"
                                           class="form-control form-control-line"
                                           value="{{ Auth::user()->name }}"> </div>
                            </div>


                            <div class="form-group">
                                <label class="col-md-12">Your Full Name </label>
                                <div class="col-md-12">
                                    <input type="text"
                                           name="name"
                                           class="form-control form-control-line"
                                           value="{{ Auth::user()->id }}"> </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-12">Your Email </label>
                                <div class="col-md-12">
                                    <input type="text"  name="email"
                                           class="form-control form-control-line" value="{{ Auth::user()->email }}"> </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-12">Your Mobile </label>
                                <div class="col-md-12">
                                    <input type="text" required name="job_title"
                                           class="form-control form-control-line" value="{{ Auth::user()->mobile }}"></div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-12">Renew Date </label>
                                <div class="col-md-12">
                                    <input type="text"  class="form-control form-control-line" value="{{ date('d-m-y') }}" ></div>
                            </div>


                            <div class="form-group">
                                <label class="col-md-12">Amount </label>
                                <div class="col-md-12">
                                    <input type="number"  name="amount" class="form-control form-control-line"  ></div>
                            </div>



                            <button class="btn btn-primary btn-block"><i class="fa fa-credit-card-alt"></i> Pay Now</button>
                        </form>

                    </div>
            </div>

        </div>
        <!-- /.container-fluid -->
        <footer class="footer text-center">&copy; 2017 <img src="img/logo-tiny.png" alt="">Technologies Private Limited </footer>
    </div>
    <!-- ============================================================== -->
    <!-- End Page Content -->
    <!-- ============================================================== -->
</div>
<!-- /#wrapper -->
<!-- jQuery -->
<script src="dashboard/compiled.min.js"></script>

</body>

</html>