<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('/img/logo-small.png')}}">
    <title>@yield('admintitle') | Dreamlink Admin</title>
    <!-- Bootstrap Core CSS -->
    <link href="{{asset('dashboard/compiled.min.css')}}" rel="stylesheet">


    <link href="{{asset('css/colors/megna.css')}}" id="theme" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body class="fix-header">
<!-- ============================================================== -->
<!-- Preloader -->
<!-- ============================================================== -->
<div class="preloader">
    <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/>
    </svg>
</div>
<!-- ============================================================== -->
<!-- Wrapper -->
<!-- ============================================================== -->
<div id="wrapper">
    <!-- ============================================================== -->
    <!-- Topbar header - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <nav class="navbar navbar-default navbar-static-top m-b-0">
        <div class="navbar-header">
            <div class="top-left-part">
                <!-- Logo -->
                <a class="logo" href="/">
                    <!-- Logo icon image, you can use font-icon also -->
                        <!--This is dark logo icon--><img src="{{ asset('img/logo-small.png')}}" alt="home" class="dark-logo" />
                        <!--This is light logo icon--><img src="{{ asset('img/logo-small.png')}}" alt="home" class="light-logo" />

                    <!-- Logo text image you can use text also --><span class="hidden-xs">
                        <!--This is dark logo text--><img src="{{ asset('img/logo-word.png')}}"
                                                          alt="home" class="dark-logo" />
                        <!--This is light logo text--><img src="{{ asset('img/logo-word.png')}}" alt="home" class="light-logo" />
                     </span> </a>
            </div>
            <!-- /Logo -->
            <!-- Search input and Toggle icon -->
            <ul class="nav navbar-top-links navbar-left">


                <li>
                    <a href="javascript:void(0)"  style="top: 20px" class="open-close waves-effect waves-light">
                        <i  class="ti-menu"></i></a>
                </li>

                <!-- /.Megamenu -->
            </ul>
            <ul class="nav navbar-top-links navbar-right pull-right">
                <li>
                    <form role="search" class="app-search hidden-sm hidden-xs m-r-10">
                        <input type="text" placeholder="Search..." class="form-control"> <a href=""><i class="fa fa-search"></i></a> </form>
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle profile-pic"
                       data-toggle="dropdown"

                       href="#">
                        <img src="https://api.adorable.io/avatars/120/{{ Auth::user()->username }}.png" alt="user-img" width="30" class="img-circle">

                        <b class="hidden-xs">{{ Auth::user()->name }}</b><span class="caret"></span> </a>
                    <ul class="dropdown-menu dropdown-user animated flipInY">
                        <li>
                            <div class="dw-user-box">
                                <div class="u-img"><img
                                            src="https://api.adorable.io/avatars/200/{{ Auth::user()->username }}.png" alt="user" /></div>
                                <div class="u-text"><h4> {{ Auth::user()->name }} </h4>
                                    <p class="text-muted"> {{ Auth::user()->email }} </p>
                                    <a href="/profile" class="btn btn-rounded btn-danger btn-sm">
                                        View Profile</a></div>
                            </div>
                        </li>
                        <li role="separator" class="divider"></li>
                        <li><a href="/profile"><i class="ti-user"></i> My Profile</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="/profile"><i class="ti-settings"></i> Account Setting</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="{{ route('logout') }}"
                               onclick="event.preventDefault();document.getElementById('logout-form').submit();"><i class="fa fa-power-off"></i> Logout</a></li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>

                <!-- /.dropdown -->
            </ul>
        </div>
        <!-- /.navbar-header -->
        <!-- /.navbar-top-links -->
        <!-- /.navbar-static-side -->
    </nav>
    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        {{ csrf_field() }}
    </form>
    <!-- End Top Navigation -->
    <!-- ============================================================== -->
@include('dashboard.admin.components.left-nav')
<!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Page Content -->
    <!-- ============================================================== -->

    @yield('admincontent');


    <!-- /#wrapper -->
    <!-- jQuery -->
    <script src="{{asset('dashboard/compiled.min.js')}}"></script>
    @yield('adminfooter');

</body>

</html>