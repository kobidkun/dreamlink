@extends('dashboard.admin.structure');

@section('admintitle')
    Details of {{ $allusers->name }}
    @endsection
@section('admincontent')
    <link href="{{ asset('dashboard/admin/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />


    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">{{ $allusers->name }}'s Profile </h4> </div>


                    <ol class="breadcrumb">
                        <li><a href="#">Dashboard</a></li>
                        <li class="active">{{ $allusers->name }}'s Profile</li>
                    </ol>
                </div>
            </div>
            <!-- /.row -->
            <!-- .row -->
            <div class="row">
                <div class="col-md-4 col-xs-12">
                    <div class="white-box">
                        <div class="user-bg"> <img width="100%" alt="user"
                                                   src="https://api.adorable.io/avatars/120/{{ $allusers->username }}.png">
                            <div class="overlay-box">
                                <div class="user-content">
                                    <a href="javascript:void(0)"><img src="https://api.adorable.io/avatars/120/{{ $allusers->username }}.png" class="thumb-lg img-circle" alt="img"></a>
                                    <h4 class="text-white">{{ $allusers->name }}</h4>
                                    <h5 class="text-white">{{ $allusers->username }}</h5>
                                    <h5 class="text-white">{{ $allusers->email }}</h5>

                                </div>
                            </div>
                        </div>
                        <div class="user-btm-box">
                            <div class="row">
                                <div class="col-md-12 col-xs-6 b-r"> <strong>Full Name</strong>
                                    <br>
                                    <p class="text-muted">{{ $allusers->name }}</p>
                                </div>
                                <div class="col-md-12 col-xs-12 b-r"> <strong>Mobile</strong>
                                    <br>
                                    <p class="text-muted">{{$allusers->mobile }}</p>
                                </div>
                                <div class="col-md-12 col-xs-12 b-r"> <strong>Email</strong>
                                    <br>
                                    <p class="text-muted">{{ $allusers->email }}</p>
                                </div>
                                <div class="col-md-12 col-xs-12"> <strong>Location</strong>
                                    <br>
                                    <p class="text-muted">Siliguri</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>



                <div class="col-md-8 col-xs-12">
                                <div class="white-box">
                                    <ul class="nav nav-tabs tabs customtab">
                                        <li class="active tab">
                                            <a href="#home" data-toggle="tab">
                                                <span class="visible-xs">
                                                    <i class="fa fa-exchange"></i></span> <span class="hidden-xs"><i class="fa fa-exchange" aria-hidden="true"></i>  All Tranactions</span> </a>
                                        </li>
                                        <li class="tab">
                                            <a href="#profile" data-toggle="tab"> <span class="visible-xs"><i class="fa fa-user"></i></span> <span class="hidden-xs">
                                                    <i class="fa fa-user"></i> Profile</span> </a>
                                        </li>
                                        <li class="tab">
                                            <a href="#messages" data-toggle="tab" aria-expanded="true"> <span class="visible-xs"><i class="fa fa-unlock-alt"></i></span>
                                                <span class="hidden-xs"><i class="fa fa-unlock-alt" aria-hidden="true"></i> Change Password</span> </a>
                                        </li>
                                        <li class="tab">
                                            <a href="#settings" data-toggle="tab" aria-expanded="false"> <span class="visible-xs">
                                                    <i class="fa fa-inr"></i></span> <span class="hidden-xs"><i class="fa fa-inr" aria-hidden="true"></i> Request Payment</span> </a>
                                        </li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="home">

                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="white-box">
                                                        <h3 class="box-title m-b-0">Data Export</h3>
                                                        <p class="text-muted m-b-30">Export data to Copy, CSV, Excel, PDF & Print</p>
                                                        <div class="table-responsive">
                                                            <table id="example23" class="display nowrap" cellspacing="0" width="100%">
                                                                <thead>
                                                                <tr>
                                                                    <th>TXTID</th>
                                                                    <th>STATUS</th>
                                                                    <th>AMOUNT</th>
                                                                    <th>DETAILS</th>
                                                                </tr>
                                                                </thead>
                                                                <tfoot>
                                                                <tr>
                                                                    <th>TXTID</th>
                                                                    <th>STATUS</th>
                                                                    <th>AMOUNT</th>
                                                                    <th>DETAILS</th>
                                                                </tr>
                                                                </tfoot>
                                                                <tbody>
                                                                @foreach($payments as $payment)
                                                                    <tr>
                                                                        <td>{{$payment->txnid}}</td>
                                                                        <td>
                                                                            @if ($payment->status === 'Failed')
                                                                                <span class="label label-danger">{{$payment->status}}</span>

                                                                            @else
                                                                                <span class="label label-primary">{{$payment->status}}</span>
                                                                            @endif

                                                                        </td>
                                                                        <td><i class="fa fa-inr" aria-hidden="true"></i> {{$payment->amount}}</td>

                                                                        <td> <a href="{{ route('details.payment.admin', $payment->id) }}" target="_blank">
                                                                                <button type="button" class="btn btn-primary btn-outline">View Details</button></a>
                                                                        </td>
                                                                    </tr>
                                                                @endforeach
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="tab-pane" id="profile">

                                            <form
                                                    role="form" method="post"

                                                    action="{{ route('user.profile.update', $allusers->id) }}"
                                                    class="form-horizontal form-material">
                                                {{ csrf_field() }}
                                                <input type="hidden" name="_method" value="PUT">

                                                <div class="form-group">
                                                    <label class="col-md-12">Name</label>
                                                    <div class="col-md-12">
                                                        <input type="text" placeholder="{{ $allusers->name }}"
                                                               value="{{ $allusers->name }}"
                                                               name="name"
                                                               required
                                                               class="form-control form-control-line"> </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-12">Username</label>
                                                    <div class="col-md-12">
                                                        <input type="text" placeholder="{{ $allusers->username }}"
                                                               value="{{ $allusers->username }}"
                                                               name="username"
                                                               required
                                                               class="form-control form-control-line"> </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-12">Email</label>
                                                    <div class="col-md-12">
                                                        <input type="email" placeholder="{{ $allusers->email }}"
                                                               value="{{ $allusers->email }}"
                                                               name="email"
                                                               required
                                                               class="form-control form-control-line"> </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-12">Phone No</label>
                                                    <div class="col-md-12">
                                                        <input type="text" placeholder="{{ $allusers->mobile }}"
                                                               value="{{ $allusers->mobile }}"
                                                               name="mobile"
                                                               required
                                                               class="form-control form-control-line"> </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-12">Address</label>
                                                    <div class="col-md-12">
                                                        <input type="text"
                                                               name="address"
                                                               required
                                                               value="{{ $allusers->address }}"
                                                               placeholder="{{ $allusers->address }}" class="form-control form-control-line"> </div>
                                                </div>


                                                <div class="form-group">
                                                    <div class="col-sm-12">
                                                        <button class="btn btn-success">Update Profile</button>
                                                    </div>
                                                </div>
                                            </form>

                                        </div>
                                        <div class="tab-pane" id="messages">

                                            <form
                                                    role="form" method="post"

                                                    action="{{ route('user.password.update', $allusers->id) }}"
                                                    class="form-horizontal form-material">
                                                {{ csrf_field() }}
                                                <input type="hidden" name="_method" value="PUT">
                                                <div class="form-group">
                                                    <label class="col-md-12">Password
                                                        <span style="color: rgba(255,73,97,1); font-weight: 200">(Required)</span></label>
                                                    <div class="col-md-12">

                                                        <input type="password"
                                                               required
                                                               name="password"
                                                               class="form-control form-control-line"> </div>
                                                </div>





                                                <div class="form-group">
                                                    <div class="col-sm-12">
                                                        <button class="btn btn-success">Update Password</button>
                                                    </div>
                                                </div>
                                            </form>

                                        </div>
                                        <div class="tab-pane" id="settings">

                                            <form
                                                    role="form" method="post"

                                                    action="{{ route('admin.request.payment') }}"
                                                    class="form-horizontal form-material">
                                                {{ csrf_field() }}


                                                <div class="form-group">
                                                    <label class="col-md-12">Name</label>
                                                    <div class="col-md-12">
                                                        <input type="text" placeholder="{{ $allusers->name }}"
                                                               value="{{ $allusers->name }}"
                                                               name="name"
                                                               required
                                                               class="form-control form-control-line"> </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-12">Username</label>
                                                    <div class="col-md-12">
                                                        <input type="text" placeholder="{{ $allusers->username }}"
                                                               value="{{ $allusers->username }}"
                                                               name="username"
                                                               required
                                                               class="form-control form-control-line">

                                                        <input type="hidden" placeholder="{{ $allusers->id }}"
                                                               value="{{ $allusers->id }}"
                                                               name="user_id"
                                                               required
                                                               class="form-control form-control-line"> </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-12">Email</label>
                                                    <div class="col-md-12">
                                                        <input type="email" placeholder="{{ $allusers->email }}"
                                                               value="{{ $allusers->email }}"
                                                               name="email"
                                                               required
                                                               class="form-control form-control-line"> </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-12">Phone No</label>
                                                    <div class="col-md-12">
                                                        <input type="text" placeholder="{{ $allusers->mobile }}"
                                                               value="{{ $allusers->mobile }}"
                                                               name="mobile"
                                                               required
                                                               class="form-control form-control-line"> </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-12">Amount</label>
                                                    <div class="col-md-12">
                                                        <input type="text"
                                                               name="amount"
                                                               value=""
                                                               required
                                                               placeholder="100" class="form-control form-control-line"> </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-12">Package</label>
                                                    <div class="col-md-12">
                                                        <input type="text"
                                                               name="package"
                                                               value=""
                                                               required
                                                               placeholder="3 Mbps UL" class="form-control form-control-line"> </div>
                                                </div>


                                                <div class="form-group">
                                                    <div class="col-sm-12">
                                                        <button class="btn btn-success">Submit Payment Request</button>
                                                    </div>
                                                </div>
                                            </form>

                                        </div>


                                    </div>



                        </div>
                    </div>
                </div>
            </div>



            <!-- ============================================================== -->
    <!-- End Page Content -->
    <!-- ============================================================== -->
    </div>
    </div>
@endsection;

@section('adminfooter');
<script src="{{ asset('dashboard/admin/jquery.dataTables.min.js')}}"></script>
<!-- start - This is for export functionality only -->
<script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
<script>
    $(document).ready(function () {
        $('#myTable').DataTable();
        $(document).ready(function () {
            var table = $('#example').DataTable({
                "columnDefs": [
                    {
                        "visible": false
                        , "targets": 2
                    }
                ]
                , "order": [[2, 'asc']]
                , "displayLength": 25
                , "drawCallback": function (settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;
                    api.column(2, {
                        page: 'current'
                    }).data().each(function (group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                            last = group;
                        }
                    });
                }
            });
            // Order by the grouping
            $('#example tbody').on('click', 'tr.group', function () {
                var currentOrder = table.order()[0];
                if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                    table.order([2, 'desc']).draw();
                }
                else {
                    table.order([2, 'asc']).draw();
                }
            });
        });
    });
    $('#example23').DataTable({
        dom: 'Bfrtip'
        , buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
</script>



@endsection


         {{----}}

           {{-- <form
                    role="form" method="post"

                    action="{{ route('profile.update', Auth::user()->id) }}"
                    class="form-horizontal form-material">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="PUT">
                <div class="form-group">
                    <label class="col-md-12">Password
                        <span style="color: rgba(255,73,97,1); font-weight: 200">(Required)</span></label>
                    <div class="col-md-12">

                        <input type="password"
                               required
                               name="password"
                               class="form-control form-control-line"> </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">Phone No</label>
                    <div class="col-md-12">
                        <input type="text" placeholder="{{ Auth::user()->mobile }}"
                               value="{{ Auth::user()->mobile }}"
                               name="mobile"
                               class="form-control form-control-line"> </div>
                </div>

                <div class="form-group">
                    <label class="col-md-12">Address</label>
                    <div class="col-md-12">
                        <input type="text"
                               name="address"
                               value="{{ Auth::user()->address }}"
                               placeholder="{{ Auth::user()->address }}" class="form-control form-control-line"> </div>
                </div>


                <div class="form-group">
                    <div class="col-sm-12">
                        <button class="btn btn-success">Update Profile</button>
                    </div>
                </div>
            </form>--}}