@extends('dashboard.admin.structure');
@section('admintitle')
    Admin Dashboard
@endsection
@section('admincontent')
    <link href="{{ asset('dashboard/chart/morris.css')}}" rel="stylesheet">
    <link href="{{ asset('dashboard/chart/animate.css')}}" rel="stylesheet">
    <link href="{{ asset('dashboard/admin/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
  {{--

    <link href="{{ asset('dashboard/chart/chartist-plugin-tooltip.css')}}" rel="stylesheet">

    <link href="{{ asset('dashboard/chart/cmGauge.css')}}" rel="stylesheet">

    --}}
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">Dashboard</h4> </div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">

                    <a href="#" target="_blank" class="btn btn-primary pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light">
                        Get Support</a>
                    <ol class="breadcrumb">
                        <li><a href="#">Dashboard</a></li>
                    </ol>
                </div>
                <!-- /.col-lg-12 -->
            </div>

            <div class="row">

                <div class="col-md-12 col-lg-4">
                    <div class="white-box">
                        <div class="row">
                            <div class="col-sm-6">
                                <h2 class="m-b-0 font-medium">{{$payments}}</h2>
                                <h5 class="text-muted m-t-0">Payments Recieved</h5></div>
                            <div class="row">
                                <div class="col-sm-4 col-xs-6"><span class="peity-bar"
                                                                     data-peity='{ "fill": ["#6164c1", "#13dafe"]}'
                                                                     data-width="100%"
                                                                     data-height="60">
                                        @foreach(  $payment as $paymentdetails  )
                                            {!!  $paymentdetails->id !!},
                                        @endforeach
                                        0

                                    </span></div>


                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-lg-4">
                    <div class="white-box">
                        <div class="row">
                            <div class="col-sm-6">
                                <h2 class="m-b-0 font-medium"><i class="fa fa-inr" aria-hidden="true"></i> {{$paymenttotal}}</h2>
                                <h5 class="text-muted m-t-0">Total Bill Amount</h5></div>
                            <div class="row">
                                <div class="col-sm-4 col-xs-6"><span class="peity-bar"
                                                                     data-peity='{ "fill": ["#f96262", "#f2f2f2"]}'
                                                                     data-width="100%" data-height="60">
                                        {{--30,2,8,4,-3,8,1,-3,6,-5,9,2,-8,1,4,8,9,8,2,1--}}
                                        @foreach(  $payment as $paymentdetails  )
                                            {!!  $paymentdetails->amount !!},
                                        @endforeach
                                        0

                                    </span></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-lg-4">
                    <div class="white-box">
                        <div class="row">
                            <div class="col-sm-6">
                                <h2 class="m-b-0 font-medium">{{ $failedpayment }}</h2>
                                <h5 class="text-muted m-t-0">Failed Payments</h5></div>
                            <div class="row">
                                <div class="col-sm-4 col-xs-6"><span class="peity-bar"
                                                                     data-peity='{ "fill": ["#4c5667", "#99d683"]}'
                                                                     data-width="100%"
                                                                     data-height="60">
                                         @foreach(  $failedpaymentlist as $paymentdetails  )
                                            {!!  $paymentdetails->amount !!},
                                        @endforeach
                                        0
                                    </span></div>
                            </div>
                        </div>
                    </div>
                </div>



                <div class="col-md-12 col-lg-4">
                    <div class="white-box">
                        <div class="row">
                            <div class="col-sm-6">
                                <h2 class="m-b-0 font-medium">{{ $totalusers }}</h2>
                                <h5 class="text-muted m-t-0">Total Users</h5></div>
                            <div class="row">
                                <div class="col-sm-4 col-xs-6"><span class="peity-bar"
                                                                     data-peity='{ "fill": ["#5628e0", "#edbd2d"]}'
                                                                     data-width="100%"
                                                                     data-height="60">
                                         @foreach(  $getusers as $paymentdetails  )
                                            {!!  $paymentdetails->id !!},
                                        @endforeach
                                        0
                                    </span></div>
                            </div>
                        </div>
                    </div>
                </div>



                <div class="col-md-12 col-lg-4">
                    <div class="white-box">
                        <div class="row">
                            <div class="col-sm-6">
                                <h2 class="m-b-0 font-medium"> <i class="fa fa-inr" aria-hidden="true"></i> {{ $paymenttodaycount }}</h2>
                                <h5 class="text-muted m-t-0">Today's Payment</h5></div>
                            <div class="row">
                                <div class="col-sm-4 col-xs-6"><span class="peity-bar"
                                                                     data-peity='{ "fill": ["#5628e0", "#edbd2d"]}'
                                                                     data-width="100%"
                                                                     data-height="60">
                                         @foreach(  $paymenttoday as $paymentdetails  )
                                            {!!  $paymentdetails->amount !!},
                                        @endforeach
                                        0
                                    </span></div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="col-md-12 col-lg-4">
                    <div class="white-box">
                        <div class="row">
                            <div class="col-sm-6">
                                <h2 class="m-b-0 font-medium"> <i class="fa fa-inr" aria-hidden="true"></i> {{ $paymenttodaycount }}</h2>
                                <h5 class="text-muted m-t-0">Pending Payment Requested</h5></div>
                            <div class="row">
                                <div class="col-sm-4 col-xs-6"><span class="peity-bar"
                                                                     data-peity='{ "fill": ["#5628e0", "#edbd2d"]}'
                                                                     data-width="100%"
                                                                     data-height="60">
                                         @foreach(  $paymenttoday as $paymentdetails  )
                                            {!!  $paymentdetails->amount !!},
                                        @endforeach
                                        0
                                    </span></div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- /.row -->

            <div class="row">
                <div class="col-sm-12">
                    <div class="white-box">
                        <h3 class="box-title m-b-0">Data Export</h3>
                        <p class="text-muted m-b-30">Export data to Copy, CSV, Excel, PDF & Print</p>
                        <div class="table-responsive">
                            <table id="example23" class="display nowrap" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Username</th>
                                    <th>Contact</th>
                                    <th>Details</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>Name</th>
                                    <th>Username</th>
                                    <th>Contact</th>
                                    <th>Details</th>
                                </tr>
                                </tfoot>
                                <tbody>
                                @foreach($payment as $tranaction)
                                    <tr>
                                        <td>{{$tranaction->txnid}}</td>
                                        <td>{{$tranaction->firstname}}</td>
                                        <td>
                                            @if ($tranaction->status === 'pending')
                                                <span class="label label-warning">{{$tranaction->status}}</span>

                                            @elseif(($tranaction->status === 'Completed'))
                                                <span class="label label-primary">{{$tranaction->status}}</span>
                                            @else
                                                <span class="label label-danger">{{$tranaction->status}}</span>
                                            @endif


                                        </td>
                                        <td> <a href="{{ route('details.payment.admin', $tranaction->id) }}" target="_blank">
                                                <button type="button" class="btn btn-primary btn-outline">View Details</button></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>


        <!-- ============================================================== -->
        <!-- End Page Content -->
        <!-- ============================================================== -->
    </div>
    @endsection;

@section('adminfooter');

<script src="{{ asset('dashboard/chart/jquery.peity.min.js')}}"></script>
<script src="{{ asset('dashboard/chart/jquery.peity.init.js')}}"></script>
        <script src="{{ asset('dashboard/admin/jquery.dataTables.min.js')}}"></script>
        <!-- start - This is for export functionality only -->
        <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
        <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
        <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
        <script>
            $(document).ready(function () {
                $('#myTable').DataTable();
                $(document).ready(function () {
                    var table = $('#example').DataTable({
                        "columnDefs": [
                            {
                                "visible": false
                                , "targets": 2
                            }
                        ]
                        , "order": [[2, 'asc']]
                        , "displayLength": 25
                        , "drawCallback": function (settings) {
                            var api = this.api();
                            var rows = api.rows({
                                page: 'current'
                            }).nodes();
                            var last = null;
                            api.column(2, {
                                page: 'current'
                            }).data().each(function (group, i) {
                                if (last !== group) {
                                    $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                                    last = group;
                                }
                            });
                        }
                    });
                    // Order by the grouping
                    $('#example tbody').on('click', 'tr.group', function () {
                        var currentOrder = table.order()[0];
                        if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                            table.order([2, 'desc']).draw();
                        }
                        else {
                            table.order([2, 'dec']).draw();
                        }
                    });
                });
            });
            $('#example23').DataTable({
                dom: 'Bfrtip'
                , buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
        </script>



@endsection


