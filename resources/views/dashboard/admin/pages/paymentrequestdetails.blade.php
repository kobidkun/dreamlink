@extends('dashboard.admin.structure');
@section('admintitle')
    Payment Request  of {{$pay->name}}
@endsection
@section('admincontent')
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">Dashboard</h4></div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">

                    <a href="#" target="_blank"
                       class="btn btn-primary pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light">
                        Get Support</a>
                    <ol class="breadcrumb">
                        <li><a href="#">Dashboard</a></li>
                    </ol>
                </div>
                <!-- /.col-lg-12 -->
            </div>


            <div class="row">



                    <div class="col-md-12 col-xs-12">
                        <div class="white-box printableArea">

                            <div>
                                <!-- .row -->



                                </div>

                                <div class="row text-center m-t-10">
                                    <div class="col-md-2 b-r" style="margin-top: -35px">{!! QrCode::size(100)->generate($pay->id); !!}
                                        <br>
                                        <p style="font-weight: 600; margin-top: -20px"># {{$pay->id}}</p>

                                    </div>
                                    <div class="col-md-3 b-r"><strong>Name</strong>
                                        <p>{{ $pay->name }}</p>
                                    </div>
                                    <div class="col-md-3 b-r"><strong>User Name</strong>
                                        <p>{{ $pay->username }}</p>
                                    </div>

                                    <div class="col-md-2 b-r"><strong>Amount</strong>
                                        <p><i class="fa fa-inr"></i> {{ $pay->amount }}</p>
                                    </div>

                                    <div class="col-md-2 b-r"><strong>Date</strong>
                                        <p><i class="fa fa-date"></i> {{ $pay->created_at }}</p>
                                    </div>


                                </div>



                                <!-- /.row -->
                                <hr>
                                <!-- .row -->
                                <div class="row text-center m-t-10">
                                    <div class="col-md-6 b-r"><strong>Email ID</strong>
                                        <p>{{ $pay->email }}</p>
                                    </div>
                                    <div class="col-md-6"><strong>Phone</strong>
                                        <p>{{ $pay->mobile }}</p>
                                    </div>
                                </div>
                                <!-- /.row -->
                                <hr>
                                <!-- .row -->
                            <div class="row text-center m-t-10">
                                <div class="col-md-12 b-r"><strong>Status</strong><br>
                                    @if ($pay->status === 'pending')

                                                  <button  class="btn btn-warning btn-outline">
                         {{$pay->status}} </button>


                                    @else
                                        <button  class="btn btn-primary btn-outline">
                                            {{$pay->status}} </button>
                                    @endif

                                </div>

                            </div>
                                <hr>


                            <!-- .row -->

                                <!-- /.row -->

                            </div>
                        </div>
                <div class="col-md-12 col-xs-12" >
                    <div class="white-box" style="height: 100px">
                        <div class="col-md-3 col-xs-12">
                        <button id="print" class="btn btn-info btn-block" type="button">

                    <span><i class="fa fa-print"></i> Print</span> </button>
                        </div>

                        <div class="col-md-3 col-xs-12">
                <button  class="btn btn-primary btn-block" type="button"> <span>
                        <i class="fa fa-check"></i> Mark As Complete</span> </button>
                        </div>

                        <div class="col-md-3 col-xs-12">
                <button  class="btn btn-success btn-block" type="button"> <span>
                        <i class="fa fa-check-circle"></i> Mark As Processed</span> </button>
                        </div>


                        <div class="col-md-3 col-xs-12">
                <button  class="btn btn-danger btn-block" type="button"> <span>
                        <i class="fa fa-times"></i> Mark As Failed</span> </button>
                        </div>


                    </div>
                </div>
            </div>







            </div>




            <!-- /.row -->


            <!-- /.container-fluid -->
            <footer class="footer text-center">&copy; 2017 <img src="img/logo-tiny.png" alt="">Technologies Private
                Limited
            </footer>
        </div>
        <!-- ============================================================== -->
        <!-- End Page Content -->
        <!-- ============================================================== -->
    </div>
@endsection;

@section('adminfooter');

<script src="{{ asset('dashboard/jquery.PrintArea.js')}}"></script>

<script>
    $(document).ready(function () {
        $("#print").click(function () {
            var mode = 'iframe'; //popup
            var close = mode == "popup";
            var options = {
                mode: mode
                , popClose: close
            };
            $("div.printableArea").printArea(options);
        });
    });
</script>

@endsection