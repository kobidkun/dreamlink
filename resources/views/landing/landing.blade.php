
<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="author" content="SemiColonWeb" />

    <!-- Stylesheets
    ============================================= -->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{asset('landing/css/bootstrap.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('landing/css/style.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('landing/css/dark.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('landing/css/font-icons.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('landing/css/animate.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('landing/css/magnific-popup.css')}}" type="text/css" />

    <link rel="stylesheet" href="{{asset('landing/css/responsive.css')}}" type="text/css" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <!-- Document Title
    ============================================= -->
    <title>Welcome to Dreamlink Technologies</title>

</head>

<body class="stretched">

<!-- Document Wrapper
	============================================= -->
<div id="wrapper" class="clearfix">

    <!-- Header
    ============================================= -->
    <header id="header">

        <div id="header-wrap">

            <div class="container clearfix">

                <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

                <!-- Logo
                ============================================= -->
                <div id="logo">
                    <a href="/landingpage" class="standard-logo" data-dark-logo="landing/logo/logo.png">
                        <img src="landing/logo/logo.png" alt="Dreamlink"></a>
                    <a href="/landingpage" class="retina-logo"
                       data-dark-logo="landing/logo/logo.png"><img src="landing/logo/logo.png" alt="Dreamlink"></a>
                </div><!-- #logo end -->

                <!-- Primary Navigation
                ============================================= -->
                <nav id="primary-menu">

                    <ul class="one-page-menu">
                        <li class="current"><a href="#" data-href="#section-home"><div>Home</div></a></li>
                        <li><a href="#" data-href="#section-features"><div>Features</div></a></li>
                        <li><a href="#" data-href="#section-pricing"><div>Pricing</div></a></li>
                        <li><a href="#" data-href="#section-contact"><div>Contact</div></a></li>
                    </ul>

                </nav><!-- #primary-menu end -->

            </div>

        </div>

    </header><!-- #header end -->

    <section id="slider" style="background: url(landing/img/pexels-photo-374074.jpeg) center; overflow: visible;" data-height-lg="500"
             data-height-md="500" data-height-sm="600" data-height-xs="600" data-height-xxs="600">
        <div class="container clearfix">

            <div class="contact-widget">

                <div class=""></div>



                <form role="form" method="POST" action="{{ route('landingpage.saveform') }}"

                      class="landing-wide-form landing-form-overlay dark nobottommargin clearfix">

                    {{ csrf_field() }}
                    <div class="heading-block nobottommargin nobottomborder">
                        <h2>Enquire Now</h2>
                        <span>Book a New Internet Connection </span>
                    </div>
                    <div class="line" style="margin: 20px 0 30px;"></div>
                    <div class="col_full">
                        <input type="text" required name="name" class="form-control required input-lg not-dark"
                               value="" placeholder="Your Name*">
                    </div>
                    <div class="col_full">
                        <input type="text" required name="mobile" class="form-control required input-lg not-dark"
                               value="" placeholder="Mobile Number">
                    </div>

                    <div class="col_full">
                        <input type="text" name="email" class="form-control required input-lg not-dark"
                               value="" placeholder="Email (optional)">
                    </div>


                    <div class="col_full nobottommargin">
                       {{-- <button class="btn btn-lg btn-danger btn-block nomargin"
                                 type="submit">Book New Connection</button>--}}
                        <input type="submit" value="Submit & View All Plans" class="btn btn-lg btn-danger btn-block nomargin"  />
                    </div>
                </form>

            </div>

        </div>
    </section>

    <!-- Content
    ============================================= -->
    <section id="content" style="overflow: visible;">

        <div class="content-wrap">

            <div class="promo promo-dark promo-full landing-promo header-stick" style="background-color: #39cba5">
                <div class="container clearfix">
                    <h3>Only FTTH Internet Service in North Bengal  <i class="icon-circle-arrow-right" style="position:relative;top:2px;"></i></h3>
                    <span>Available all over Siliguri with Free Installation <i class="fa fa-phone " aria-hidden="true"></i> 0353 2501854 </span>
                </div>
            </div>

            <div class="container clearfix">

                <div class="clear bottommargin-lg"></div>

                <div id="section-features" class="heading-block title-center page-section">
                    <h2>Overview</h2>
                    <span>Service Available all over Siliguri</span>
                </div>

                <div class="col_one_third">
                    <div class="feature-box fbox-plain">
                        <div class="fbox-icon" data-animate="bounceIn">
                            <a href="#"><img src="landing/img/internet.png" alt="Responsive Layout"></a>
                        </div>
                        <h3>Blazing Fast Internet</h3>
                        <p>
                         Get Blazing Fast Internet with speed upto 100 Mbps.Our internet solutions are rated to be the fastest with minimal hang ups
                        </p>
                    </div>
                </div>

                <div class="col_one_third">
                    <div class="feature-box fbox-plain">
                        <div class="fbox-icon" data-animate="bounceIn" data-delay="200">
                            <a href="#"><img src="landing/img/support.png" alt="Retina Graphics"></a>
                        </div>
                        <h3>Dedicated Support</h3>
                        <p>24x7 Support are available by Professionals round the clock in providing support to all your requirements.
                        </p>
                    </div>
                </div>

                <div class="col_one_third col_last">
                    <div class="feature-box fbox-plain">
                        <div class="fbox-icon" data-animate="bounceIn" data-delay="400">
                            <a href="#"><img src="landing/img/install.png" alt="Powerful Performance"></a>
                        </div>
                        <h3>Free Installation</h3>
                        <p>
                            Get free installation on every new connection around Siliguri. Limited Period offer
                        </p>
                    </div>
                </div>







                <div class="clear"></div>

                <div class="divider divider-short divider-center"><i class="icon-circle"></i></div>

                <div id="section-pricing" class="heading-block title-center nobottomborder page-section">
                    <h2>Pricing Details</h2>
                    <span>We have Pricing plans for everyone. Pick a plan that suits your needs.</span>
                </div>
                <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

                <div class="pricing-box pricing-extended bottommargin clearfix">

                    <div class="pricing-desc">
                        <div class="pricing-title">
                            <h3>Home Plans Starting from  3 Mbps Unlimited</h3>
                        </div>
                        <div class="pricing-features">
                            <ul class="iconlist-color cleafix">
                                <li><i class="icon-desktop"></i> 3 Mbps UNLIMITED</li>
                                <li><i class="icon-eye-open"></i> Unlimited Bandwidth</li>
                                <li><i class="icon-magic"></i> Free Installation</li>
                                <li><i class="icon-file2"></i> Validity: 30 days</li>
                                <li><i class="icon-support"></i> 24x7 Support</li>
                                <li><i class="icon-beaker"></i>Discount on Quarterly & half yearly Packages </li>
                                <li><i class="icon-"></i> </li>
                                <br>
                                <br>

                            </ul>
                        </div>
                    </div>

                    <div class="pricing-action-area">
                        <div class="pricing-meta">
                            As Low as
                        </div>
                        <div class="pricing-price">
                            <span class="price-unit"><i class="fa fa-inr " aria-hidden="true"></i></span>700<span class="price-tenure">monthly</span>
                        </div>
                        <div class="pricing-action">
                            <a href="#" class="button button-3d button-large btn-block nomargin">Get Started</a>
                        </div>
                    </div>

                </div>
                <div class="pricing-box pricing-extended bottommargin clearfix">

                    <div class="pricing-desc">
                        <div class="pricing-title">
                            <h3>Business Plans Starting from  6 Mbps Unlimited </h3>
                        </div>
                        <div class="pricing-features">
                            <ul class="iconlist-color cleafix">
                                <li><i class="icon-desktop"></i> 6 Mbps UNLIMITED</li>
                                <li><i class="icon-eye-open"></i> Unlimited Bandwidth</li>
                                <li><i class="icon-magic"></i> Free Installation</li>
                                <li><i class="icon-file2"></i> Validity: 30 days</li>
                                <li><i class="icon-support"></i> 24x7 Support</li>
                                <li><i class="icon-beaker"></i>Discount on Quarterly & Half yearly Packages </li>
                                <li><i class="icon"></i> </li>
                                <br>
                                <br>

                            </ul>
                        </div>
                    </div>

                    <div class="pricing-action-area">
                        <div class="pricing-meta">
                            As Low as
                        </div>
                        <div class="pricing-price">
                            <span class="price-unit"><i class="fa fa-inr " aria-hidden="true"></i></span>1000<span class="price-tenure">monthly</span>
                        </div>
                        <div class="pricing-action">
                            <a href="#" class="button button-3d button-large btn-block nomargin">Get Started</a>
                        </div>
                    </div>

                </div>

                <div class="pricing-box pricing-extended bottommargin clearfix">

                    <div class="pricing-desc">
                        <div class="pricing-title">
                            <h3>100 Mbps Package </h3>
                        </div>
                        <div class="pricing-features">
                            <ul class="iconlist-color cleafix">
                                <li><i class="icon-desktop"></i> 100 Mbps Speed</li>
                                <li><i class="icon-eye-open"></i>Bandwidth: 250 GB</li>
                                <li><i class="icon-magic"></i> Free Installation</li>
                                <li><i class="icon-file2"></i> Validity: 30 days</li>
                                <li><i class="icon-support"></i> 24x7 Support</li>
                                <li><i class="icon-beaker"></i>Discount on Quarterly & Half yearly Packages </li>
                                <li><i class="icon"></i> </li>
                                <br>
                                <br>

                            </ul>
                        </div>
                    </div>

                    <div class="pricing-action-area">
                        <div class="pricing-meta">
                            As Low as
                        </div>
                        <div class="pricing-price">
                            <span class="price-unit"><i class="fa fa-inr " aria-hidden="true"></i></span>3500<span class="price-tenure">monthly</span>
                        </div>
                        <div class="pricing-action">
                            <a href="#" class="button button-3d button-large btn-block nomargin">Get Started</a>
                        </div>
                    </div>

                </div>




                <div class="clear"></div>

            </div>

            <div class="section">

                <div class="container clearfix">

                    <div id="section-testimonials" class="heading-block title-center page-section">
                        <h2>Testimonials</h2>
                        <span>So many people cannot be wrong</span>
                    </div>

                    <ul class="testimonials-grid grid-3 clearfix">
                        <li>
                            <div class="testimonial">
                                <div class="testi-image">
                                    <a href="#"><img src="landing/img/1.png" alt="Customer Testimonails"></a>
                                </div>
                                <div class="testi-content">
                                    <p>

                                        I was recommended to use wnet by my friend, fully satified with their service
                                    </p>
                                    <div class="testi-meta">
                                        Susmita

                                    </div>
                                </div>
                            </div>
                        </li>

                        <li>
                            <div class="testimonial">
                                <div class="testi-image">
                                    <a href="#"><img src="landing/img/1.png" alt="Customer Testimonails"></a>
                                </div>
                                <div class="testi-content">
                                    <p>

                                        With their unlimited peering my downloads are way faster . Keep the good service up
                                    </p>
                                    <div class="testi-meta">
                                        Anmol

                                    </div>
                                </div>
                            </div>
                        </li>


                        <li>
                            <div class="testimonial">
                                <div class="testi-image">
                                    <a href="#"><img src="landing/img/1.png" alt="Customer Testimonails"></a>
                                </div>
                                <div class="testi-content">
                                    <p>

                                        Great surfing speed & download speed. Best ISP in our area , highly recommended
                                    </p>
                                    <div class="testi-meta">
                                        Bikash

                                    </div>
                                </div>
                            </div>
                        </li>



                        <li>
                            <div class="testimonial">
                                <div class="testi-image">
                                    <a href="#"><img src="landing/img/1.png" alt="Customer Testimonails"></a>
                                </div>
                                <div class="testi-content">
                                    <p>

                                        With their unlimited peering my downloads are way faster . Keep the good service up
                                    </p>
                                    <div class="testi-meta">
                                        Ankita

                                    </div>
                                </div>
                            </div>
                        </li>



                        <li>
                            <div class="testimonial">
                                <div class="testi-image">
                                    <a href="#"><img src="landing/img/1.png" alt="Customer Testimonails"></a>
                                </div>
                                <div class="testi-content">
                                    <p>

                                        Plans of Wnet are very cheap with best possible rates & speed. No issues for the services
                                    </p>
                                    <div class="testi-meta">
                                        Subham Jain

                                    </div>
                                </div>
                            </div>
                        </li>


                        <li>
                            <div class="testimonial">
                                <div class="testi-image">
                                    <a href="#"><img src="landing/img/1.png" alt="Customer Testimonails"></a>
                                </div>
                                <div class="testi-content">
                                    <p>

                                        The internet speed is very fast , and their services are totally reliable. Internet plans are affordable with best speeds.
                                    </p>
                                    <div class="testi-meta">
                                        Anurag Das

                                    </div>
                                </div>
                            </div>
                        </li>

                    </ul>

                </div>

            </div>



            <div class="section footer-stick">

                <div class="container clearfix">

                    <div id="section-buy" class="heading-block title-center nobottomborder page-section">
                        <h2>Gets get in touch!</h2>
                        <span>Now that you have read all the Tid-Bits, so start</span>
                    </div>

                    <div class="center">

                        <a href="#" data-animate="tada" class="button button-3d button-teal button-xlarge nobottommargin">
                            <i class="icon-star3"></i>Book a New Connection</a> - OR -
                        <a href="#" data-scrollto="#section-pricing" class="button button-3d button-red button-xlarge nobottommargin">
                            <i class="icon-user2"></i>Back To Homepage</a>



                    </div>

                    <br>
                    <br>
                    <br>
                    <br>


                    <div class="center">

                        <div class="col_full">


                            <div class="contact-widget">

                                <div class="contact-form-result"></div>




                                <form class="nobottommargin"

                                      role="form" method="POST" action="{{ route('landingpage.saveform') }}"


                                >

                                    {{ csrf_field() }}

                                    <div class="form-process"></div>

                                    <div class="col_one_third">
                                        <label for="template-contactform-name">Name <small>*</small></label>
                                        <input type="text" name="name" value="" class="sm-form-control required" />
                                    </div>

                                    <div class="col_one_third">
                                        <label for="template-contactform-email">Email (optional) <small>*</small></label>
                                        <input type="text"  name="email" value="" class="email sm-form-control" />
                                    </div>

                                    <div class="col_one_third col_last">
                                        <label for="template-contactform-phone">Phone</label>
                                        <input type="text"  name="mobile" required value="" class="sm-form-control" />
                                    </div>

                                    <div class="clear"></div>

                                    <div class="col_full">
                                        <button type="submit" class="btn button
                                        button-rounded button-reveal button-large button-primary btn-lg btn-block">

                                            Book a New Internet Connection View All Plans

                                        </button>


                                    </div>

                                </form>
                            </div>

                        </div><!-- Contact Form End -->



                    </div>






                </div>

            </div>



        </div>

        <div class="container clearfix">

            <!-- Contact Form
            ============================================= -->

            <!-- Google Map
            ============================================= -->


            <!-- Contact Info
            ============================================= -->
            <div class="row clear-bottommargin">

                <div class="col-md-3 col-sm-6 bottommargin clearfix">
                    <div class="feature-box fbox-center fbox-bg fbox-plain">
                        <div class="fbox-icon">
                            <a href="#"><i class="icon-map-marker2"></i></a>
                        </div>
                        <h3>Our Office<span class="subtitle">Spencer Plaza, Burdwan Road, Siliguri </span></h3>
                    </div>
                </div>

                <div class="col-md-3 col-sm-6 bottommargin clearfix">
                    <div class="feature-box fbox-center fbox-bg fbox-plain">
                        <div class="fbox-icon">
                            <a href="#"><i class="icon-phone3"></i></a>
                        </div>
                        <h3>Speak to Us<span class="subtitle">+91 353 2501854/ 52</span></h3>
                    </div>
                </div>

                <div class="col-md-3 col-sm-6 bottommargin clearfix">
                    <div class="feature-box fbox-center fbox-bg fbox-plain">
                        <div class="fbox-icon">
                            <a href="#"><i class="icon-email"></i></a>
                        </div>
                        <h3>Email us<span class="subtitle">info@dreamlink.in</span></h3>
                    </div>
                </div>

                <div class="col-md-3 col-sm-6 bottommargin clearfix">
                    <div class="feature-box fbox-center fbox-bg fbox-plain">
                        <div class="fbox-icon">
                            <a href="#"><i class="icon-facebook2"></i></a>
                        </div>
                        <h3>Like on Facebook<span class="subtitle">Like Now</span></h3>
                    </div>
                </div>

            </div><!-- Contact Info End -->

        </div>


    </section><!-- #content end -->

    <!-- Footer
    ============================================= -->
    <footer id="footer" class="dark">



        <!-- Copyrights
        ============================================= -->
        <div id="copyrights">

            <div class="container clearfix">

                <div class="col_half">
                    Copyrights &copy; 2017 All Rights Reserved<br>
                    <div class="copyright-links"><a href="/terms-and-condition">Terms of Use</a> / <a href="/privacy">Privacy Policy</a></div>
                </div>

                <div class="col_half col_last tright">
                    <div class="fright clearfix">
                        <a href="https://www.facebook.com/dreamlinktechnologies/" target="_blank" class="social-icon si-small si-borderless si-facebook">
                            <i class="icon-facebook"></i>
                            <i class="icon-facebook"></i>
                        </a>
                    </div>

                    <div class="clear"></div>

                    <i class="icon-envelope2"></i> info@dreamlink.com <span class="middot">&middot;</span>
                    <i class="icon-headphones"></i> 0353 2501854 / 52 <span class="middot">&middot;</span>
                </div>

            </div>

        </div><!-- #copyrights end -->

    </footer><!-- #footer end -->

</div><!-- #wrapper end -->

{{--Chat Request--}}
<style>
    .flootingform {
        position: fixed;
        -moz-border-radius-topleft: 10px;
        -moz-border-radius-topright: 5px;
        height: 32px;
        width: 240px;
        background: #39cba5;
        z-index: 100;
        bottom: 0;
        right: 10px;
        border-radius: 25px 25px 0px 0px;

    }

    .flootingform p a{
        margin-left: 27px;
        margin-top: 5px;!important;
        text-align: center;
        align-content: center;
        align-items: center;
        color: #ffffff;!important;
        font-size: 18px;
        font-weight: 500;
        text-decoration: none;

    }

    .modalpopup {
        z-index: 1000;
        margin-top: 100px;

    }

    .flootingformright {
        position: fixed;
        -moz-border-radius-topleft: 10px;
        -moz-border-radius-topright: 5px;
        height: 40px;
        width: 200px;
        background: #39cba5;
        z-index: 100;
        bottom: 55%;
        left: -90px;
        -webkit-transform: rotate(90deg);
        -moz-transform: rotate(90deg);
        -o-transform: rotate(90deg);
        -ms-transform: rotate(90deg);
        transform: rotate(90deg);


    }

    .flootingformright p a{
        margin-left: 27px;
        margin-top: 5px;!important;
        text-align: center;
        align-content: center;
        align-items: center;
        color: #ffffff;!important;
        font-size: 18px;
        font-weight: 500;
        text-decoration: none;

    }

    .modal-backdrop {
        z-index: 30;
    }

</style>




<div class="flootingform">
    <div style="height: 3px"></div>
    <p><a data-toggle="modal" href="#" data-target="#myModal">
            <i class="fa fa-phone" aria-hidden="true"></i> Book Now</a></p>
</div>
<div id="myModal" class="modal modalpopup " role="dialog">
    <div class="modal-dialog">

        <div class="modal-content modalpopup">
            <div class="modal-header" style="background-color: #39cba5">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" style="color: #ffffff;">Quick Booking Form</h4>
            </div>
            <form role="form" method="POST" action="{{ route('landingpage.saveform') }}">

                <div class="modal-body">
                    {{ csrf_field() }}
                    <input type="text" required name="name" class="form-control required input-lg not-dark"
                           value="" placeholder="Your Name*">
                    <br>
                    <input type="text" required name="mobile" class="form-control required input-lg not-dark"
                           value="" placeholder="Mobile Number">
                    <br>
                    <input type="text" name="email" class="form-control required input-lg not-dark"
                           value="" placeholder="Email (optional)">

                </div>
                <div class="modal-footer">
                    <button type="submit" style="background-color: #00D7A5; border-color: #39cba5" class="btn btn-primary btn-block">
                        <i class="fa fa-envelope"></i> Book Now
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>


{{--end Chat--}}

<!-- Go To Top
============================================= -->
<div id="gotoTop" class="icon-angle-up"></div>

<!-- External JavaScripts
============================================= -->
<script type="text/javascript" src="{{asset('landing/js/jquery.js')}}"></script>
<script type="text/javascript" src="{{asset('landing/js/plugins.js')}}"></script>
<!-- Facebook Pixel Code -->
<script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window, document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '138099013528935');
    fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
               src="https://www.facebook.com/tr?id=138099013528935&ev=PageView&noscript=1"
    /></noscript>
<!-- End Facebook Pixel Code -->


<!-- Footer Scripts
============================================= -->
<script type="text/javascript" src="{{asset('landing/js/functions.js')}}"></script>

<script type="text/javascript">
    $(function() {
        $( "#side-navigation" ).tabs({ show: { effect: "fade", duration: 400 } });
    });
</script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-110170833-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-110170833-1');
</script>


</body>
</html>