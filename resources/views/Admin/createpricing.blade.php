@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Create New Pricing Table</div>

                    <div class="panel-body">
                        <form class="form-horizontal">
                            {{ csrf_field() }}
{{--form element--}}
                            <div class="form-group">
                                <label for="email" class="col-md-4 control-label">Package Name</label>

                                <div class="col-md-6">
                                    <input
                                           type="text"
                                           class="form-control"
                                           name="plan_name"
                                           required
                                           autofocus
                                    >

                                </div>
                            </div>


                            <div class="form-group">
                                <label for="email"
                                       class="col-md-4 control-label">
                                    Package Pricing
                                </label>

                                <div class="col-md-6">
                                    <input
                                           type="text"
                                           class="form-control"
                                           name="price"
                                           required
                                           autofocus
                                    >

                                </div>
                            </div>


                            <div class="form-group">
                                <label for="email"
                                       class="col-md-4 control-label">
                                    Package Validity
                                </label>

                                <div class="col-md-6">
                                    <input
                                           type="text"
                                           class="form-control"
                                           name="validity"
                                           required
                                           autofocus
                                    >

                                </div>
                            </div>

                            {{--form element--}}
                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Add new  Package
                                    </button>

                                </div>
                            </div>

                            {{--form element--}}
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection