

@extends('structure')
@section('content')



    <div id="container">

        <div id="container-inside">
            <table width="1100" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="544" align="left" valign="top">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td>
                                    <div class="subtitle">
                                        <div id="breadcrumbs"><a href="#" class="subtitle-link">Support </a>&gt;Email
                                            Setup
                                        </div>
                                    </div>
                                    <br/>
                                    <table width="556" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td colspan="3"><strong> DNS Settings (Domain Name Servers)<br/>
                                                    <br/>
                                                </strong></td>
                                        </tr>
                                        <tr>
                                            <td width="155" align="left" valign="top">Servers<br/>
                                                <br/>
                                                DNS Server
                                            </td>
                                            <td width="200" align="left" valign="top">IP Address<br/>
                                                <br/>
                                                202.79.32.4
                                            </td>
                                            <td width="201" align="left" valign="top">Description<br/>
                                                <br/>
                                                ns1.wlink.com.np
                                            </td>
                                        </tr>
                                    </table>
                                    <br/>
                                    <br/>
                                    <table width="556" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td colspan="2"><strong> Email Settings<br/>
                                                    <br/>
                                                </strong></td>
                                        </tr>
                                        <tr>
                                            <td width="290" align="left" valign="top">Incoming Mail Server (POP3)</td>
                                            <td width="240" align="left" valign="top">pop3.wlink.com.np <br/> pop3 port:
                                                110
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top">Incoming Mail Server (IMAP)</td>
                                            <td align="left" valign="top">imap.wlink.com.np <br/>imap port: 143</td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top">Outgoing Mail Server: (SMTP)</td>
                                            <td align="left" valign="top">smtp.wlink.com.np </br> smtp port: 25</td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top">Outgoing Mail Server: (SSMTP)</td>
                                            <td align="left" valign="top">ssmtp.wlink.com.np<br/>
                                                ssmtp port: 465<br/>
                                                for more detail <a href="configemail_ssmtp.php">CLICK HERE</a></td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top">Account name</td>
                                            <td align="left" valign="top">Your WorldLink username (the part before
                                                &quot;@&quot;
                                                on your e-mail address, for e.g., username is support for address
                                                support@wlink.com.np)<br/>
                                                <br/></td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top">Password</td>
                                            <td align="left" valign="top">Your account password</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" align="left" valign="top"><br/>
                                                Note: WorldLink will provide you support services on issues related to
                                                configuration and troubleshooting on only the e-mail software mentioned
                                                below
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top">&nbsp;</td>
                                            <td align="left" valign="top">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" align="left" valign="top"><strong>Windows:</strong><br/>
                                                1. Outlook Express<br/>
                                                2. Microsoft Outlook<br/>
                                                3. Eudora Pro<br/>
                                                4. Mozilla Thunderbird
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top">&nbsp;</td>
                                            <td align="left" valign="top">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" align="left" valign="top"><strong>Macintosh:</strong><br/>
                                                1. Mail (default Mail client)<br/>
                                                <br/>
                                                <a href="configure_email.php"
                                                   target="_blank" style="color: #0A246A;"><strong>Click
                                                        Here</strong></a> for your e-mail software
                                                configuration.<br><br/>
                                                We will not provide support services on e-mail software other than the
                                                ones
                                                mentioned above. You are requested to configure or check settings using
                                                the
                                                above information on your own.
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top">&nbsp;</td>
                                            <td align="left" valign="top">&nbsp;</td>
                                        </tr>
                                    </table>
                                    <p><br/>
                                    </p></td>
                            </tr>
                        </table>
                    </td>
                    <td width="32" align="left" valign="top">&nbsp;</td>
                    <td width="500" align="left" valign="top">
                        <table width="500" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="524" align="center"><img src="images/setting.jpg" width="417" height="442"/>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="box"><strong>Contact Customer Care</strong><br/>
                                        Call 9801523051 <br/>
                                        Email us: support@worldlink.com.np
                                        <br/>
                                        <a href="tech_support_form.php" class="link">Online Technical Support
                                            Form</a><br/>

                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <!-- <tr>
                               <td><div class="box">
                                 <strong>For Accounting Issues</strong>
                                 <br />
                                 E-mail: account@wlink.com.np<br />
                       Tel. 5523050, Ext. 405</div></td>
                             </tr>
                             <tr>
                               <td>&nbsp;</td>
                             </tr>-->
                        </table>
                    </td>
                </tr>
            </table>
        </div>


        <br/> <br/>




@endsection