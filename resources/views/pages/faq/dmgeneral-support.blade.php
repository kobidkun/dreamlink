@section('title', 'General support and frequently asked question')
@extends('structure')
@section('content')


@section('header-css')

@stop


<script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script>
    var menu=function(){
        var t=15,z=50,s=6,a;
        function dd(n){this.n=n; this.h=[]; this.c=[]}
        dd.prototype.init=function(p,c){
            a=c; var w=document.getElementById(p), s=w.getElementsByTagName('ul'), l=s.length, i=0;
            for(i;i<l;i++){
                var h=s[i].parentNode; this.h[i]=h; this.c[i]=s[i];
                h.onmouseover=new Function(this.n+'.st('+i+',true)');
                h.onmouseout=new Function(this.n+'.st('+i+')');
            }
        }
        dd.prototype.st=function(x,f){
            var c=this.c[x], h=this.h[x], p=h.getElementsByTagName('a')[0];
            clearInterval(c.t); c.style.overflow='hidden';
            if(f){
                p.className+=' '+a;
                if(!c.mh){c.style.display='block'; c.style.height=''; c.mh=c.offsetHeight; c.style.height=0}
                if(c.mh==c.offsetHeight){c.style.overflow='visible'}
                else{c.style.zIndex=z; z++; c.t=setInterval(function(){sl(c,1)},t)}
            }else{p.className=p.className.replace(a,''); c.t=setInterval(function(){sl(c,-1)},t)}
        }
        function sl(c,f){
            var h=c.offsetHeight;
            if((h<=0&&f!=1)||(h>=c.mh&&f==1)){
                if(f==1){c.style.filter=''; c.style.opacity=1; c.style.overflow='visible'}
                clearInterval(c.t); return
            }
            var d=(f==1)?Math.ceil((c.mh-h)/s):Math.ceil(h/s), o=h/c.mh;
            c.style.opacity=o; c.style.filter='alpha(opacity='+(o*100)+')';
            c.style.height=h+(d*f)+'px'
        }
        return{dd:dd}
    }();
</script>

<script type="text/javascript">
    <!--
    function toggle_visibility(id) {
        var e = document.getElementById(id);
        if(e.style.display == 'block')
            e.style.display = 'none';
        else
            e.style.display = 'block';
    }
    //-->
</script>


<style>
    .js .panel-title {
        margin: 0;
    }

    .panel-title a {
        border-bottom: none;
        color: #ffffff;
        display: block;
        padding: 1.25em 0;
        position: relative;
        text-decoration: none;
        -webkit-transition: color 200ms ease 0s;
        -moz-transition: color 200ms ease 0s;
        transition: color 200ms ease 0s;
        width: 95%;
    }

    .panel-title a .icon {
        color: #fff;
        position: absolute;
        right: 0;
        -webkit-transition: all 200ms ease 0s;
        -moz-transition: all 200ms ease 0s;
        transition: all 200ms ease 0s;
        margin-right: 20px;
        margin-left: 10px;
    }

    .panel-title a:hover,
    .panel-title a:focus {
        color: #37474f;
    }

    .panel-title a:hover .icon,
    .panel-title a:focus .icon {
        color: #242424;
        margin-right: 0px;

    }

    .panel-title a.active {
        color: #37474f;
    }

    .panel-title a.active .icon {
        color: #242424;
        -webkit-transform: rotate(45deg);
        -moz-transform: rotate(45deg);
        transform: rotate(45deg);
        margin-right: 0px;

    }

    .js .accordion {
        border-bottom: 1px solid #ececec;
        width: 500px;
        text-align: justify;
        font-size: 14px !important;

    }

    .accordion .panel-title a {
        border-top: 1px solid #ececec;
        background-color: #39cc70;
        padding: 20px;
    }

    [id^="panel-"] {
        padding-bottom: 2em;
    }

    h3 {
        font-size: 14px;
    }

    .faqImg {
        width: 100%;
        padding-right: 5%;
        padding-bottom: 2px;
        margin-top: 5px;
        margin-bottom: 5px;
    }

    .ul {
        padding: 10px 0px 10px 20px;
    }

</style>


<div id="container">


    <div class="content-box">
        <table width="1100" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td width="500" align="left" valign="top">
                    <div id="breadcrumbs" class="subtitle"><a href="support.php" class="subtitle-link">Support </a>&gt;Support
                        FAQs
                    </div>

                    <table width="450" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="450">
                                <div class="accordion">
                                    <h3 class="panel-title">How do I check my Internet usage?</h3>
                                    <div class="panel-content">

                                        <ul class="ul">
                                            <li>Open your Internet Browser [Google Chrome, Firefox or Safari etc.]</li>
                                            <li>Go to <a class="link" href="http://www.dreamlink.in" target="_blank">www.dreamlink.in</a>
                                            </li>
                                            <li>After the page opens, click on “Online Services” link at the top.</li>
                                        </ul>

                                        <img class="faqImg" src="img/faq/header.jpeg">
                                        <ul class="ul">
                                            <li> Enter “Username” and “Password” given to you by Dreamlink. Select on
                                                <strong>I’m not a robot</strong> option and click on
                                                <strong>Submit</strong></li>
                                        </ul>
                                        <img class="faqImg" src="img/faq/login.jpeg">
                                        <span style="font-style: italic">Note: Password is sent to you through registered  Email during service installation</span>
                                        <ul class="ul">
                                            <li>After logging-in, click on <strong>Online Services </strong>from left
                                                menu and click on <strong>Check Internet Usage</strong></li>
                                        </ul>
                                        <img class="faqImg" src="img/faq/onlineservice.jpeg">
                                        <ul class="ul">
                                            <li>Click on desired month for which you would like to check the internet
                                                usage and click on <strong>Check Usage</strong></li>
                                        </ul>
                                        <img class="faqImg" src="img/faq/internetusage.jpeg">
                                        <ul class="ul">
                                            <li>Finally, you can view details of your internet connection for desired
                                                month and download it by clicking on <strong>Download</strong></li>
                                        </ul>
                                        <img class="faqImg" src="img/faq/usage.png">
                                        <span style="font-style: italic;"><strong>Note:</strong> You can also check internet usage from <strong>myDreamlink</strong> mobile app available on Playstore and App Store.</span>

                                    </div>



                                    <h3 class="panel-title">What kind of ‘Traffic’ over the internet consumes
                                        volume?</h3>
                                    <div class="panel-content">
                                        <p style="padding:10px 0px 10px 5px;">‘Traffic’ is the data transferred both to
                                            and from your computer. Some of the applications/usage that generates
                                            traffic and consumes your subscribed volume is listed below. <br/><br/>

                                        <table width="450" border="0">
                                            <tr>
                                                <td bgcolor="#D0D8E8" style="padding-left:5px;"><strong>Traffic</strong>
                                                </td>
                                                <td bgcolor="#E9EDF4" style="padding:0px 5px;"><strong>Generated
                                                        while….</strong></td>
                                            </tr>
                                            <tr>
                                                <td bgcolor="#D0D8E8" style="padding-left:5px;">The Web (HTTP/HTTPS)
                                                </td>
                                                <td bgcolor="#E9EDF4" style="padding:0px 5px;">…viewing web pages,
                                                    social networking sites, downloading materials.
                                                </td>
                                            </tr>
                                            <tr>
                                                <td bgcolor="#D0D8E8" style="padding-left:5px;">E-Mail (SMTP/POP/IMAP)
                                                </td>
                                                <td bgcolor="#E9EDF4" style="paddingt:0px 5px;">...sending/receiving
                                                    e-mail messages.
                                                </td>
                                            </tr>
                                            <tr>
                                                <td bgcolor="#D0D8E8" style="padding-left:5px;">Instant Messaging</td>
                                                <td bgcolor="#E9EDF4" style="padding:0px 5px;">…using Skype, Windows
                                                    Live Messenger, Yahoo Messenger, eBuddy, Meebo etc
                                                </td>
                                            </tr>
                                            <tr>
                                                <td bgcolor="#D0D8E8" style="padding-left:5px;">Peer2Peer</td>
                                                <td bgcolor="#E9EDF4" style="padding:0px 5px;">… using Applications like
                                                    uTorrent, BitTorrent, BitComet, Frostwire, Kazaa etc.
                                                </td>
                                            </tr>
                                            <tr>
                                                <td bgcolor="#D0D8E8" style="padding-left:5px;">File Transfer (FTP)</td>
                                                <td bgcolor="#E9EDF4" style="padding:0px 5px;">...downloading, uploading
                                                    or viewing files through FTP connection.
                                                </td>
                                            </tr>
                                            <tr>
                                                <td bgcolor="#D0D8E8" style="padding-left:5px;">Antivirus, Applications
                                                    & OS Update
                                                </td>
                                                <td bgcolor="#E9EDF4" style="padding:0px 5px;">…updating Virus
                                                    definitions and applications, running Windows Update.
                                                </td>
                                            </tr>
                                            <tr>
                                                <td bgcolor="#D0D8E8" style="padding-left:5px;">Streaming Audio/Video
                                                </td>
                                                <td bgcolor="#E9EDF4" style="padding:0px 5px;">…viewing/listening online
                                                    streaming using YouTube, iTunes, Yahoo Music, Vimeo etc.
                                                </td>
                                            </tr>
                                            <tr>
                                                <td bgcolor="#D0D8E8" style="padding-left:5px;">Gaming</td>
                                                <td bgcolor="#E9EDF4" style="padding:0px 5px;">…playing online games or
                                                    gambling.
                                                </td>
                                            </tr>

                                        </table>


                                        </p>
                                    </div>


                                    <h3 class="panel-title">I'm a Volume user of Dreamlink. I want to know how much
                                        volume I've used and change the bandwidth as well. Should I contact you for the
                                        same?</h3>
                                    <div class="panel-content">
                                        <p style="padding:10px 0px 10px 20px;"> No, you do not need to contact us to
                                            check/update volume. This service is available on our website. When
                                            connected to the internet, visit our Online Services page at
                                            http://www.dreamlink.in and log-in with your
                                            username/password. Click on "View & Update Volume Bandwidth" and proceed
                                            further. </p>
                                    </div>


                                    <h3 class="panel-title">What other elements should be taken care of?</h3>
                                    <div class="panel-content">
                                        <p style="padding:10px 0px 10px 20px;"> The other elements such as Virus,
                                            Spyware and associated risk factors cause the unauthorized use of your
                                            connection, eat up your subscription as well as slow down the overall
                                            performance by running in the background.<br/><br/>

                                            To learn more about what these Malwares can do and to avoid them from
                                            getting into your computer system, please refer to the following online
                                            Support pages:<br/><br/>

                                            • Computer Viruses, Worms, Trojan Horses - avoid, minimize or recover
                                            http://www.Dreamlink.in<br/><br/>

                                            • Security risks and countermeasures associated with Internet connectivity
                                            http://www.Dreamlink.in


                                        </p>
                                    </div>

                                    <!--


                                      <h3 class="panel-title">How can I find out how many hours I have used the Internet?</h3>
                                      <div class="panel-content">
                                        <p style="padding:10px 0px 10px 20px;"> Please visit http://www.wlinktech.com.np, click on 'Customers' and the click on ‘Check Account Status’. You can view the log of your internet usage from ‘Check Internet Usage’.</p>
                                      </div>
                                      -->

                                    <!--  <h3 class="panel-title">What if I cross a time slot during one login session?</h3>
                                     <div class="panel-content">
                                       <p style="padding:10px 0px 10px 20px;"> After you have crossed your time slot, our server automatically disconnects you.
                               To renew your account, please call Account Section. </p>
                                     </div> -->


                                    <h3 class="panel-title">I want to change the username of my Internet Account
                                        subscribed some time back. Please explain the procedures for this service.</h3>
                                    <div class="panel-content">
                                        <p style="padding:10px 0px 10px 20px;"> For this, if not urgent, we suggest our
                                            users to change their usernames at the time of account renewal by
                                            subscribing under a new username. Once registered, Dreamlink charges Rs.1000
                                            for a username change in between or prior to the account expiry. </p>
                                    </div>

                                    <h3 class="panel-title">I want to change the password given to me during account
                                        subscription. How do I do that?</h3>
                                    <div class="panel-content">
                                        <p style="padding:10px 0px 10px 20px;"> To change your password, please connect
                                            first to Dreamlink using the current password. After you're connected, open
                                            your Internet Browser and log-on to our Homepage. After the page opens,
                                            click on "Customer " on the main menu and then on "Change Password". Now,
                                            follow the simple instructions given on the page to change your password
                                            online! </p>
                                    </div>


                                    <h3 class="panel-title">I could not connect through my account. I contacted
                                        Dreamlink Support and I was told that my account has expired. Why wasn't I
                                        informed prior to my account's expiration?</h3>
                                    <div class="panel-content">
                                        <p style="padding:10px 0px 10px 20px;"> We request our users to periodically
                                            check their "Account Status" and "Internet Usage Report" from our homepage
                                            to stay informed of their account status, validity or the remaining volume.
                                            Additionally, our server also send reminders of the same to your registered
                                            mobile numbers and Dreamlink email address prior to the expiry. We request
                                            users to check the mails sent from us and also update their mobile numbers
                                            in our database. </p>
                                    </div>



                                    <h3 class="panel-title">What is Dreamlink Fair Usage Policy?</h3>
                                    <div class="panel-content">
                                        <p style="padding:10px 0px 10px 10px;"> We have designed this Fair Usage Policy
                                            (FUP) effective from 1st April 2013, as part of our effort to provide the
                                            reliable and high-quality service to our customers. We intend to protect
                                            privacy and security of our customers and encourage responsible use of
                                            network resources. This Policy supplements (but does not supersede) our
                                            Terms & Conditions for Internet subscription and applies to all Internet
                                            customers with unlimited data plans.</p>
                                        <p style="padding:10px 0px 10px 10px;">
                                            <strong>Why Fair Usage Policy?</strong><br>

                                            We intend to provide the best possible Internet experience to all our
                                            customers. For this, we rely on customers being fair in the way they use the
                                            Internet. Generally, this is the case too. However, some small numbers of
                                            heavy or excessive users on unlimited data plans take up all the bandwidth,
                                            slowing down the network and connection that is shared for everyone
                                            else.<br/>

                                            This "Fair Usage Policy" is here to give us an option that if your usage is
                                            excessive over a period of time to the point it is impacting on other users
                                            experience, then we reserve the right to ask you to reduce your broadband
                                            usage.<br/>


                                            For example, if customer uses file-sharing software to upload and download
                                            large-sized a file, this activity uses a lot of network resources and
                                            affects the speed of network and Internet of other users. To ensure that
                                            majority of our customers do not suffer, we monitor the usage by looking at
                                            a number of factors including the amount of excessive usage over a period of
                                            time and the bandwidth used.</p>
                                        <p style="padding:10px 0px 10px 10px;">
                                            <strong>How will this policy affect me?</strong><br/>

                                            Under the policy, we normally define internal fair usage levels for
                                            unlimited data transfer plans. The usage levels set are very much adequate
                                            and as such, most customers will not be affected. Our system continuously
                                            monitors the data usage of each user periodically. <br/>

                                            If the system determines
                                            your usage to be excessive and unfair it will throttle your Internet speed
                                            until the subscriptions expires and send you the notification via E-mail or
                                            SMS. If not heeded to our requests, such accounts shall be disabled
                                            temporarily. If continued further, then, in addition to any remedy that it
                                            may have at law, we shall terminate contract to use the services and charge
                                            user any applicable cancellation fee and outstanding dues.
                                            <br/><br/>

                                            <strong>Note:</strong> Dreamlink reserves the right to change this FUP in
                                            part
                                            or in whole as
                                            and when required. The customer is solely responsible for any consequences
                                            that result due to the violation of this FUP. </p>
                                    </div>


                                </div>


    </div>


    <script src="js/modernizr.js"></script>

    <script>
        // Hiding the panel content. If JS is inactive, content will be displayed
        $('.panel-content').hide();

        // Preparing the DOM

        // -- Update the markup of accordion container
        $('.accordion').attr({
            role: 'tablist',
            multiselectable: 'true'
        });

        // -- Adding ID, aria-labelled-by, role and aria-labelledby attributes to panel content
        $('.panel-content').attr('id', function (IDcount) {
            return 'panel-' + IDcount;
        });
        $('.panel-content').attr('aria-labelledby', function (IDcount) {
            return 'control-panel-' + IDcount;
        });
        $('.panel-content').attr('aria-hidden', 'true');
        // ---- Only for accordion, add role tabpanel
        $('.accordion .panel-content').attr('role', 'tabpanel');

        // -- Wrapping panel title content with a <a href="">
        $('.panel-title').each(function (i) {

            // ---- Need to identify the target, easy it's the immediate brother
            $target = $(this).next('.panel-content')[0].id;

            // ---- Creating the link with aria and link it to the panel content
            $link = $('<a>', {
                'href': '#' + $target,
                'aria-expanded': 'false',
                'aria-controls': $target,
                'id': 'control-' + $target
            });

            // ---- Output the link
            $(this).wrapInner($link);

        });

        // Optional : include an icon. Better in JS because without JS it have non-sense.
        $('.panel-title a').append('<span class="icon">+</span>');

        // Now we can play with it
        $('.panel-title a').click(function () {

            if ($(this).attr('aria-expanded') == 'false') { //If aria expanded is false then it's not opened and we want it opened !

                // -- Only for accordion effect (2 options) : comment or uncomment the one you want

                // ---- Option 1 : close only opened panel in the same accordion
                //      search through the current Accordion container for opened panel and close it, remove class and change aria expanded value
                $(this).parents('.accordion').find('[aria-expanded=true]').attr('aria-expanded', false).removeClass('active').parent().next('.panel-content').slideUp(200).attr('aria-hidden', 'true');

                // Option 2 : close all opened panels in all accordion container
                //$('.accordion .panel-title > a').attr('aria-expanded', false).removeClass('active').parent().next('.panel-content').slideUp(200);

                // Finally we open the panel, set class active for styling purpos on a and aria-expanded to "true"
                $(this).attr('aria-expanded', true).addClass('active').parent().next('.panel-content').slideDown(200).attr('aria-hidden', 'false');

            } else { // The current panel is opened and we want to close it

                $(this).attr('aria-expanded', false).removeClass('active').parent().next('.panel-content').slideUp(200).attr('aria-hidden', 'true');
            }
            // No Boing Boing
            return false;
        });
    </script>


    </td>
    </tr>
    </table>

    </td>
    <td width="32" align="left" valign="top">&nbsp;</td>
    <td width="500" align="left" valign="top">
        <table width="500" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td width="524" align="center"><img src="images/tech_support0.jpg"/></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <div class="box"><strong>Contact Customer Care</strong><br/>
                        Call: 0353 2501854/ 52<br/>
                        Email us: info@dreamlink.in <br/>
                        <br/>

                    </div>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <!--<tr>
              <td><div class="box">
                <strong>For Accounting Issues</strong><br />
                E-mail: account@wlink.com.np
                <br />
                Tel. 5523050, Ext. 405 </div></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
            </tr>-->
        </table>
    </td>
    </tr>
    </table>
</div>

<br/> <br/>

<div class="footer">
    <center> ©  Copyright 2017 Dreamlink Technologies P. Ltd. All rights reserved.</center>


    <!--<div style="display:none;" class="nav_up" id="nav_up"></div>-->
    <div style="display:none;" class="nav_down" id="nav_down"></div>
    @section('footerscripts')
    <script src="js/jquery-1.3.2.js" type="text/javascript"></script>
    <script src="js/scroll-startstop.events.jquery.js" type="text/javascript"></script>
    <script>
        $(function () {
            var $elem = $('body');

            $('#nav_up').fadeIn('slow');
            $('#nav_down').fadeIn('slow');

            $(window).bind('scrollstart', function () {
                $('#nav_up,#nav_down').stop().animate({'opacity': '0.2'});
            });
            $(window).bind('scrollstop', function () {
                $('#nav_up,#nav_down').stop().animate({'opacity': '1'});
            });

            $('#nav_down').click(
                function (e) {
                    $('html, body').animate({scrollTop: $elem.height()}, 800);
                }
            );
            $('#nav_up').click(
                function (e) {
                    $('html, body').animate({scrollTop: '0px'}, 800);
                }
            );
        });
    </script>

@stop
@endsection