@section('title', 'Contact Us')
@extends('structure')
@section('content')
    <style>
        .top-gap{
            margin-top: 2%;

        }
        .texts {
            margin-bottom: 3%;

        }
        .heading-primary {
            margin: 1% 1% 1% 1%;
            color: #00B58B;
            font-weight: 600;
        }
        .no-dec {
            list-style-type: none;
            text-decoration: none;
        }
    </style>





    <div class="container top-gap" style=" background-color: #ffffff;">
        <div class="row texts">
            <div class="col-sm">
                <h1 class="heading-primary"><span style="font-weight: 600">Terms & Condition</span> Us</h1>
                <p>
<ul>



                    <li> Installation, Activation & Package Charges are to be paid in advance.  </li>
                    <li>  Installation & Activation Charges are non refundable.  </li>
                    <li>  LAN Card cost is not included in installation amount.  </li>
                    <li>  Service Taxes will be charged extra GST @ 18%.  </li>
                    <li>  All Package are valid for 30 days.  </li>

                    <li>  Customers are requested to renew to their packages within expiry or almost within 23.00
                        hours after expiry  </li>
                    <li>  Customers can renew the Unlimited Packages any time within expiry date,   </li>


                    <li>   Customers can avail upto mentioned bandwidth for respective packages.  </li>

                    <li>      The above packages are valid for selected area's of Siliguri circle only.  </li>

                </ul>
                </p>

                <h1 class="heading-primary"><span style="font-weight: 600">Refund Policy</span></h1>
                <ul>
                    <li>
                All sales of Recharge are final and there will be no refund or exchange permitted.
                Please be advised that You are responsible for the Recharge for and all charges that result
                from those purchases. Dreamlink Technologies
                Pvt. Ltd. is not responsible for any purchase of Recharge for an incorrect account number
                    </li>
                    <li>
                        However, in a case where a transaction has been completed by you on the Site, and money
                        has been charged to your card or bank account but a Recharge has not delivered within 24
                        hours of your completion of the transaction then you may inform us by sending us an email
                        on info@dreamlink.in. In such a scenario you will be entitled to a full refund. We request
                        you to include in the email the following details - the mobile number, user name, full name,
                        Recharge value, Transaction date and Order Number Dreamlink Technologies
                        Pvt. Ltd.. shall
                        investigate the incident and if it is found that money was indeed charged to your card or bank account
                        without delivery of the Recharge
                        then you will be refunded the money within 7 working days from the date of the receipt of your email.
                    </li>
                </ul>
            </div>

        </div>
    </div>

@endsection