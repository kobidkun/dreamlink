@section('title', 'FTTH ( Fibert o the home )  Connection problems troubleshoot')
@extends('structure')
@section('content')
    <style type="text/css">
        .article {
            text-align: justify;
            border-top: 1px solid #e0e0e0;
            padding-top: 25px;
            padding: 2px 5px;
            margin-top: 2%;

        }

        .article h3 {
            color: #084d93;
            font-size: 1em;
            margin-left: 2px;
            padding-top: 15px;
            padding-left: 20px;
        }

        .article ul {
            font-size: 16px;
            margin: 0% 0% 1% 4%;
            text-align: justify;
        }

        .article ul li {
            margin-top: 2px;
        }

        .article img {
            border: 1px solid #0956A4;
            margin: 2% 0;
        }

        .article a:link {
            color: #0956a4 !important;
        }

        .internal_link li > a:link {
            color: #2C50A4 !important;

        }

        .sub_list_solution {
            padding-left: 5%;
        }

        .footnote {
            border-top: 1px solid #e0e0e0;
            margin-left: 24px;
            margin-top: 50px;

        }

        .footnote li {
            font-size: 14px;
            margin-left: 25px;
            font-weight: bold;
        }

    </style>


    <style id="style-1-cropbar-clipper">/* Copyright 2014 Evernote Corporation. All rights reserved. */
        .en-markup-crop-options {
            top: 18px !important;
            left: 50% !important;
            margin-left: -100px !important;
            width: 200px !important;
            border: 2px rgba(255, 255, 255, .38) solid !important;
            border-radius: 4px !important;
        }

        .en-markup-crop-options div div:first-of-type {
            margin-left: 0px !important;
        }
    </style>

    <div id="container">

        <div id="container-inside">
            <h1 class="title" style="
    margin:0px;
    font-weight: bolder;
    color: #0956A4;
    padding-top:5px;
    padding-left: 20px;
    font-size: 3em;
    font-weight: bold;
    letter-spacing: 1px;
    ">FTTH FAQs</h1>
            <h4 style="
	margin-left: 4px;
	margin-top:25px;
	color: #084d93;
	padding-top:15px;
	padding-left: 20px;
	font-size: 1em;
	">
                Please, read the following FAQs to troubleshoot FTTH Connection problems yourself.
            </h4>
            <ul style="margin-left: 38px; margin-top: 20px; font-size:14px">
                <strong class="internal_link">
                    <li type="1">
                        <a href="#internet_not_working" style="color: #00B58B">Why is my internet not working?</a>
                    </li>
                    <li type="1">
                        <a href="#internet_slow" style="color: #00B58B">Why is my internet connection so slow?</a>
                    </li>
                    <li type="1">
                        <a href="#change_password" style="color: #00B58B !important;">How do I change my WiFi Password?</a>
                    </li>
                </strong>

            </ul>

            <a name="internet_not_working"></a>
            <div class="article">


                <h3>1. Why is my internet not working ?</h3>

                <ul>
                    <li>Check whether the Fiber router is switched on. If you are using a secondary router for Wi-Fi,
                        check
                        to see if it is powered on.
                    </li>
                    <div></div>
                    <li> Check whether <strong>“LOS indicator”</strong> on the Fiber Router is glowing <span
                                style="color: red"><strong>red</strong></span>. The RED LOS
                        light
                        indicates some ongoing network problem.
                    </li>

                    <ul class="sub_list_solution">
                        <li>Try restarting the Fiber router.</li>
                        <li>Fiber cable around your premises needs to be well managed and should not be bent, twisted or
                            pressed.
                        </li>
                        Please contact Dreamlink Support [0353-3812000], if the LOS indicator is still glowing red. <br>
                        <img src="images/router.jpeg" width="350px">
                    </ul>
                    <li>
                        Check whether <strong>“PON light”</strong> is blinking. It needs to continuously glow green for
                        Internet to work.
                        Try restarting the Fiber router.
                    </li>

                </ul>
                <p style="margin: 10px 41px; font-size: 16px; color:#084d93; font-weight:bold; ">Note: Please contact
                    Dreamlink Support [0353 2501854 / 52], if the PON light is still blinking continuously.</p>


            </div>
            <a name="internet_slow"></a>


            <div class="article" style=" margin-top: 25px;">
                <h3>2. Why is my internet connection so slow?</h3>
                <ul>
                    <li>
                        <strong>Internet Connection through Wi-Fi</strong> : There are many factors that affect the
                        internet connection speed through Wi-Fi.

                    </li>
                    <ul class="sub_list_solution">
                        <li>
                            Check whether your computer/smartphone connected to the router through Wi-Fi is not too far
                            and is <strong>inside the Wi-Fi coverage with good signal strength.</strong>
                        </li>
                        <li> Usually you experience a slow internet connection through Wi-Fi due to <strong>high latency
                                and
                                interference </strong> in the wireless network.
                        </li>
                        <span><strong><br>Recommendations</strong></span><br>
                        <li>
                            Adding a secondary router will amplify the Wi-Fi coverage/Signal strength inside your
                            premises and decrease the latency while connecting the internet through Wi-Fi. You will
                            experience faster and reliable connection after that.
                        </li>
                        <li>
                            For best internet connection speed, we recommend you to use a LAN/Ethernet cable to connect
                            your computer. Using ethernet cable is best recommended for gaming and live streaming.
                        </li>
                        <img src="images/laptop.jpeg">
                    </ul>
                </ul>

            </div>
            <a name="change_password"></a>
            <div class="article">

                <h3>3. How do I change my WiFi Password?</h3>
                <ul>
                    <li>Open your Internet Browser [Google Chrome, Firefox or Safari etc.]</li>
                    <li>Go to <a href="//dreamlink.com" target="_blank">www.dreamlink.com</a>.</li>
                    <li>After the page opens, click on “Customer Login” link at the top.</li>
                    <img src="img/faq/header.jpeg" width="100%">
                    <li>Enter “Username” and “Password” given to you by Dreamlink.</li>
                    <i>Note: Password is sent to you through SMS (from our number 5465) during service installation</i>
                    <img src="images/login.jpeg">
                    <li>
                        After logging-in, click on UPDATE SSID link at the left.

                    </li>
                    <i>Note : Wi-Fi SSID and password can only be changed if the User Status is Online. Please refer to the
                        picture below.</i><br>
                    <img src="images/status.jpeg">

                    <li>Enter new Wi-Fi name on Wi-Fi SSID field and new Wi-Fi password on Wi-Fi Key field.</li>
                    <img src="images/ssid.jpeg">

                    <li>Finally, enter the password you initially used to login to the Online Services and click on Save
                        Changes to save all the settings.
                    </li>
                    <img src="images/password.jpeg">

                </ul>
                <div class="footnote">
                    <h3> IMPORTANT POINTS </h3>
                    <ul>

                        <li>DOWNLOAD “<a href="https://play.google.com/store/apps/details?id=in.com.dreamlink.dreamlinkapp"
                                         target="_blank">MyDreamlink</a>” APP FROM PLAYSTORE ON ANDROID]<br>
                            <span class="note"></span></li>
                        <li>PLEASE DON’T RESET THE ROUTER</li>
                        <li>PLEASE DON’T MISHANDLE/BEND THE FIBER CABLE</li>
                        <li>SUPPORT CONTACT NO.: 0353 2501854 / 52</li>

                    </ul>
                </div>
            </div>

        </div>
        <br> <br>

@endsection