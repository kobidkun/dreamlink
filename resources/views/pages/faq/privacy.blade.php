@section('title', 'Contact Us')
@extends('structure')
@section('content')
    <style>
        .top-gap{
            margin-top: 2%;

        }
        .texts {
            margin-bottom: 3%;

        }
        .heading-primary {
            margin: 1% 1% 1% 1%;
            color: #00B58B;
            font-weight: 600;
        }
        .no-dec {
            list-style-type: none;
            text-decoration: none;
        }
    </style>





    <div class="container top-gap" style=" background-color: #ffffff;">
        <div class="row texts">
            <div class="col-sm">


                <h1 class="heading-primary"><span style="font-weight: 600">Privacy Policy</span></h1>

                    <div class="well">
                        <ul>
                            <li>	This Policy governs and covers Dreamlink Technologies Pvt. Ltd Ltd hereinafter, (The Company) is dedicated to protect  our Users personal information that the Company collects and receives from time to time, including information related to  previous usage, (if any) of the Company's products and services. </li>
                            <li>	Personal information refers to all such information about the Users that are personally identifiable like Name, Contact Address, E-mail ID, Phone number etc. and/or any such information deemed relevant within the ground and scope of this Privacy Policy.  </li>
                            <li>	The construed personal information, with relevant admissible proofs must sustain and support such claim. Therefore it may be stated, that the Company only peruse to collect the personal information for a variety of regulatory and business purposes and /or for specific, general and statutory purposes only. .</li>
                            <li>	These at present include, but does not limit to,fulfilling these requests for Products and Services, upgrade our services, interact with the Users, conduct various research and development, conduct statutory, legal, regulatory, fraud/crime prevention and general audit requirements.</li>
                        </ul>

                        <h3>Purpose of Sharing and Disclosure</h3>
                        <p>Dreamlink Technologies Pvt. Ltd Ltd, Privacy Policy is designed and developed to address the privacy and security of the personal information provided to us. Please be informed thatThe Company, may share the Users' personal information with Third Party, under the following circumstances:</p>
                        <ul>
                            <li>To provide the information to our trusted business associates/ partners who work on behalf of or with the Company, however bound by the scope and tenure of confidentiality agreements signed with The Company. Therefore, these companies may use such personal information to assist the Company, communicate with the organizations, and apprise Users of offers from the Company and our marketing/distribution partners, as the case may be.</li>
                            <li>The Company believes it is relevant to share information with State/Central investigative agencies pursuing investigation to prevent/take action regarding illegal activities, suspected fraud, situations involving potential threats to the physical/emotional safety of any person(s) or groups,acts of illegal trafficking, terrorism and any other acts of violence or violations of the Company's Terms and Conditions as underlined in the Customer's Application Form (CAF), or as otherwise required by such investigative agencies enforcing the Law of the Land.</li>
                            <li>The Company reserves the right to convey to the Userscertain communications relating to the Company's administrative messages, service related announcements and other information that are exclusively considered to be mandatory and intrinsic in nature, for functioning.</li>

                        </ul>
                        <h3>Modification/Alteration to Privacy Policy</h3>
                        <p>The Company may update this Privacy Policy from
                            time to time and may display the same in their website without notifying any
                            one personally. Dreamlink Technologies Pvt. Ltd Ltdreserves the right to
                            amend/alter/update/ modify this Privacy Policy at any time, as and when
                            necessitated. We request our Users to visit our website
                            <u>http://www.dreamlink.in</u> periodically
                            for contemporary information and updates. However, it may be noted that, posting of such information in the above website would be deemed to have been reported, read and understood by the Users.</p>

                    </div>        </div>

            </div>

        </div>


@endsection