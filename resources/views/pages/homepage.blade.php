
@section('title', 'Largest ISP of Siliguri , Internet Service Provider in Siliguri, Siliguri ISP, High speed
    internet,
    Internet Services in Siliguri, Wireless Internet Service')

@extends('structure')
@section('content')

    <div class="container-fluid " style="padding-top: 30px; width: 104%; margin-left: -2%">
        <div id="carouselExampleIndicators" class="carousel slide" data-interval="15000" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img class="d-block w-100" src="banner/4.jpg" alt="First slide">
                </div>

                <div class="carousel-item">
                    <a href="/dreamlink-rf">
                        <img class="d-block w-100"  src="banner/6.jpg" alt="Second slide">
                    </a>
                </div>

                <div class="carousel-item ">
                    <img class="d-block w-100" src="banner/1.jpg" alt="First slide">
                </div>

                <div class="carousel-item">
                    <a href="https://play.google.com/store/apps/details?id=in.com.dreamlink.dreamlinkapp"> <img
                                class="d-block w-100" src="banner/3.jpg" alt="Third slide"> </a>
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
    <div id="container">


        <center>
            <table width="950" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td><a href="/dreamlink-fiber"><img src="images/icons/internet.png" width="200" height="140"
                                                     style="margin-left: 15px;"></a></td>

                    <td><a href="#"><img src="images/icons/myworldlinkapp.png" width="200"
                                                           height="140"></a></td>
                    <td><a href="#"><img src="images/icons/refer.png" width="200" height="140"></a></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </center>


        <br>
        <div class="tab-header" style="background-color: inherit;">
            <span class="subtitle" style="padding-top: 15px;">Fiber Unlimited (100 Mbps )</span></div>
        <table width="1000" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
                <td width="1200">
                    <div>
                        <p>
                        <div class="tab-subheader">
                            <div class="tab-title-col" style="width: 16%;">
                                <ul class="white">
                                    <li class="bold" style="padding: 17px;"></li>
                                    <li class="transparent">Price/Renewal</li>
                                    <li class="transparent2">Installation Charge</li>
                                    <li class="transparent2">Turbo Speed (GB)</li>
                                    <li class="transparent">Unlimited Speed
                                    </li>
                                </ul>
                            </div>
                            <div class="tab-pricing-col" style="width: 27%;">
                                <ul>
                                    <li class="bold">1 Month</li>
                                    <li class="transparent"><strong>
                                            <i class="fa fa-inr" aria-hidden="true"></i> 3500&nbsp;</strong></li>
                                    <li class="transparent2"><strong><strike><i class="fa fa-inr"
                                                                                aria-hidden="true"></i>
                                                2500</strike>&nbsp;FREE&nbsp;</strong></li>
                                    <li class="transparent"><strong> Unlimited&nbsp;</strong></li>

                                    <li><a href="#" target="_blank">
                                            <div
                                                    class="order-button" data-id="0">Contact Us
                                            </div>
                                        </a></li>
                                </ul>
                            </div>
                            <div class="tab-pricing-col" style="width: 27%;">
                                <ul>
                                    <li class="bold">6 + 1 Months</li>
                                    <li class="transparent"><strong><i class="fa fa-inr" aria-hidden="true"></i> 21000&nbsp;</strong>
                                    </li>
                                    <li class="transparent2"><strong><strike><i class="fa fa-inr"
                                                                                aria-hidden="true"></i>
                                                2500</strike>&nbsp;FREE&nbsp;</strong></li>
                                    <li class="transparent"><strong> Unlimited&nbsp;</strong></li>

                                    <li><a href="#" target="_blank">
                                            <div
                                                    class="order-button" data-id="0">Contact Us
                                            </div>
                                        </a></li>
                                </ul>
                            </div>
                            <div class="tab-pricing-col" style="width: 27%;">
                                <ul>
                                    <li class="bold">12 + 2 Months</li>
                                    <li class="transparent"><strong><i class="fa fa-inr" aria-hidden="true"></i> 42000&nbsp;</strong>
                                    </li>
                                    <li class="transparent2"><strong><strike><i class="fa fa-inr"
                                                                                aria-hidden="true"></i>
                                                2500</strike>&nbsp;FREE&nbsp;</strong></li>
                                    <li class="transparent"><strong>Unlimited&nbsp;</strong></li>

                                    <li><a href="#" target="_blank">
                                            <div
                                                    class="order-button" data-id="0">Contact Us
                                            </div>
                                        </a></li>
                                </ul>
                            </div>
                            <div class="clear">
                            </div>
                        </div>
                        </p>
                    </div>
                </td>
            </tr>
        </table>
        <br>
        <div class="tab-header" style="background-color: inherit;">
            <span class="subtitle" style="padding-top: 15px;">Fiber - Unlimited ( 8 Mbps )</span></div>
        <table width="1000" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
                <td width="1200">
                    <div>
                        <p>
                        <div class="tab-subheader">
                            <div class="tab-title-col" style="width: 16%;">
                                <ul class="white">
                                    <li class="bold" style="padding: 17px;"></li>
                                    <li class="transparent">Price/Renewal</li>
                                    <li class="transparent2">Installation Charge</li>
                                </ul>
                                <span style="font-size: smaller; font-weight: normal;color: white;
                                padding: 5px 10px; margin-top: 10px; margin-left: 5px;display: block;">
                    Fair Usage Policy is applicable</span>
                            </div>
                            <div class="tab-pricing-col" style="width: 27%;">
                                <ul>
                                    <li class="bold">1 Month</li>
                                    <li class="transparent"><strong><i class="fa fa-inr" aria-hidden="true"></i> 800&nbsp;</strong>
                                    </li>
                                    <li class="transparent2"><strong><strike><i class="fa fa-inr"
                                                                                aria-hidden="true"></i> 2500</strike>&nbsp;FREE&nbsp;</strong>
                                    </li>
                                    <li><a href="#" target="_blank">
                                            <div
                                                    class="order-button" data-id="1">Order Now »
                                            </div>
                                        </a></li>
                                </ul>
                            </div>
                            <div class="tab-pricing-col" style="width: 27%;">
                                <ul>
                                    <li class="bold">6 + 1 Months</li>
                                    <li class="transparent"><strong><i class="fa fa-inr" aria-hidden="true"></i> 4800&nbsp;</strong>
                                    </li>
                                    <li class="transparent2"><strong><strike><i class="fa fa-inr"
                                                                                aria-hidden="true"></i> 2500</strike>&nbsp;FREE&nbsp;</strong>
                                    </li>
                                    <li><a href="#" target="_blank">
                                            <div
                                                    class="order-button" data-id="1">Order Now »
                                            </div>
                                        </a></li>
                                </ul>
                            </div>
                            <div class="tab-pricing-col" style="width: 27%;">
                                <ul>
                                    <li class="bold">12 + 2 Months</li>
                                    <li class="transparent"><strong><i class="fa fa-inr" aria-hidden="true"></i> 9600</strong>
                                    </li>
                                    <li class="transparent2"><strong><strike><i class="fa fa-inr"
                                                                                aria-hidden="true"></i> 2500</strike>&nbsp;FREE&nbsp;</strong>
                                    </li>
                                    <li><a href="#" target="_blank">
                                            <div
                                                    class="order-button" data-id="1">Order Now »
                                            </div>
                                        </a></li>
                                </ul>
                            </div>
                            <div class="clear">
                            </div>
                        </div>
                        </p>
                    </div>
                </td>
            </tr>
        </table>
        <br>

        @include('components.setup')
        </center>
        <br>
        <br>
        <br>
        <!--<center>  <a href="rates.php" class="box-link">  Other Rates  </a> </center><br> <br>-->


    </div>





@endsection