@section('title', 'Internet service Plans Tariff and pricing for Business')
@extends('structure')
@section('content')

@section('header-css')
    <link rel="stylesheet" type="text/css"
          href="css/easy-responsive-tabs.css "/>

@stop

<div id="wholeBody">
    <div id="full-width"
         style="background-image: url(images/fibermain.jpg); background-position: center; background-size: cover; background-repeat: no-repeat; height: 400px;">

        <div id="container">
            <div style="padding-top: 50px;">
                <div style="width: 500px; color: white; float: left;background-color:rgba(0, 0, 0, 0.2);">
                    <div class="subtitle_banner"
                         style="font-weight: bold; font-size: 42px;padding:12px 12px 0px 12px;text-align: center;">Fiber
                        Optic Internet
                    </div>
                    <p style="padding:0px 12px 12px 12px;text-align: center;">
                        Fiber Optic Internet is a high speed FTTH based internet
                        service that relies on world&rsquo;s best GPON technology. It has
                        very low latency which makes the internet connection highly
                        stable. We ensure consistently fast speeds to provide you with
                        high download & upload speed with minimal buffering.
                    </p>
                </div>
                <div class="clear"></div>
            </div>
        </div>

    </div>

    <div id="container">
        <table width="1200" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td width="1200">&nbsp;</td>
            </tr>
            <tr>
                <td align="center">
                    <table width="800" border="0" align="center"
                           cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="1200">&nbsp;</td>
                        </tr>
                        <tr>
                            <td align="center" valign="top"><img
                                        src="images/wlink_fiber_iptv.png" width="537" height="115"><br> <br>
                                <p> Dreamlink Fiber broadbabd service is available in Siliguri now</p> <br>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
        @include('components.home-pricing')
        @include('components.setup')


    </div>
</div>

</td>
</tr>
</table></td>
</tr>


</table></td>
</tr>
</table></td>
</tr>
<tr>
    <td>&nbsp;</td>
</tr>
</table>
</div>
<style>
    .error {
        color: red;
    }
</style>

@section('footerscripts')

    <script src="{{asset('js/easyResponsiveTabs.js')}}"></script>


    <!--Plug-in Initialisation-->
    <script type="text/javascript">
        $(document).ready(function () {
            initTabsPlugin();
        });

        function initTabsPlugin() {
            //Horizontal Tab
            /*         $('#parentHorizontalTab').easyResponsiveTabs({
             type: 'default', //Types: default, vertical, accordion
             width: 'auto', //auto or any width like 600px
             fit: true, // 100% fit in a container
             tabidentify: 'hor_1', // The tab groups identifier
             activate: function(event) { // Callback function if tab is switched
             var $tab = $(this);
             var $info = $('#nested-tabInfo');
             var $name = $('span', $info);
             $name.text($tab.text());
             $info.show();
             }
             }); */

            // Child Tab
            $('#ChildVerticalTab_1').easyResponsiveTabs({
                type: 'vertical',
                width: 'auto',
                fit: true,
                tabidentify: 'ver_1', // The tab groups identifier
                activetab_bg: '#fff', // background color for active tabs in this group
                inactive_bg: '#F5F5F5', // background color for inactive tabs in this group
                active_border_color: '#c1c1c1', // border color for active tabs heads in this group
                active_content_border_color: '#58CAAD' // border color for active tabs contect in this group so that it matches the tab head border
            });

            $('#ChildVerticalTab_2').easyResponsiveTabs({
                type: 'vertical',
                width: 'auto',
                fit: true,
                tabidentify: 'ver_2', // The tab groups identifier
                activetab_bg: '#fff', // background color for active tabs in this group
                inactive_bg: '#F5F5F5', // background color for inactive tabs in this group
                active_border_color: '#c1c1c1', // border color for active tabs heads in this group
                active_content_border_color: '#58CAAD' // border color for active tabs contect in this group so that it matches the tab head border
            });

            //Vertical Tab
            $('#parentVerticalTab').easyResponsiveTabs({
                type: 'vertical', //Types: default, vertical, accordion
                width: 'auto', //auto or any width like 600px
                fit: true, // 100% fit in a container
                closed: 'accordion', // Start closed if in accordion view
                tabidentify: 'hor_1', // The tab groups identifier
                activate: function (event) { // Callback function if tab is switched
                    var $tab = $(this);
                    var $info = $('#nested-tabInfo2');
                    var $name = $('span', $info);
                    $name.text($tab.text());
                    $info.show();
                }
            });
        }
    </script>


    <script type="text/javascript">
        $(document).on("click", ".order-button", function (e) {
            e.preventDefault();
            var th = $(this);
            var pack_name = th.closest(".resp-tab-content").find(".subtitle").html();
            var package_date = th.closest(".tab-pricing-col").find("ul .bold").html();
            var token_order = "as59daa5907814f";
            var fromPage = "fiber.php";
            var encodedData = [{
                pack_name: pack_name,
                package_date: package_date,
                type: "fiber",
                token_order: token_order,
                "to_page": fromPage
            }];

            window.open(
                'order.php?orderinfo=' + btoa(JSON.stringify(encodedData)),
                '_blank' // <- This is what makes it open in a new window.
            );
            //window.location.href='order.php?orderinfo='+btoa(JSON.stringify(encodedData));
        });

        function fn60sec() {
            window.location.reload();
        }

        setInterval(function () {
            fn60sec();
        }, 60 * 5 * 1000);
    </script>

@stop

@endsection