@section('title', 'Internet service Plans Tariff and pricing for Home Users')
@extends('structure')
@section('content')

@section('header-css')
    <link rel="stylesheet" type="text/css"
          href="css/easy-responsive-tabs.css "/>

@stop

<div  style="width: 100%; padding: 2% 7% 7% 7%; align-content: center; align-items: center">


   <div class="center-align center">
       @include('components.home-pricing')
   </div>


    @include('components.setup')
    </div>
    </p>
</div>

</div>




<br>
<br>




@section('footerscripts')

    <script src="{{asset('js/easyResponsiveTabs.js')}}"></script>




    <!--Plug-in Initialisation-->
    <script type="text/javascript">
        $(document).ready(function () {
            //Horizontal Tab
            $('#parentHorizontalTab').easyResponsiveTabs({
                type: 'default', //Types: default, vertical, accordion
                width: 'auto', //auto or any width like 600px
                fit: true, // 100% fit in a container
                tabidentify: 'hor_1', // The tab groups identifier
                activate: function (event) { // Callback function if tab is switched
                    var $tab = $(this);
                    var $info = $('#nested-tabInfo');
                    var $name = $('span', $info);
                    $name.text($tab.text());
                    $info.show();
                }
            });

            // Child Tab
            $('#ChildVerticalTab_1').easyResponsiveTabs({
                type: 'vertical',
                width: 'auto',
                fit: true,
                tabidentify: 'ver_1', // The tab groups identifier
                activetab_bg: '#fff', // background color for active tabs in this group
                inactive_bg: '#F5F5F5', // background color for inactive tabs in this group
                active_border_color: '#c1c1c1', // border color for active tabs heads in this group
                active_content_border_color: '#5AB1D0' // border color for active tabs contect in this group so that it matches the tab head border
            });

            $('#ChildVerticalTab_2').easyResponsiveTabs({
                type: 'vertical',
                width: 'auto',
                fit: true,
                tabidentify: 'ver_2', // The tab groups identifier
                activetab_bg: '#fff', // background color for active tabs in this group
                inactive_bg: '#F5F5F5', // background color for inactive tabs in this group
                active_border_color: '#c1c1c1', // border color for active tabs heads in this group
                active_content_border_color: '#5AB1D0' // border color for active tabs contect in this group so that it matches the tab head border
            });

            $('#ChildVerticalTab_3').easyResponsiveTabs({
                type: 'vertical',
                width: 'auto',
                fit: true,
                tabidentify: 'ver_3', // The tab groups identifier
                activetab_bg: '#fff', // background color for active tabs in this group
                inactive_bg: '#F5F5F5', // background color for inactive tabs in this group
                active_border_color: '#c1c1c1', // border color for active tabs heads in this group
                active_content_border_color: '#5AB1D0' // border color for active tabs contect in this group so that it matches the tab head border
            });

            //Vertical Tab
            $('#parentVerticalTab').easyResponsiveTabs({
                type: 'vertical', //Types: default, vertical, accordion
                width: 'auto', //auto or any width like 600px
                fit: true, // 100% fit in a container
                closed: 'accordion', // Start closed if in accordion view
                tabidentify: 'hor_1', // The tab groups identifier
                activate: function (event) { // Callback function if tab is switched
                    var $tab = $(this);
                    var $info = $('#nested-tabInfo2');
                    var $name = $('span', $info);
                    $name.text($tab.text());
                    $info.show();
                }
            });
        });
    </script>



    <script>
        new WOW().init();
    </script>


    <script>
        function stripQueryStringAndHashFromPath(url) {
            return url.split("?")[0].split("#")[0];
        }

        function gifRestart() {

            var iframeImages = $('#iframe_a').contents().find('img');

            for (i = 0; i < iframeImages.length; i++) {
                var src = $(iframeImages[i]).attr('src');
                src = stripQueryStringAndHashFromPath(src);

                var new_src = src + '?' + new Date().getTime();

                $(iframeImages[i]).attr('src', new_src);
            }
        }

        $("._tabs a").on('click', function () {
            gifRestart();
        })
    </script>


    <script type="text/javascript">
        $(document).on("click", ".order-button", function (e) {
            e.preventDefault();
            var th = $(this);
            var pack_name = th.closest(".resp-tab-content").find(".subtitle").html();
            var package_date = th.closest(".tab-pricing-col").find("ul .bold").html();
            var token_order = "as59daa97ac7f09";
            var fromPage = "ratesnew.php";
            var type1 = $("#parentHorizontalTab").find(".resp-tab-active").html();
            if (type1.toLowerCase().indexOf("cable") >= 0) {
                var type = "cable";
            } else if (type1.toLowerCase().indexOf("fiber") >= 0) {
                var type = "fiber";
            }
            var encodedData = [{
                pack_name: pack_name,
                package_date: package_date,
                type: type,
                token_order: token_order,
                "to_page": fromPage
            }];

            window.open(
                'order.php?orderinfo=' + btoa(JSON.stringify(encodedData)),
                '_blank' // <- This is what makes it open in a new window.
            );
            //window.location.href='order.php?orderinfo='+btoa(JSON.stringify(encodedData));
        });

        function fn60sec() {
            window.location.reload();
        }

        setInterval(function () {
            fn60sec();
        }, 60 * 5 * 1000);
    </script>



@stop

@endsection