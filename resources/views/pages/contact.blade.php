@section('title', 'Contact Us')
@extends('structure')
@section('content')
    <style>
        .top-gap{
            margin-top: 2%;
            margin-left:-2%;
            padding-bottom: 3%;

        }
        .texts {
            margin-bottom: 3%;

        }
        .heading-primary {
            margin: 1% 1% 1% 1%;
            color: #00B58B;
            font-weight: 600;
        }
        .no-dec {
            list-style-type: none;
            text-decoration: none;
        }
    </style>
    <div class="container-fluid">
        <div class="col-md-12 top-gap ">
            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14256.42842247398!2d88.4170994!3d26.7090251!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xf1d1c6b06ea2e017!2sDreamlink+Technologies+Pvt+Ltd!5e0!3m2!1sen!2sin!4v1511494557646"
                    width="104%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>


    </div>

    <div class="container" style=" background-color: #ffffff;">
        <div class="row texts">
            <div class="col-sm">
                <h1 class="heading-primary"><span style="font-weight: 600">Contact</span> Us</h1>
                <form role="form" method="POST" action="{{ route('landingpage.saveform') }}" >
                    <div class="form-group">
                        <label for="exampleInputEmail1">Name</label>
                        {{ csrf_field() }}
                        <input type="text" class="form-control" name="name"
                               aria-describedby="emailHelp" placeholder="Enter Name">
                        <small id="emailHelp" class="form-text text-muted">We'll never share your Details with anyone else.</small>
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Email</label>
                        <input type="email" class="form-control" name="email" aria-describedby="emailHelp" placeholder="Enter email">
                     </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Phone number</label>
                        <input type="text" class="form-control"
                               aria-describedby="emailHelp" name="mobile" value="+91 " placeholder="Enter Phone Number">
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Details</label>
                        <textarea class="form-control"  rows="3"></textarea>
                        </textarea>
                    </div>

                    <button style="background-color: #00B58B" type="submit" class="btn btn-primary btn-block">Submit</button>
                </form>
            </div>
            <div class="col-sm">
                <h1 class="heading-primary">The <strong>Office Address</strong></h1>
                <br>

                <ul class="list list-icons list-icons-style-3 mt-xlg">
                    <li class="no-dec"><i class="fa fa-map-marker"></i> <strong>Address:</strong>
                        Spencer Plaza, 2nd Floor, Burdwan Road, Siliguri 734005 India</li>
                    <li class="no-dec"><i class="fa fa-phone"></i> <strong>Phone:</strong> +91 353 2501854/ 52</li>
                    <li class="no-dec"><i class="fa fa-envelope"></i> <strong>Email:</strong>
                        info@dreamlink.in</li>
                </ul>
            </div>
        </div>
    </div>

@endsection