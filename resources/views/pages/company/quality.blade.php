@section('title', 'Quality Policy')
@extends('structure')
@section('content')

    <div id="container">

    </div>
    




    <div id="container-inside">
        <span class="title">Our Quality Policy</span><br/>
        <br/>
        <p><strong><img src="images/policy.png" align="right"/></strong>Dreamlink is focused on our customers' ultimate
            success and is committed to having the highest level of quality in the industry. It is our guiding principle
            to provide our external and internal customers with a level of quality and service that consistently meets
            or exceeds expectations through the following philosophies: </p> <br/> <br/>
        <ul style="padding-left:20px;">
            <li>Meeting or exceeding customer and organizational requirements - Constant striving for quality services
                that meet or exceed the customer&rsquo;s and/or organizational requirements.<br/>
                <br/>
            </li>
            <li> Effectively communicating up and down the &quot;Supply Chain&quot; - Effective communication of quality
                policy and objectives to customers, suppliers and our employees.<br/>
                <br/>
            </li>
            <li>Hiring the best people in the industry - Training those people on our system and focusing those people
                on executing our processes flawlessly.<br/>
                <br/>
            </li>

            <li>Aligning ourselves with the most competent base of suppliers available in the industry - The ability of
                our suppliers to provide us with quality goods and services is critical to our success. We will strive
                to achieve excellence in our supply chain with a common vision in the areas of quality, continuous
                improvement and excellence in customer service.
            </li>
        </ul>
    </div>

@endsection