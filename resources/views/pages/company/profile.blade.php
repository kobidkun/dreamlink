@section('title', 'Company Profile')
@extends('structure')
@section('content')
    <div id="container">

    </div>

    <div id="container-inside">
        <span class="title">Company Profile</span><br/>
        <strong><br/>
            ‘Success follows those who dare, especially those who dare with guts and sincerity’</strong><br/>
        <br/>

        <img src="images/roundimg.png" align="right"/>
        <p style="text-align: justify; margin-bottom: 15px;!important;">
            Dreamlink Technology Pvt Ltd was born with a vision to provide the best internet service for entertainment
            , education and business purposes in West Bengal
            On 2016.
            Based in Siliguri we offer broadband services on optic fiber and our Fiber-to-the-home (FTTH) Technology can carry huge amount of information at speed upto 1Gbps.
            We are the first FTTH(fiber-to-the-home) broadband service provider in this area.

        </p>


        <p style="text-align: justify; margin-bottom: 15px;!important;">
            Dreamlink operates state-of-the-art network built upon the network equipment manufacturer like Juniper , Cisco, Huawei for IP/MPLS Core and Aggregation, Extreme Network in Switching, and Huawei for the Access side.
        </p>


        <p style="text-align: justify; margin-bottom: 15px;!important;">
            We always believe in giving our customer the best value for their money and we are also looking forward to provide the best and fastest Internet Service to our valued customer
        </p>


    </div>
@endsection