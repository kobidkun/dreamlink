@section('title', 'Mission and vision')
@extends('structure')
@section('content')
    <div id="container">

    </div>

    <div id="container-inside">
        <span class="title">Vision & Mission</span><br/>
        <br/>
        <p><strong><img src="images/round_mission.png" align="right"/>Our Vision is </strong><br/>
            &ldquo;To connect everyone, anywhere, all the time&rdquo;</p>
        <p><strong><br/>
                Our Mission is </strong><br/>
            &ldquo;Enrich the lives of our customers through world-class service&quot;</p>
        <p><br/><br/><br/><br/><br/>
            <strong class="title"><a name="core" id="core">CORE VALUES</a></strong><br/><br/>
        </p>
        <p><strong>Customer-focus: </strong>We will go out of our way and walk that extra mile to WOW our customers </p>
        <p><br/>
            <strong>Honesty</strong>: We are honest and transparent in our dealings.</p>
        <p><br/>
            <strong>Efficiency</strong>: We strive to maximize efficiency in everything we do. We are frugal and seek
            value in our spending.</p>
        <p><br/>
            <strong>Trust and Respect</strong>: We maintain a healthy work environment based on mutual trust and
            respect, that builds strong teams and fosters long term relationships. </p>
        <p><br/>
            <strong>Innovation:</strong> We seek new and innovative approaches to solving our problems, even at the risk
            of failure.</p>
    </div>


@endsection