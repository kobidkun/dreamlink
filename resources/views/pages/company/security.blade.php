@section('title', 'Security Information')
@extends('structure')
@section('content')

    <div id="container">


        <div id="container-inside">
            <table width="1100" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="544" align="left" valign="top">
                        <div id="breadcrumbs" class="subtitle"> <a href="support.php" class="subtitle-link">Support </a>&gt; <a href="security.php" class="subtitle-link">Security Articles </a>&gt;  Computer Security</div>


                        <table width="556" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="556">
                                    <div class="accordion">
                                        <h3 class="panel-title">What is computer security?</h3>
                                        <div class="panel-content">
                                            <p style="padding:10px 0px 10px 20px;"> Computer security is the process of preventing and detecting unauthorized use of your computer. Prevention measures help you to stop unauthorized users (also known as "intruders") from accessing any part of your computer system. Detection helps you to determine whether or not someone attempted to break into your system, if they were successful, and what they may have done.</p>
                                        </div>
                                        <h3 class="panel-title">Why should you care about computer security?</h3>
                                        <div class="panel-content">
                                            <p style="padding:10px 0px 10px 20px;"> We use computers for everything from banking and investing to shopping and communicating with others through email or chat programs. Although you may not consider your communications "top secret," you probably do not want strangers reading your email, using your computer to attack other systems, sending forged email from your computer, or examining personal information stored on your computer (such as financial statements).</p>
                                        </div>


                                        <h3 class="panel-title">Who would want to break into your computer?</h3>
                                        <div class="panel-content">
                                            <p style="padding:10px 0px 10px 20px;"> Intruders (also referred to as hackers, attackers or crackers) may not care about your identity. Often they want to gain control of your computer so they can use it to launch attacks on other computer systems.<br /> <br />

                                                Having control of your computer gives them the ability to hide their true location as they launch attacks, often against high-profile computer systems such as government or financial systems. Even if you have a computer connected to the Internet only to play the latest games or to send email to friends and family, your computer may be a target.<br /> <br />

                                                Intruders may be able to watch all your actions on the computer, or cause damage to your computer by reformatting your hard drive or changing your data.</p>
                                        </div>


                                        <h3 class="panel-title">How easy is it to break into your computer?</h3>
                                        <div class="panel-content">
                                            <p style="padding:10px 0px 10px 20px;">   Unfortunately, intruders are always discovering new vulnerabilities (informally called "holes") to exploit in computer software. The complexity of software makes it increasingly difficult to thoroughly test the security of computer systems.<br /> <br />

                                                When holes are discovered, computer vendors will usually develop patches to address the problem(s). However, it is up to you, the user, to obtain and install the patches, or correctly configure the software to operate more securely. Most of the incident reports of computer break-ins received at WorldLink could have been prevented if system administrators and users kept their computers up-to-date with patches and security fixes.<br /> <br />

                                                Also, some software applications have default settings that allow other users to access your computer unless you change the settings to be more secure. Examples include chat programs that let outsiders execute commands on your computer or web browsers that could allow someone to place harmful programs on your computer that run when you click on them.  </p>
                                        </div>

                                        <h3 class="panel-title">Risk Factors</h3>
                                        <div class="panel-content">
                                            <p style="padding:10px 0px 10px 20px;">  i) Email Spoofing: Email “spoofing” is when an email message appears to have originated from one source when it actually was sent from another source. Email spoofing is often an attempt to trick the user into making a damaging statement or releasing sensitive information (such as passwords). <br /> <br />

                                                Spoofed email can range from harmless pranks to social engineering ploys. Examples of the latter include: <br /> <br />

                                            <ul>
                                                <li> email claiming to be from a system administrator requesting users to change their passwords to a specified string and threatening to suspend their account if they do not comply </li>
                                                <li> email claiming to be from a person in authority requesting users to send them a copy of a password file or other sensitive information </li>
                                            </ul>

                                            ii) Email-borne Viruses: Viruses and other types of malicious code are often spread as attachments to email messages. Before opening any attachments, be sure you know the source of the attachment. It is not enough that the mail originated from an address you recognize. The Melissa virus (see References) spread precisely because it originated from a familiar address. Also, malicious code might be distributed in amusing or enticing programs. <br /> <br />

                                            Never run a program unless you know it to be authored by a person or company that you trust. Also, don't send programs of unknown origin to your friends or coworkers simply because they are amusing -- they might contain a Trojan horse program. <br /> <br />

                                            iii) Hidden file Extensions: Windows operating systems contain an option to "Hide file extensions for known file types". The option is enabled by default, but a user may choose to disable this option in order to have file extensions displayed by Windows. Multiple email-borne viruses are known to exploit hidden file extensions. The first major attack that took advantage of a hidden file extension was the VBS/LoveLetter worm which contained an email attachment named "LOVE-LETTER-FOR-YOU.TXT.vbs". Other malicious programs have since incorporated similar naming schemes. Examples include:
                                            <ul>
                                                <li> Downloader (MySis.avi.exe or QuickFlick.mpg.exe) </li>
                                                <li>    VBS/Timofonica (TIMOFONICA.TXT.vbs) </li>
                                                <li>    VBS/CoolNote (COOL_NOTEPAD_DEMO.TXT.vbs) </li>
                                                <li>   VBS/OnTheFly (AnnaKournikova.jpg.vbs)</li>
                                            </ul>

                                            The files attached to the email messages sent by these viruses may appear to be harmless text (.txt), MPEG (.mpg), AVI (.avi) or other file types when in fact the file is a malicious script or executable (.vbs or .exe, for example).<br /> <br />

                                            iv) Unprotected Windows Shares: Unprotected Windows networking shares can be exploited by intruders in an automated way to place tools on large numbers of Windows-based computers attached to the Internet. Because site security on the Internet is interdependent, a compromised computer not only creates problems for the computer's owner, but it is also a threat to other sites on the Internet. The greater immediate risk to the Internet community is the potentially large number of computers attached to the Internet with unprotected Windows networking shares combined with distributed attack tools.<br /> <br />

                                            Another threat includes malicious and destructive code, such as viruses or worms, which leverage unprotected Windows networking shares to propagate. There is great potential for the emergence of other intruder tools that leverage unprotected Windows networking shares on a widespread basis.<br /> <br />

                                            v) Denial of Service: Another form of attack is called a denial-of-service (DoS) attack. This type of attack causes your computer to crash or to become so busy processing data that you are unable to use it. In most cases, the latest patches will prevent the attack. It is important to note that in addition to being the target of a DoS attack, it is possible for your computer to be used as a participant in a denial-of-service attack on another system.<br /> <br />

                                            vi) Chat Clients: Internet chat applications, such as instant messaging applications and Internet Relay Chat (IRC) networks, provide a mechanism for information to be transmitted bi-directionally between computers on the Internet. Chat clients provide groups of individuals with the means to exchange dialog, web URLs, and in many cases, files of any type.<br /> <br />

                                            Because many chat clients allow for the exchange of executable code, they present risks similar to those of email clients. As with email clients, care should be taken to limit the chat client’s ability to execute downloaded files. As always, you should be wary of exchanging files with unknown parties.
                                            </p>
                                        </div>



                                        <h3 class="panel-title">Actions users can take to protect their computer systems</h3>
                                        <div class="panel-content">
                                            <p style="padding:10px 0px 10px 20px;">    i) Use virus protection software: We recommend the use of anti-virus software on all Internet-connected computers. Be sure to keep your anti-virus software up-to-date. Many anti-virus packages support automatic updates of virus definitions. We recommend the use of these automatic updates when available. <br /> <br />


                                                ii) Use a firewall: We strongly recommend the use of some type of firewall product, such as a network appliance or a personal firewall software package. Intruders are constantly scanning home user systems for known vulnerabilities. Network firewalls (whether software or hardware-based) can provide some degree of protection against these attacks. However, no firewall can detect or stop all attacks, so it’s not sufficient to install a firewall and then ignore all other security measures.<br /> <br />

                                                iii) Don’t open unknown email attachments: Before opening any email attachments, be sure you know the source of the attachment. It is not enough that the mail originated from an address you recognize. The Melissa virus spread precisely because it originated from a familiar address. Malicious code might be distributed in amusing or enticing programs.
                                                If you must open an attachment before you can verify the source, we suggest the following procedure:<br /> <br />

                                            <ul>
                                                <li> make sure your virus definitions are up-to-date (see "Use virus protection software" above) </li>
                                                <li> save the file to your hard disk </li>
                                                <li> scan the file using your antivirus software </li>
                                                <li> open the file </li>
                                            </ul>

                                            For additional protection, you can disconnect your computer's network connection before opening the file. Following these steps will reduce, but not wholly eliminate, the chance that any malicious code contained in the attachment might spread from your computer to others. <br /> <br />

                                            iv) Don’t run programs of unknown origin: Never run a program unless you know it to be authored by a person or company that you trust. Also, don't send programs of unknown origin to your friends or coworkers simply because they are amusing -- they might contain a Trojan horse program.<br /> <br />

                                            v) Keep all applications (including your operating system) patched: Vendors will usually release patches for their software when a vulnerability has been discovered. Most product documentation offers a method to get updates and patches. You should be able to obtain updates from the vendor's web site. Read the manuals or browse the vendor's web site for more information.<br /> <br />

                                            Some applications will automatically check for available updates, and many vendors offer automatic notification of updates via a mailing list. Look on your vendor's web site for information about automatic notification. If no mailing list or other automated notification mechanism is offered you may need to check periodically for updates.<br /> <br />

                                            vi) Make regular backups of critical data: Keep a copy of important files on removable media such as ZIP disks or recordable CD-ROM disks (CD-R or CD-RW disks). Use software backup tools if available, and store the backup disks somewhere away from the computer.<br /> <br />

                                            vii) Disable scripting features in email programs: Because many email programs use the same code as web browsers to display HTML, vulnerabilities that affect ActiveX, Java, and JavaScript are often applicable to email as well as web pages. Therefore, in addition to disabling scripting features in web browsers (see "Disable Java, JavaScript, and ActiveX if possible", above), we recommend that users also disable these features in their email programs.<br /> <br />

                                            viii) Turn off your computer or disconnect from the network when not in use: Turn off your computer or disconnect its Ethernet interface when you are not using it. An intruder cannot attack your computer if it is powered off or otherwise completely disconnected from the network. </p>
                                        </div>

                                        <div class="accordion">
                                            <h3 class="panel-title">What is a Computer Virus?</h3>
                                            <div class="panel-content">
                                                <p style="padding:10px 0px 10px 20px;"> A computer virus is a self-multiplying computer program that spreads by inserting copies of itself into other executable code or documents through legitimate programs. A computer virus acts in a way parallel to a natural virus, which spreads by inserting itself into living cells. Broadening the similarity, the insertion of a virus into the program is known as an "infection", and the infected file, or executable code that is not part of a file, is called a "host". Viruses are one of the numerous types of malicious software or malware. In common jargon, the term virus often goes on to refer to ‘worms’, ‘trojan horses’ and other sorts of malware.
                                                </p>
                                            </div>
                                            <h3 class="panel-title"> What is a Trojan Horse program?</h3>
                                            <div class="panel-content">
                                                <p style="padding:10px 0px 10px 20px;"> A Trojan horse is a simple but malicious computer program that is camoflagued  as or snagged inside a authentic software. The term comes from the classical myth of the ‘Trojan Horse’. They may look useful or interesting (or at the very least harmless. For eg. It might claim to be a game) to an unsuspecting user, but are actually harmful when carried out, it might go as far as erasing your whole hard drive. Often the program is simply known as ‘trojan’.</p>
                                            </div>


                                            <h3 class="panel-title">What is a Computer Worm?</h3>
                                            <div class="panel-content">
                                                <p style="padding:10px 0px 10px 20px;"> A worm is a small piece of software that uses computer networks and security holes to duplicate itself. A copy of the worm searches within the network for another machine that has a particular security hole. It copies itself on to the new machine using the security hole, and then starts copying itself from there to other systems and it does so without any interference. In general, worms damage the network and consume bandwidth, while viruses infect or corrupt files on a targeted computer. Viruses generally do not affect network performance, as their activities are mostly confined within the target computer itself. <br /> <br />
                                            </div>


                                            <h3 class="panel-title">General tips on avoiding Virus Infections</h3>
                                            <div class="panel-content">
                                                <p style="padding:10px 0px 10px 20px;">  The following are some good ways of stopping your computer from getting a computer virus infection: <br /> <br />

                                                <ul>
                                                    <li>
                                                        Turn off the feature that allows automatic opening of email attachments, never open attachments from unidentified sources or attachments you are not expecting.
                                                    </li>
                                                    <li> Always scan diskettes, CD's and any other removable media before using them. </li>
                                                    <li> Always scan files downloaded from the Internet before using them</li>
                                                    <li> Do not install any unapproved software on your computer. </li>
                                                    <li>  Make sure that your virus pattern files are updated. </li>
                                                    <li> Make sure that your computer is patched with the latest security updates. </li>
                                                    <li>  Scan your computer on a habitual basis </li>
                                                </ul>


                                            </div>

                                            <h3 class="panel-title"> Dealing with Virus Infections</h3>
                                            <div class="panel-content">
                                                <p style="padding:10px 0px 10px 20px;">  Always keep in mind that just because your computer is acting strangely or one of your programs doesn't work right, this does NOT mean that your computer has a virus.
                                                    Extreme measures such as formatting your hard drive should be avoided. They usually don't fix a virus infection, and may end up doing more harm than good, unless you're very knowledgeable about the effects of the particular virus you're dealing with. <br /> <br />

                                                <ul>
                                                    <li>

                                                        If you haven't already installed a reliable anti-virus program on your computer, do that first. Many problems which are thought to have occurred due to a virus infection are actually caused by software configuration blunders or other problems that have nothing to do with a virus.

                                                    </li>

                                                    <li>
                                                        If you do get a virus in your machine, follow the directions in your anti-virus program to clean it. If you have a backup of the corrupted files, use those to restore the files. Check the backups before you re-install them to make sure they aren’t infected as well.

                                                    </li>

                                                    For further assistance, check the web site and support services of your anti-virus program.
                                                </p>
                                            </div>



                                            <h3 class="panel-title"> Helpful Links</h3>
                                            <div class="panel-content">
                                                <p style="padding:10px 0px 10px 20px;">    Please follow the links below for more information about Latest Virus Threats, Security Advises, Virus Definitions and Removal Tools : <br />

                                                <ul>
                                                    <li> <a href="http://www.sarc.com" class="link" target="_blank">  http://www.sarc.com  </a></li>
                                                    <li><a href="http://www.mcafee.com" class="link" target="_blank"> http://www.mcafee.com </a></li>
                                                    <li> <a href="http://www.pandasoftware.com" class="link" target="_blank">http://www.pandasoftware.com </a></li>
                                                    <li> <a href="http://www.grisoft.com" class="link" target="_blank">http://www.grisoft.com </a></li>
                                                    <li> <a href="http://www.trendmicro.com" class="link" target="_blank">http://www.trendmicro.com </a></li>
                                                    <li> <a href="http://www.eset.com" class="link" target="_blank">http://www.eset.com </a></li>
                                                </ul>
                                                </p>
                                            </div>


                                            <h3 class="panel-title">Virus Definitions</h3>
                                            <div class="panel-content">
                                                <p style="padding:10px 0px 10px 20px;">


                                                <ul>
                                                    <li> <a href="#" class="link">  Norton Antivirus (Intelligent Updater, Mar 31, 2010) </a> </li>
                                                    <li> <a href="#" class="link"> AVG Scanner (Priority Update, Mar 31, 2010)</a> </li>
                                                    <li> <a href="#" class="link">McAfee Antivirus (Dat file, Mar 31, 2010) </a></li>
                                                    <li> <a href="#" class="link">McAfee Antivirus (EXE file, Mar 31, 2010) </a></li>
                                                    <li> <a href="#" class="link">Avira Antivirus (ZIP file, Mar 31, 2010) </a></li>
                                                </ul>
                                                </p>

                                            </div>




        </td>
        </tr>
        </table>

        </td>
        <td width="32" align="left" valign="top">&nbsp;</td>
        <td width="500" align="left" valign="top"><table width="500" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="524" align="center"><img src="images/security.jpg" width="490" height="482" /></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td><div class="box"><strong>Contact Customer Care</strong><br />
                            Call 0353 2501854
                            <br />
                            Email us: support@dreamlik.in<br />

                        </div></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <!--<tr>
                  <td><div class="box">
                    <strong>For Accounting Issues</strong>
                    <br />
                    E-mail: account@wlink.com.np
                    <br />
                    Tel. 5523050, Ext. 405 </div></td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                </tr>-->
            </table></td>
        </tr>
        </table>
    </div>
    </div>

    <br>
    <br>
    <br>

@endsection

        @section('footerscripts')
            <script src="js/jquery-1.3.2.js" type="text/javascript"></script>
            <script src="js/scroll-startstop.events.jquery.js" type="text/javascript"></script>
            <script>
                // Hiding the panel content. If JS is inactive, content will be displayed
                $( '.panel-content' ).hide();

                // Preparing the DOM

                // -- Update the markup of accordion container
                $( '.accordion' ).attr({
                    role: 'tablist',
                    multiselectable: 'true'
                });

                // -- Adding ID, aria-labelled-by, role and aria-labelledby attributes to panel content
                $( '.panel-content' ).attr( 'id', function( IDcount ) {
                    return 'panel-' + IDcount;
                });
                $( '.panel-content' ).attr( 'aria-labelledby', function( IDcount ) {
                    return 'control-panel-' + IDcount;
                });
                $( '.panel-content' ).attr( 'aria-hidden' , 'true' );
                // ---- Only for accordion, add role tabpanel
                $( '.accordion .panel-content' ).attr( 'role' , 'tabpanel' );

                // -- Wrapping panel title content with a <a href="">
                $( '.panel-title' ).each(function(i){

                    // ---- Need to identify the target, easy it's the immediate brother
                    $target = $(this).next( '.panel-content' )[0].id;

                    // ---- Creating the link with aria and link it to the panel content
                    $link = $( '<a>', {
                        'href': '#' + $target,
                        'aria-expanded': 'false',
                        'aria-controls': $target,
                        'id' : 'control-' + $target
                    });

                    // ---- Output the link
                    $(this).wrapInner($link);

                });

                // Optional : include an icon. Better in JS because without JS it have non-sense.
                $( '.panel-title a' ).append('<span class="icon">+</span>');

                // Now we can play with it
                $( '.panel-title a' ).click(function() {

                    if ($(this).attr( 'aria-expanded' ) == 'false'){ //If aria expanded is false then it's not opened and we want it opened !

                        // -- Only for accordion effect (2 options) : comment or uncomment the one you want

                        // ---- Option 1 : close only opened panel in the same accordion
                        //      search through the current Accordion container for opened panel and close it, remove class and change aria expanded value
                        $(this).parents( '.accordion' ).find( '[aria-expanded=true]' ).attr( 'aria-expanded' , false ).removeClass( 'active' ).parent().next( '.panel-content' ).slideUp(200).attr( 'aria-hidden' , 'true');

                        // Option 2 : close all opened panels in all accordion container
                        //$('.accordion .panel-title > a').attr('aria-expanded', false).removeClass('active').parent().next('.panel-content').slideUp(200);

                        // Finally we open the panel, set class active for styling purpos on a and aria-expanded to "true"
                        $(this).attr( 'aria-expanded' , true ).addClass( 'active' ).parent().next( '.panel-content' ).slideDown(200).attr( 'aria-hidden' , 'false');

                    } else { // The current panel is opened and we want to close it

                        $(this).attr( 'aria-expanded' , false ).removeClass( 'active' ).parent().next( '.panel-content' ).slideUp(200).attr( 'aria-hidden' , 'true');;

                    }
                    // No Boing Boing
                    return false;
                });
            </script>


@stop