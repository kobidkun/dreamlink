<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use User;
class Payment extends Model
{
   public function user() {
       return $this->belongsTo('App/User','user_id', 'id');
   }
}
