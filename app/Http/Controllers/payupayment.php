<?php

namespace App\Http\Controllers;

use App\Mail\paymentsuccess;
use App\PaymentRequest;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use Tzsk\Payu\Facade\Payment;
use App\Payment as Paymentmodel;
use Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

use SimpleSoftwareIO\QrCode\Facades\QrCode;
class payupayment extends Controller
{
    public function dashboard() {
        return view('dashboard.delete');
    }

    public function pay(Request $request) {

        $amount = $request->input('amount');
        $name = $request->input('name');
        $email = $request->input('email');
        $plan = '2mbps';
        $mobile = $request->input('job_title');
        $id = $request->input('udf1');
        /**
         * These are the minimum required fieldset.
         */
        $data = [
            'txnid' => strtoupper(str_random(8)), # Transaction ID.
            'amount' => $amount, # Amount to be charged.
            'productinfo' => $plan,
            'firstname' => $name, # Payee Name.
            'email' => $email, # Payee Email Address.
            'phone' => $mobile, # Payee Phone Number.
            'udf1' => $id, # Payee Phone Number.


     ];
     // dd($data);

     return Payment::make($data, function ($then) {
         $then->redirectTo('/payment/status');
         # OR...
         $then->redirectRoute('payment_status');
         # OR...
         $then->redirectAction('payupayment@status');
         /**
          * Above are general Redirect::to(), Redirect::route() and Redirect::action() Methods.
          * You can use them as you normally would (With Parameters if you like) in any place to redirect.
          *
          * Note: You have to return the Payment Facade.
          */
     });
}

    public function status(Request $request) {
        $payment = Payment::capture(); # capture the payment after it's done. That's it.
     // dd($payment);
       // $payment->transaction_id;
        $mobile = $payment->phone;
        $txtid = $payment->transaction_id;
        $total = $payment->amount;
        $firstname = $payment->firstname;
        $email = $payment->email;
        $status = $payment->status;
        $payable_id = $payment->payable_id ;
        $mihpayid = $payment->mihpayid ;
        $phone = $payment->phone ;
        $discount = $payment->discount ;
        $net_amount_debit = $payment->net_amount_debit ;
        $unmappedstatus = $payment->unmappedstatus ;
        $mode = $payment->mode ;
        $bank_ref_num = $payment->bank_ref_num ;
       // $bankcode = $payment->bankcode ;
        $cardnum = $payment->cardnum ;
        $name_on_card = $payment->name_on_card ;
        $issuing_bank = $payment->issuing_bank ;
        $card_type = $payment->card_type ;

        //extra
        $id = $payment->data;
        $json = json_decode($id);

        $reqid = $json->udf3;

//dd($json->udf3);

        //save

        $artist = new Paymentmodel;
        $artist->card_type = $card_type;
        $artist->issuing_bank = $issuing_bank;
        $artist->name_on_card = $name_on_card;
        $artist->cardnum = $cardnum;
       // $artist->bankcode = $bankcode;
        $artist->bank_ref_num = $bank_ref_num;
        $artist->mode = $mode;
        $artist->unmappedstatus = $unmappedstatus;
        $artist->net_amount_debit = $net_amount_debit;
        $artist->discount = $discount;
        $artist->phone = $phone;
        $artist->mihpayid = $mihpayid;
        $artist->payable_id = $payable_id;
        $artist->firstname = $firstname;
        $artist->email = $email;
        $artist->amount = $total;
        $artist->status = $status;
        $artist->userid = $json->udf1;
        $artist->txnid = $txtid;
      //  dd($artist);
        $artist->save();




              //dd($payment);

               //sms

             //Your authentication key
               $authKey = "143565AsrNcm5EU58b8ba2b";

               //Multiple mobiles numbers separated by comma
               $mobileNumber = $mobile;

               //Sender ID,While using route4 sender id should be 6 characters long.
               $senderId = "DRMLNK";

               //Your message to send, Add URL encoding here.
               if ( $status === 'Failed'){
                   $message = urlencode("Hi " . $firstname . " your payment of INR " .$total . " has Failed INR Your Ref id is ". $txtid . "Thankyou for using Dreamlink Payment Service");


               }else{
                   $message = urlencode("Hi " . $firstname . " your account has been successfully credited with INR " .$total . " Your Ref id is ". $txtid . "Thankyou for using Dreamlink Payment Service");

               }



               //Define route
               $route = "4";
               //Prepare you post parameters
               $postData = array(
                   'authkey' => $authKey,
                   'mobiles' => $mobileNumber,
                   'message' => $message,
                   'sender' => $senderId,
                   'route' => $route
               );

               //API URL
               $url="https://control.msg91.com/api/sendhttp.php";

               // init the resource
               $ch = curl_init();
               curl_setopt_array($ch, array(
                   CURLOPT_URL => $url,
                   CURLOPT_RETURNTRANSFER => true,
                   CURLOPT_POST => true,
                   CURLOPT_POSTFIELDS => $postData
                   //,CURLOPT_FOLLOWLOCATION => true
               ));


               //Ignore SSL certificate verification
               curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
               curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);


               //get response
               $output = curl_exec($ch);

               //Print error if any
               if(curl_errno($ch))
               {
                   echo 'error:' . curl_error($ch);
               }

               curl_close($ch);

              // echo ($output);
               //sms

             /*  if( $payment->success === 'success') {
                   dd($payment);

               } else {
                   echo ('failed');
               }*/
       // dd($payment);

        if ( $status === 'Failed'){
            Mail::to($email)->send(new paymentsuccess($payment));

        }else{
            Mail::to($email)->send(new paymentsuccess($payment));
        }



       return view('dashboard.pages.success')->with('paymentstatus', $payment);


   }

    protected function dispatch($job)
    {
    }

    public function showpayments(Request $request){
        $getuser = Auth::user()->id;
        $userpayment = DB::table('payments')->where('userid', $getuser)->orderBy('created_at', 'desc')->get();
     // dd($userpayment);
        //$user  = $user-&gt;load('posts');
        return view('dashboard.pages.payment', ['payment' => $userpayment]);
    }
// $images = App\Images::where('realisation_id',Request::get('id'))->get();
    public function viewdetails($id) {

        $getdetails = Paymentmodel::find($id);
return view('dashboard.pages.paymentdetails', ['details' => $getdetails]);
     //  $qr = QrCode::generate('Make me a QrCode!');

         //echo ($getdetails);

    }


}
