<?php

namespace App\Http\Controllers\admin;

use App\Http\Requests\adminadduser;
use App\Payment;
use App\PaymentRequest;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Waavi\UrlShortener\UrlShortener;

class dashboard extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function dashboardrequest(Request $request) {

        $userpayment = DB::table('payments')->orderBy('created_at', 'desc')->get();
        $userpaymentcount = DB::table('payments')->count();
        $totalpaymentlist =  DB::table('payments')->sum('amount');
        $totalpaymentfailed =  DB::table('payments')->where('status', 'Failed')->count();
        $totalpaymentfailedlist =  DB::table('payments')->where('status', 'Failed')->get();
        $getusers =  DB::table('users')->get();
        $totalusers =  DB::table('users')->count();
        $paymenttodaycount = DB::table('payments')->whereDate('created_at', DB::raw('CURDATE()'))->sum('amount');
        $paymenttoday = DB::table('payments')->whereDate('created_at', DB::raw('CURDATE()'))->get();

        //echo($totalpaymentfailedlist);
        //month




        // $totalpaymentcount =  $totalpaymentlist->amount;  ->whereBetween('placed_at',[$dateS,$dateE])
//  dd($totalpaymentlist);

        //  return view('dashboard.pages.payment', ['payment' => $userpayment]);
        return view('dashboard.admin.pages.dashboard')->with([
            'payments' => $userpaymentcount,
            'paymenttotal' => $totalpaymentlist,
            'failedpayment' => $totalpaymentfailed,
            'payment' => $userpayment,
            'failedpaymentlist' => $totalpaymentfailedlist,
            'totalusers' => $totalusers,
            'getusers' => $getusers,
            'paymenttodaycount' => $paymenttodaycount,
            'paymenttoday' => $paymenttoday,
        ]);
    }

    public function listusers() {
        $getusers =  DB::table('users')->get();
        return view('dashboard.admin.pages.listuser')->with([
            'allusers' => $getusers
        ]);

    }


    public function usersdetails($id) {
        $usersdetails = User::find($id);
       // dd($usersdetails);
        $getid = $id;

        $userpayment = DB::table('payments')
            ->where('userid', $getid)
            ->orderBy('created_at', 'desc')->get();
        // dd($userpayment);
        //$user  = $user-&gt;load('posts');
       // dd($userpayment);
       return view('dashboard.admin.pages.userdetails')->with([
            'allusers' => $usersdetails,
           'payments' => $userpayment,
        ]);

    }

    public function viewdetails($id) {

        $getdetails = Payment::find($id);

//dd($getdetails);
        return view('dashboard.admin.pages.paymentdetails')->with([
            'details' => $getdetails,

        ]);
        //  $qr = QrCode::generate('Make me a QrCode!');

        //echo ($getdetails);

    }

    public function alltranactions() {
        $alltranactions = DB::table('payments')->orderBy('created_at', 'desc')->get();
        return view('dashboard.admin.pages.alltranactions')->with(['tranactions' => $alltranactions]);
    }

    public function adduser() {
        return view('dashboard.admin.pages.adduser');
    }

    public function adduserstore(Request $request) {



            $adduser = new User();
            $adduser->name = $request->name;
            $adduser->email = $request->email;
            $adduser->mobile = $request->mobile;
            $adduser->username = $request->username;
            $adduser->address = $request->address;
            $adduser->password = bcrypt('dreamlink');
            $adduser->save();

            return back();


    }

    public function requestpaymentstore(Request $request){
        $payurl = $request->username.md5(uniqid(rand(), true));
        $payment = new PaymentRequest();
        $payment->name = $request->name;
        $payment->username = $request->username;
        $payment->user_id = $request->user_id;
        $payment->mobile = $request->mobile;
        $payment->email = $request->email;
        $payment->amount = $request->amount;
        $payment->status = 'pending';
        $payment->package = $request->package;
        $payment->link_send_url	 = $payurl;
        $payment->save();

       // $baseurl = url();

        $url = \UrlShortener::shorten('https://www.dreamlink.in/payments-requests/'.$payurl);

        Mail::to($request->email)->send(new \App\Mail\paymentRequest($request, $url));

        //sms

        //Your authentication key
        $authKey = "143565AsrNcm5EU58b8ba2b";

        //Multiple mobiles numbers separated by comma
        $mobileNumber = $request->mobile;

        //Sender ID,While using route4 sender id should be 6 characters long.
        $senderId = "DRMLNK";

        //Your message to send, Add URL encoding here.

            $message = urlencode("Hi " . $request->name . " have a  payment Request of INR "
                .$request->amount . " for username ". $request->username .
                " Pay now " . $url);





        //Define route
        $route = "4";
        //Prepare you post parameters
        $postData = array(
            'authkey' => $authKey,
            'mobiles' => $mobileNumber,
            'message' => $message,
            'sender' => $senderId,
            'route' => $route
        );

        //API URL
        $url="https://control.msg91.com/api/sendhttp.php";

        // init the resource
        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $postData
            //,CURLOPT_FOLLOWLOCATION => true
        ));


        //Ignore SSL certificate verification
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);


        //get response
        $output = curl_exec($ch);

        //Print error if any
        if(curl_errno($ch))
        {
            echo 'error:' . curl_error($ch);
        }

        curl_close($ch);

        // echo ($output);
        //sms

        /*  if( $payment->success === 'success') {
              dd($payment);

          } else {
              echo ('failed');
          }*/
        // dd($payment);
        //endsms



        return back();
    }

    public function listallrequestpayment() {
        $all = PaymentRequest::all();
        return view('dashboard.admin.pages.listrequestpayment', ['data' => $all]);
    }

    public function viewdetailsofpaymentrequest($id){

        $find = PaymentRequest::find($id);
        return view('dashboard.admin.pages.paymentrequestdetails',['pay' => $find]);

        //dd($find);
    }

    public function updatedaminpassword($id, Request $request){
        $sv = User::find($id);
        $sv->password = bcrypt($request->password);

        $sv->save();
        return redirect('admin/alluser/'.$id);
    }

    public function updatedaminprofile($id, Request $request){
        $sv = User::find($id);
        $sv->name = $request->name;
        $sv->username = $request->username;
        $sv->email = $request->email;
        $sv->mobile = $request->mobile;
        $sv->address = $request->address;

        $sv->save();
        return redirect('admin/alluser/'.$id);
    }


}
