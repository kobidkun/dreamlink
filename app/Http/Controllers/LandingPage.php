<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CampaignCache;
use Illuminate\Support\Facades\Mail;
class LandingPage extends Controller
{
   public function saveform(Request $request) {
       $s = new CampaignCache();
       $s->name = $request->name;
       $s->email = $request->email;
       $s->mobile = $request->mobile;
       $s->save();

       Mail::to('amit@dreamlink.in', 'nazim.tarafdar@dreamlink.in', 'info@tecions.com')->send(new \App\Mail\callRequest($request));

       return redirect('/');



   }
}
