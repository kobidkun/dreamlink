<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Payment as Paymentmodel;
class Dashboard extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

   public function dashboard2(Request $request) {
       $getuser = Auth::user()->id;
       $userpayment = DB::table('payments')->where('userid', $getuser)->orderBy('created_at', 'desc')->get();
       $userpaymentcount = DB::table('payments')->where('userid', $getuser)->count();
     $totalpaymentlist =  DB::table('payments')->where('userid', $getuser)->sum('amount');
       $totalpaymentfailed =  DB::table('payments')->where('userid', $getuser)->where('status', 'Failed')->count();
       $totalpaymentfailedlist =  DB::table('payments')->where('userid', $getuser)->where('status', 'Failed')->get();
       //echo($totalpaymentfailedlist);
     //month




       // $totalpaymentcount =  $totalpaymentlist->amount;  ->whereBetween('placed_at',[$dateS,$dateE])
//  dd($totalpaymentlist);

     //  return view('dashboard.pages.payment', ['payment' => $userpayment]);
       return view('dashboard.pages.dashboard')->with([
           'payments' => $userpaymentcount,
           'paymenttotal' => $totalpaymentlist,
           'failedpayment' => $totalpaymentfailed,
           'payment' => $userpayment,
           'failedpaymentlist' => $totalpaymentfailedlist
       ]);
   }

   public function paymentsdetails() {
       return view('dashboard.pages.paymentdetails');
   }
}
