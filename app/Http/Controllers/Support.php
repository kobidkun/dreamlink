<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Support extends Controller
{
    public function ftth_troubleshoot() {
        return view('pages.faq.dlfiber');
    }

    public function dreamlinksupport()
    {
        return view('pages.faq.dmgeneral-support');
    }

    public function dreamlinksettings()
    {
        return view('pages.faq.dreamlink-settings');
    }
}
