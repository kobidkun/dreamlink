<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Company extends Controller
{
    public function companyprofile() {
        return view('pages.company.profile');

    }

    public function corevalues() {
        return view('pages.company.core-values');
    }

    public function membership() {

        return view('pages.company.membership');

    }

    public function missionvision() {

        return view('pages.company.mission-vission');

    }
    public function partnerships() {
        return view('pages.company.partnerships');

    }
    public function quality() {

    return view('pages.company.quality');
    }
}
