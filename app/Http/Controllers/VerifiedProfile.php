<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
class VerifiedProfile extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */



    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( )

    {

        return view('dashboard.pages.updateprofile');
    }

    public function storeprofile(Request $request) {
       //dd('jhjhj');
        $user = Auth::user();
        $user->address = $request->address;
        $user->country = $request->country;
        $user->city = $request->city;
        $user->pin = $request->pin;
        $user->password = bcrypt($request->password);
        $user->profile_saved = '1';

      $user->save();

        return redirect('/home');

    }

    public function editprofile() {
        return view('dashboard.pages.profile');
    }
    public function  updateprofile($id, Request $request) {

        $user = User::findOrFail($id);
        $user->password = bcrypt($request->password);
        $user->mobile = $request->mobile;
        $user->address = $request->address;
        $user->update();
        return redirect('/profile');


    }
}
