<?php

namespace App\Http\Controllers;
use App\Mail\paymentfailure;
use App\Mail\paymentsuccess;
use Auth;
use App\PaymentRequest;
use function foo\func;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Tzsk\Payu\Facade\Payment;
use App\Payment as Paymentmodel;
use Illuminate\Support\Facades\Mail;
class PaymentRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $findid =  Auth::user()->id;
        $p =  DB::table('payment_requests')->where('user_id', $findid)->get();
    // dd($p);
        return view('dashboard.pages.requestpaymentlist', ['data' => $p]);
    }

    public function details($id){
        $p = PaymentRequest::find($id);
        return view('dashboard.pages.requestpayment', ['data' => $p]);
    }

    public function detailspublic($link_send_url){
        $p = PaymentRequest::where('link_send_url', '=' ,$link_send_url)->firstOrFail();
       // find($link_send_url);
       // dd($p);


        return view('dashboard.pages.public-requestpayment', ['data' => $p]);
    }

    public function paynow(Request $request) {
        $name = $request->name;
        $email = $request->email;
        $plan = $request->package;
        $mobile = $request->mobile;
        $id = $request->id;
        $amount = $request->amount;
        $reqid = $request->reqid;
        /**
         * These are the minimum required fieldset.
         */
        $data = [
            'txnid' => strtoupper(str_random(8)), # Transaction ID.
            'amount' => $amount, # Amount to be charged.
            'productinfo' => $plan,
            'firstname' => $name, # Payee Name.
            'email' => $email, # Payee Email Address.
            'phone' => $mobile, # Payee Phone Number.
            'udf1' => $id, # Payee Phone Number.
            'udf2' => $plan, # Payee Phone Number.
            'udf3' => $reqid, # Payee Phone Number.


        ];
        // dd($data);

        return Payment::make($data, function ($then) {
            $then->redirectTo('/payment/status');
            # OR...
            $then->redirectRoute('payment_status');
            # OR...
            $then->redirectAction('payupayment@status');
            /**
             * Above are general Redirect::to(), Redirect::route() and Redirect::action() Methods.
             * You can use them as you normally would (With Parameters if you like) in any place to redirect.
             *
             * Note: You have to return the Payment Facade.
             */
        });
    }



    public function paynowpublic(Request $request) {
        $name = $request->name;
        $email = $request->email;
        $plan = $request->package;
        $mobile = $request->mobile;
        $id = $request->id;
        $amount = $request->amount;
        $reqid = $request->reqid;
        /**
         * These are the minimum required fieldset.
         */
        $data = [
            'txnid' => strtoupper(str_random(8)), # Transaction ID.
            'amount' => $amount, # Amount to be charged.
            'productinfo' => $plan,
            'firstname' => $name, # Payee Name.
            'email' => $email, # Payee Email Address.
            'phone' => $mobile, # Payee Phone Number.
            'udf1' => $id, # Payee Phone Number.
            'udf2' => $plan, # Payee Phone Number.
            'udf3' => $reqid, # Payee Phone Number.


        ];
        // dd($data);

        return Payment::make($data, function ($then) {
            $then->redirectTo('/public-payment/status');
            # OR...
            $then->redirectRoute('paymentpublicstatus');
            # OR...
            $then->redirectAction('PaymentRequestController@status');
            /**
             * Above are general Redirect::to(), Redirect::route() and Redirect::action() Methods.
             * You can use them as you normally would (With Parameters if you like) in any place to redirect.
             *
             * Note: You have to return the Payment Facade.
             */
        });
    }


    public function status(Request $request) {
        $payment = Payment::capture(); # capture the payment after it's done. That's it.
        // dd($payment);
        // $payment->transaction_id;
        $mobile = $payment->phone;
        $txtid = $payment->transaction_id;
        $total = $payment->amount;
        $firstname = $payment->firstname;
        $email = $payment->email;
        $status = $payment->status;
        $payable_id = $payment->payable_id ;
        $mihpayid = $payment->mihpayid ;
        $phone = $payment->phone ;
        $discount = $payment->discount ;
        $net_amount_debit = $payment->net_amount_debit ;
        $unmappedstatus = $payment->unmappedstatus ;
        $mode = $payment->mode ;
        $bank_ref_num = $payment->bank_ref_num ;
        // $bankcode = $payment->bankcode ;
        $cardnum = $payment->cardnum ;
        $name_on_card = $payment->name_on_card ;
        $issuing_bank = $payment->issuing_bank ;
        $card_type = $payment->card_type ;

        //extra
        $id = $payment->data;
        $json = json_decode($id);

        $reqid = $json->udf3;

//dd($json->udf3);

        //save

        $artist = new Paymentmodel;
        $artist->card_type = $card_type;
        $artist->issuing_bank = $issuing_bank;
        $artist->name_on_card = $name_on_card;
        $artist->cardnum = $cardnum;
        // $artist->bankcode = $bankcode;
        $artist->bank_ref_num = $bank_ref_num;
        $artist->mode = $mode;
        $artist->unmappedstatus = $unmappedstatus;
        $artist->net_amount_debit = $net_amount_debit;
        $artist->discount = $discount;
        $artist->phone = $phone;
        $artist->mihpayid = $mihpayid;
        $artist->payable_id = $payable_id;
        $artist->firstname = $firstname;
        $artist->email = $email;
        $artist->amount = $total;
        $artist->status = $status;
        $artist->userid = $json->udf1;
        $artist->txnid = $txtid;
        //  dd($artist);
        $artist->save();

        if( $status === 'Success') {

            $p =  DB::table('payment_requests')->where('id', $reqid)->update(['status' => 'Success']);

        } else {
            $p =  DB::table('payment_requests')->where('id', $reqid)->update(['status' => 'Failed']);
            //$getid = $p->id;
           // dd('failed');


        }




        //dd($payment);




       $authKey = "143565AsrNcm5EU58b8ba2b";
        $mobileNumber = $mobile;
        $senderId = "DRMLNK";
        if ( $status === 'Failed'){
            $message = urlencode("Hi " . $firstname . " your payment of INR " .$total . " has Failed INR Your Ref id is ". $txtid . "Thankyou for using Dreamlink Payment Service");
        }else{
            $message = urlencode("Hi " . $firstname . " your account has been successfully credited with INR " .$total . " Your Ref id is ". $txtid . "Thankyou for using Dreamlink Payment Service");
          }
        $route = "4";
        $postData = array(
            'authkey' => $authKey, 'mobiles' => $mobileNumber, 'message' => $message, 'sender' => $senderId, 'route' => $route
        );
        $url="https://control.msg91.com/api/sendhttp.php";
        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $postData
            //,CURLOPT_FOLLOWLOCATION => true
        ));


        //Ignore SSL certificate verification
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);


        //get response
        $output = curl_exec($ch);

        //Print error if any
        if(curl_errno($ch))
        {
            echo 'error:' . curl_error($ch);
        }

        curl_close($ch);



        if ( $status === 'Failed'){
            Mail::to($email)->send(new paymentfailure($payment));

        }else{
            Mail::to($email)->send(new paymentsuccess($payment));
        }



        return view('dashboard.pages.public.success')->with('paymentstatus', $payment);


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PaymentRequest  $paymentRequest
     * @return \Illuminate\Http\Response
     */
    public function show(PaymentRequest $paymentRequest)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PaymentRequest  $paymentRequest
     * @return \Illuminate\Http\Response
     */
    public function edit(PaymentRequest $paymentRequest)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PaymentRequest  $paymentRequest
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PaymentRequest $paymentRequest)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PaymentRequest  $paymentRequest
     * @return \Illuminate\Http\Response
     */
    public function destroy(PaymentRequest $paymentRequest)
    {
        //
    }
}
