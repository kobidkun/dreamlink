<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
class VerifiedProfile
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check()) { // first check if there is a logged in user

            $user_id = Auth::user()->profile_saved; // get user id

            if($user_id == "0"){
                return redirect('/updateprofile');
            }
        }
        return $next($request);
    }
}
