<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Http\Request;

class paymentsuccess extends Mailable
{
    use Queueable, SerializesModels;
   // protected $message;
    public $payment;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($payment)
    {

        $this->payment = $payment;

        //  $status
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $address = 'noreply@mailapi.tecions.com';
        $name = 'Dreamlink';


        return $this
            ->from($address, $name)
            ->cc($address, $name)
            ->bcc($address, $name)
            ->replyTo($address, $name)
            ->subject('You have Successfully made transaction with id'. $this->payment->transaction_id)
            ->view('emails.success');
    }
}
