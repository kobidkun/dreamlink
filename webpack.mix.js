let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.styles([
    'resources/assets/dashboard/animate.css',
    'resources/assets/dashboard/bootstrap.min.css',
    'resources/assets/dashboard/sidebar-nav.min.css',
    'resources/assets/dashboard/style.css',
    'resources/assets/dashboard/style/icons/material-design-iconic-font2/css/material-design-iconic-font.min.css'
], 'public/dashboard/compiled.min.css');



mix.scripts([
    'resources/assets/dashboard/jquery.min.js',
    'resources/assets/dashboard/bootstrap.min.js',
    'resources/assets/dashboard/sidebar-nav.min.js',
    'resources/assets/dashboard/jquery.slimscroll.js',//
    'resources/assets/dashboard/waves.js',//
    'resources/assets/dashboard/custom.min.js',
], 'public/dashboard/compiled.min.js');
