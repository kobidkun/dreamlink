-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Aug 14, 2017 at 01:58 AM
-- Server version: 5.6.35
-- PHP Version: 7.0.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mlm`
--

-- --------------------------------------------------------

--
-- Table structure for table `epins`
--

CREATE TABLE `epins` (
  `id` int(10) UNSIGNED NOT NULL,
  `epin` varchar(120) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` varchar(120) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `activate` varchar(120) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `epins`
--

INSERT INTO `epins` (`id`, `epin`, `user_id`, `activate`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, '10524598e04fa64a7e8.02373649', NULL, NULL, NULL, '2017-08-11 13:56:50', '2017-08-11 13:56:50'),
(2, '14629598e05731cb547.73988672', '73', NULL, NULL, '2017-08-11 13:58:51', '2017-08-11 13:58:51'),
(3, '236095990c3f0da3597.53541877', '73', NULL, NULL, '2017-08-13 15:56:08', '2017-08-13 15:56:08'),
(4, '211185990ca2617b977.79021255', '73', NULL, NULL, '2017-08-13 16:22:38', '2017-08-13 16:22:38'),
(5, '132795990ca70411fb7.42677950', '73', NULL, NULL, '2017-08-13 16:23:52', '2017-08-13 16:23:52'),
(6, '13855990e2cd24e6f8.37922355', '3', NULL, NULL, '2017-08-13 18:07:49', '2017-08-13 18:07:49'),
(7, '317265990e2fe2de9a3.03316189', '3', NULL, NULL, '2017-08-13 18:08:38', '2017-08-13 18:08:38');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `epins`
--
ALTER TABLE `epins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `epins_epin_unique` (`epin`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `epins`
--
ALTER TABLE `epins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
