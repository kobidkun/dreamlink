<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->text('name')->nullable();
            $table->string('email');
            $table->string('mobile')->nullable();
            $table->string('username')->unique();
            $table->integer('payment_id')->nullable();
            $table->integer('user_id')->nullable();
            $table->text('address')->nullable();
            $table->text('city')->nullable();
            $table->text('country')->nullable();
            $table->text('pin')->nullable();
            $table->text('package')->nullable();
            $table->text('last_payment')->nullable();
            $table->text('phone')->nullable();
            $table->text('acctype')->nullable();
            $table->string('profile_saved')->default('0');
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
