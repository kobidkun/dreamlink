<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('payable_id')->default(0);
            $table->string('payable_type')->nullable();
            $table->integer('userid')->unsigned()->index()->nullable();
            $table->string('txnid')->nullable();
            $table->string('mihpayid')->nullable();
            $table->string('firstname')->nullable();
            $table->string('lastname')->nullable();
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->double('amount')->nullable();
            $table->double('discount')->nullable();
            $table->double('net_amount_debit')->nullable();
            $table->text('data')->nullable();
            $table->string('status')->nullable();
            $table->string('unmappedstatus')->nullable();
            $table->string('mode')->nullable();
            $table->string('bank_ref_num')->nullable();
            $table->string('bankcode')->nullable();
            $table->string('cardnum')->nullable();
            $table->string('name_on_card')->nullable();
            $table->string('issuing_bank')->nullable();
            $table->string('card_type')->nullable();
            $table->string('finalpaymentstatus')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
