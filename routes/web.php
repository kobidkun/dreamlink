<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




Route::get('/landingpage', function () {
    return view('landing.landing');
});

Route::post('/landingpage', 'LandingPage@saveform')->name('landingpage.saveform');


Route::get('/dash', function () {
    return view('dashboard.delete');
});



Route::get('/dreamlink-fiber', function () {
    return view('pages.services.dreamlink-fiber');
});

Route::get('/dreamlink-rf', function () {
    return view('pages.services.dreamlink-rf');
});

Route::get('/plans-pricing', function () {
    return view('pages.services.plans-pricing');
});


Route::get('/contact', function () {
    return view('pages.contact');
});

Route::get('/terms-and-condition', function () {
    return view('pages.faq.terms');
});

Route::get('/privacy', function () {
    return view('pages.faq.privacy');
});


Route::get('/security', function () {
    return view('pages.company.security');
});


//delete

//Route::get('/requested-payments', 'PaymentRequestController@index');
Route::get('/payments-requests/{link_send_url}', 'PaymentRequestController@detailspublic')
    ->name('user.payment.details.user.public');



//delete
Route::resource('/', 'PageController');
//services list
Route::get('/sme-plan', 'Services@smeplan');
Route::get('/ftth_troubleshoot', 'Support@ftth_troubleshoot');
Route::get('/dreamlink-general-support', 'Support@dreamlinksupport');
Route::get('/dreamlink-settings', 'Support@dreamlinksettings');

///company
Route::get('/companyprofile', 'Company@companyprofile');
Route::get('/membership', 'Company@membership');
Route::get('/missionvision', 'Company@missionvision');
Route::get('/partnerships', 'Company@partnerships');
Route::get('/quality', 'Company@quality');

//end static list

Auth::routes();
//user block
Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');
Route::get('/updateprofile', 'VerifiedProfile@index')->middleware('auth');
Route::post('/updateprofile', 'VerifiedProfile@storeprofile')->name('update.profile.store.verify')->middleware('auth');
Route::get('/profile', 'VerifiedProfile@editprofile')->middleware('auth');
Route::put('/profile/{id}', 'VerifiedProfile@updateprofile')->name('profile.update')->middleware('auth');
Route::get('/manage', 'Dashboard@dashboard2')->middleware('auth');
Route::get('/paymentsdetails', 'Dashboard@paymentsdetails')->middleware('auth');
Route::get('/paymentdetails/{id}', 'payupayment@viewdetails')->name('paymentdetails.show')->middleware('auth');
Route::get('/payments', 'payupayment@showpayments')->middleware('auth');
Route::get('/requested-payments', 'PaymentRequestController@index')->middleware('auth');
Route::get('/payments-requests/{id}', 'PaymentRequestController@details')->name('user.payment.details.user')->middleware('auth');

Route::get('/payment', ['as' => 'pay', 'uses' => 'payupayment@pay'])->middleware('auth');
Route::get('/requested-payment', ['as' => 'paynow', 'uses' => 'PaymentRequestController@paynow'])->middleware('auth');
Route::get('/payment/status', ['as' => 'payment_status', 'uses' => 'payupayment@status'])->middleware('auth');
Route::get('/payment/status', ['as' => 'payment_status', 'uses' => 'payupayment@status'])->middleware('auth');

Route::get('/requested-payment-public', ['as' => 'paynowpublic', 'uses' => 'PaymentRequestController@paynowpublic']);
Route::get('/public-payment/status', ['as' => 'paymentpublicstatus', 'uses' => 'PaymentRequestController@status']);
//admins

Route::prefix('admin')->group(function() {
    Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
    Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
    Route::get('/dashboard', 'admin\dashboard@dashboardrequest')->name('admin.dashboard');
    Route::get('/alluser', 'admin\dashboard@listusers');
    Route::get('/alluser/{id}', 'admin\dashboard@usersdetails')->name('details.user');
    Route::get('/payment/{id}', 'admin\dashboard@viewdetails')->name('details.payment.admin');
    Route::get('/alltranactions', 'admin\dashboard@alltranactions');
    Route::get('/adduser', 'admin\dashboard@adduser');
    Route::post('/adduser', 'admin\dashboard@adduserstore')->name('admin.user.store');
    Route::put('/user-password-update/{id}', 'admin\dashboard@updatedaminpassword')->name('user.password.update');
    Route::put('/user-profile-update/{id}', 'admin\dashboard@updatedaminprofile')->name('user.profile.update');
    Route::post('/requestpayment', 'admin\dashboard@requestpaymentstore')->name('admin.request.payment');
    Route::get('/all-pay-request', 'admin\dashboard@listallrequestpayment');
    Route::get('/all-pay-request/{id}', 'admin\dashboard@viewdetailsofpaymentrequest')->name('details.payment.request');


});


